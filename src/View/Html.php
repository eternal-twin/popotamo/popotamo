<?php

namespace Popotamo\View;

use Popotamo\Config;
use Popotamo\Entity\Profile;
use Popotamo\Model\GameProfile;
use Popotamo\Service\Profiler;

class Html
{
    private $data = [];

    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * Header of all the HTML pages of the site
     * @return string
     */
    function header($additionalhead="") {
        return '
            <!DOCTYPE html>
            <html>
                <head>
                    <title>Popotamo</title>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
                    <link type="text/css" rel="stylesheet" href="/resources/materialize.min.css"  media="screen,projection"/>
                    <link rel="stylesheet" href="/resources/main.css?v=2.2">
                    <script defer src="/resources/materialize.min.js"></script>
                    <script defer src="/resources/scripts.js?v=2.4"></script>
                    <script defer src="/resources/ApiCaller.js?v=1.2"></script>
                    '.$additionalhead.'
                </head>

                <body>
                    <header>
                        <a href="/"><img src="/resources/img/banner.gif" alt="Popotamo - Un jeu de mots !"></a>
                    </header>';
    }


    function menu() {

        return '<nav id="menu">
                    <a href="/">Jouer</a> <img src="/resources/img/grass.gif" alt="/">
                    '.(isset($this->data['gameId']) ? '<a href="/minishop/'.$this->data['gameId'].'">Poposhop</a> <img src="/resources/img/grass.gif" alt="/">' : '').
                    '<a href="/?page=launchgame">Plus de parties</a> <img src="/resources/img/grass.gif" alt="/">
                    <a href="https://eternal-twin.net/forum/sections/d058cc35-3c0c-48cf-86b7-e35489cc901b" target="_blank">Forum</a> <img src="/resources/img/grass.gif" alt="/">
                    <a href="/logout" alt=""><img src="/resources/img/logout.gif"> Quitter</a>
                </nav>';
    }


    /**
     * Footer of all the HTML pages of the site
     * @return string
     */
    function footer() {
        return '
                <footer>
                    <a href="https://discord.gg/ERc3svy">Discord</a> ·
                    <a href="https://wiki.eternal-twin.net/popotamo">Wiki</a> ·
                    <a href="/stats">Statistiques</a> ·
                    <a href="https://gitlab.com/eternaltwin/popotamo/popotamo">Code source</a> ·
                    <a>Généré en '.Profiler::getExecutionTime().'s</a> ·
                    <a>'.Config::get('server.deployment.gitRevision').'</a>
                </footer>
            </body>
        </html>';
    }


    /**
     * Generates the game board
     * @param int   $nbrCols
     * @param int   $nbrRows
     * @param array $boardLetters The letters already placed on the board
     * @return type
     */
    function board($nbrCols, $nbrRows, $boardLetters) {

        $table = '';

        // Generates the lines (each loop = 1 new line)
        for($row=0; $row<$nbrRows; $row++) {

            // Generates the cells inside the line (each loop = 1 new cell)
            $cells = '';
            for($col=0; $col<$nbrCols; $col++) {
                // Mark the central cell with a different color
                $class = ($col === (int)floor($nbrCols/2) and $row === (int)floor($nbrRows/2)) ? 'centralCell' : '';

                $cells .= '<td id="coord_'.$col.'_'.$row.'" data-coord-x="'.$col.'" data-coord-y="'.$row.'" class="'.$class.'"></td>';
            }

           // Saves the generated line for later display
            $table .= '<tr>'.$cells.'</tr>';
        }

        // Displays the generated map
        return '
            <div class="board-wrapper">
                <table id="board">'.$table.'</table>
                <div id="gameLockedAlert">La partie est en attente de joueurs...</div>
                <template data-type="letter"><div class="letter" data-letter-holder></div></template>
            </div>
        ';
    }


    function inventory() {
        return '<div id="inventory"></div>';
    }


    /**
     * @param GameProfile[] $gameProfiles
     * @return string
     */
    function profiles(array $gameProfiles): string {
        $Calc = new \Popotamo\Controller\Calc();
        $Filter = new \Popotamo\Controller\Filter();
        $html = '';

        // Sorting by current rank 
        usort($gameProfiles, function($a, $b) {
            return $a->winnerRank > $b->winnerRank;
        });

        $template = <<<HTML
        <template data-type="profile"><div class="profile">
            <div data-type="online-status" class="online-status"></div>
            <div class="username"><p>&#x1F464; <span data-type="username"></span></p></div>
            <div class="score-info">
                <div class="z-depth-2">
                    <div style="font-size:1.8em" data-type="score-percent"></div>
                    <div class="green-text text-lighten-4">
                        <strong class="bold" data-type="score"></strong>/<span data-type="score-goal"></span>
                    </div>
                </div>
            </div>
            <div class="peek">
                <div data-title="{{username}} possède {{inventoryCount}} lettres dans son inventaire">&#x1F521; <span data-type="inventory-count"></span> lettres</div>
                <div data-title="{{username}} dispose actuellement de {{actionPoints}} points d'action">&#x26A1; <span data-type="action-points"></span> actions</div>
                <div data-title="{{username}} possède {{optionsCount}} options">&#9994;&#127995; <span data-type="options-count"></span> options</div>
            </div>
        </div></template>
        HTML;

        return '<div class="js-profile-collection" id="profiles"></div>'.$template;
    }


    /**
     * The player's profile above the game board
     *
     * @param Profile $profile The basic data if the connected player (name, quids...)
     * @param array $profileSkills The "competences" and "options" of the player
     * @return string HTML
     */
    function profileInsert(Profile $profile, $profileSkills) 
    {
        $competences = $profileSkills['competences'];

        return '
            <table id="profileInsert" class="right z-depth-2 blue lighten-4">
                <tr>
                    <td colspan="2" class="blue lighten-3">
                        <span class="pseudo">&#x1F464; '.$profile->getDisplayName().'</span>
                        <a href="/logout" style="float:right;margin-top:0.2em;">[Déconnexion]</a>
                    </td>
                </tr>
                <tr>
                    <td><strong>Score :</strong> ?</td>
                    <td><strong>Rapidité :</strong> '.$competences['speed']['level'].'</td>
                </tr>
                <tr>
                    <td><strong>Coupes :</strong> ?</td>
                    <td><strong>Imagination :</strong> '.$competences['imagination']['level'].'</td>
                </tr>
                <tr>
                    <td><strong>Quids :</strong> '.$profile->getQuids().'</td>
                    <td><strong>Sagesse :</strong> '.$competences['wisdom']['level'].'</td>
                </tr>
            </table>';
    }
}
