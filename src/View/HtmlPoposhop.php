<?php
namespace Popotamo\View;

use \Popotamo\Controller\Calc;


/**
 * Builds the HTML for the Poposhop (to buy competences/options with Quids)
 */
class HtmlPoposhop {
    
    
    /**
     * Displays all the "options" buyable in the Poposhop
     * 
     * @param array $skills The characteristics of the "options" and "competences" 
     *                      (name, description...)
     * @param array $profileCharacs The characteristics of the player (competences, options...),
     *                              as extracted from the database
     * @return string HTML
     */
    function options($skills, $profileCharacs) {
        
        $result = [ 'options' => '',
                    'competences' => ''
                    ];
        
        foreach($skills as $alias=>$skill) {
            
            $htmlSkill = $this->option($alias, $skill, $profileCharacs);
            
            // NB: the "Trash" option is native for all the players, so no need to buy it
            if($skill['skill_type'] === 'options' and $skill['quids_cost'] > 0) {
                $result['options'] .= $htmlSkill;
            }
            elseif($skill['skill_type'] === 'competences') {
                $result['competences'] .= $htmlSkill;
            }
        }
        
        return '
            <table>
                <tr>
                    <th colspan="3" class="green lighten-3 green-text text-darken-4">Acquérir des compétences</th>
                </tr>
                '.$result['competences'].'
                <tr>
                    <th colspan="3" class="green lighten-3 green-text text-darken-4">Acquérir des options</th>
                </tr>
                '.$result['options'].'
            </table>';
    }
    
    
    /**
     * Displays one skill ("option" or "competence") for the Poposhop 
     * ("option" = tornado, recycling... ; "competence" = wisdom, imagination...)
     * 
     * @param string $alias The internal/technical name of the skill (e.g. "tornodo")
     * @param array $skillCharacs The characteristics of the option or competence
     *                            (name, description...)
     * @param array $profileCharacs The characteristics of the player (competences, options...),
     *                              as extracted from the database
     * @return string HTML
     */
    private function option($alias, $skillCharacs, $profileCharacs) {
        
        $Calc = new Calc();
        
        $skillType  = $skillCharacs['skill_type'];
        $nextLevel  = $profileCharacs[$skillType][$alias]['level'] + 1;
        $isMaxLevel = ($nextLevel > $skillCharacs['max_level']) ? true : false;        
        $quidsCost  = $Calc->quidsCost($skillCharacs['skill_type'], $skillCharacs['quids_cost'], $nextLevel);
        
        $htmlLevel     = ($skillType === 'competences') ? ' niv. '.$nextLevel : '';        
        $buttonColor   = ($isMaxLevel === true) ? 'disabled' : 'waves-effect waves-light blue darken-3';
        $htmlQuidsCost = ($isMaxLevel === true) 
                          ? '<span class="grey-text">max. atteint</span>'
                          : '<strong>'.$quidsCost.' &#x1F48E;</strong>';
        
        return '
            <tr>
                <td style="white-space:nowrap">
                    <div class="skill">'.$skillCharacs['icon_text'].'</div>
                    <strong style="font-size:120%" class="teal-text text-darken-2">'.ucfirst($skillCharacs['name']).$htmlLevel.'</strong>
                </td>
                <td class="description">'.$skillCharacs['description'].'</td>
                <td class="center">
                    <a href="#!" class="btn-small z-depth-2 '.$buttonColor.'"
                        onclick="sendData(\'POST\', \'skills\', \'action=upgrade&skill='.$alias.'\')">Acheter</a>
                    <br>'.$htmlQuidsCost.'
                </td>
            </tr>';        
    }
}
