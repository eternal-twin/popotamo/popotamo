<?php

namespace Popotamo\View;


use Popotamo\Model\GameProfile;

/**
 * HTML to generate modal pop-ups (uses Materialize.css)
 *
 */
class HtmlModal {
    private $optionsCharacs;
    
    /**
     * Sets the characteristics of all the "options" (name, description...)
     *
     * @param array $optionsCharacs
     */
    function setOptionsCharacs($optionsCharacs) {
        $this->optionsCharacs = $optionsCharacs;
    }

    /**
     * Pop-up to confirm the usage of a game option
     *
     * @param string $optionAlias The alias of the option to use (e.g. "trash").
     *                            See the table "popo_skills" to know the available aliases.
     * @param GameProfile[] $profiles The list a of profiles in the game
     * @return string HTML
     */
    function modalOption(string $optionAlias, array $profiles = []): string {
        $option = $this->optionsCharacs[$optionAlias];

        if ($option['popup_type'] === 'choseProfile') {
            return $this->modalChoseProfile($optionAlias, $option['icon_text'], $option['description'], $profiles);
        } elseif ($option['popup_type'] === 'choseLetter') {
            return $this->modalChoseLetter($optionAlias, $option['icon_text'], $option['description'], $profiles);
        } else { // Confirmation pop-up
            return $this->modalConfirmOption($optionAlias, $option['icon_text'], $option['description']);
        }
    }

    /**
     * HTML for pop-up
     * (uses Materilize.css)
     *
     * @param string $optionAlias The alias of the option to use (e.g. "trash").
     *                            See the array in the modalOption() method to know the available aliases.
     * @param string $icon A unicode character to illustrate the pop-up
     * @param string $text The text in the pop-up (can be HTML)
     * @param string $htmlButtons Free buttons for confirming, closing...
     * @return string HTML
     */
    private function modal($optionAlias, $icon, $text, $htmlButtons) {
        return '
        <div id="modalOption' . ucfirst($optionAlias) . '" class="modal teal lighten-5">
            <div class="modal-content">
                <div class="left" style="font-size:3em;margin-top:0.8rem">' . $icon . '</div>
                <div class="text">
                    ' . $text . '
                </div>
                <div class="center-align">
                    ' . $htmlButtons . '
                </div>
            </div>
        </div>';
    }

    /**
     * Pop-up for confirming the use of an option
     * (uses Materialize.css)
     *
     * @param string $optionAlias The alias of the option to use (e.g. "trash").
     *                            See the array in the modalOption() method to know the available aliases.
     * @param string $icon A unicode character to illustrate the pop-up
     * @param string $text The text in the pop-up (can be HTML)
     * @return string HTML
     */
    private function modalConfirmOption($optionAlias, $icon, $text) {
        return $this->modal($optionAlias, $icon, $text . '<p>Voulez faire cela ?</p>',
            '<a href="#!" class="modal-close btn waves-effect waves-light teal lighten-2">Annuler</a>
                     <a href="#!" class="modal-close btn waves-effect waves-light blue darken-3"
                        onclick="Popotamo.useOption(\''.$optionAlias.'\')">Confirmer</a>');
    }

    /**
     * Pop-up to select a player
     *
     * @param string $optionAlias The alias of the option to use (e.g. "trash").
     *                            See the array in the modalOption() method to know the available aliases.
     * @param string $icon A unicode character to illustrate the pop-up
     * @param string $text The text in the pop-up (can be HTML)
     * @param GameProfile[] $profiles The list a of players in the game
     * @return string HTML
     */
    private function modalChoseProfile(string $optionAlias, string $icon, string $text, array $profiles): string {
        $htmlProfiles = '';
        foreach ($profiles as $gameProfile) {
            $profile = $gameProfile->profile;
            $htmlProfiles .= '<a href="#!" class="btn-small waves-effect waves-light blue darken-3"
                                onclick="Popotamo.useOption' . ucfirst($optionAlias) . '(\'' . $profile->getId() . '\')">' . $profile->getDisplayName() . '</a> &nbsp; ';
        }

        return $this->modal($optionAlias, $icon,
            $text . '<div id="' . $optionAlias . 'Profiles"><p>Quel joueur voulez-vous cibler ?</p><p>' . $htmlProfiles . '</p></div>',
            '<a href="#!" class="modal-close btn-small waves-effect waves-light teal lighten-2">Annuler</a>');
    }

    /**
     * pop-up to select a letter
     *
     * @param string $optionAlias The alias of the option to use (e.g. "trash").
     *                            See the array in the modalOption() method to know the available aliases.
     * @param string $icon A unicode character to illustrate the pop-up
     * @param string $text The text in the pop-up (can be HTML)
     * @return string HTML
     */
    private function modalChoseLetter(string $optionAlias, string $icon, string $text): string {
        $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        $htmlLetters = '';
        foreach ($letters as $letter) {
            $htmlLetters .= '<span href="#!" class="letter modal-close waves-effect waves-light"
                                onclick="sendData(\'POST\', \'skills\', \'action=use&skill=joker&letter=' . $letter . '\')">' . $letter . '</span>';
        }

        return $this->modal($optionAlias, $icon,
            $text . '<p>Choisissez votre lettre :</p><p>' . $htmlLetters . '</p>',
            '<a href="#!" class="modal-close btn-small waves-effect waves-light teal lighten-2">Annuler</a>');
    }
}
