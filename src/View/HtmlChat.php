<?php

namespace Popotamo\View;
use \DateTime;

/**
 * Generates the HTML elements specific to the chat zone
 * (words placed + messages posted)
 */
class HtmlChat {

    /**
     * Call this method to gat all the HTML of the chat (new words + messages)
     *
     * @param array $chatEvents All the events of the chat, as return by the class ChatReader->getChat()
     * @return string HTML
     */
    function chat($chatEvents) {

        $result = '';

        foreach($chatEvents as $event) {

            if($event['eventType'] === 'newWords') {
                $result .= $this->chatEventNewWords($event['datetime'], $event['authorName'], $event['newWords']);
            }
            elseif($event['eventType'] === 'boardCleaned') {
                $result .= $this->chatEventBoardCleaned($event['datetime'], $event['authorName']);
            }
            elseif($event['eventType'] === 'optionUsed') {
                $result .= $this->chatEventOptionUsed($event['datetime'], $event['authorName'], $event['optionAlias']);
            }
            elseif($event['eventType'] === 'message') {
                $result .= $this->chatEventMessage($event['datetime'], $event['authorName'], $event['message']);
            }
            else {
                $result .= $this->chatEventUnknown($event['eventType']);
            }
        }

        return $result;
    }


    /**
     * Displays the date of an event in the chat
     *
     * @param string $isoDate Must be a date in the ISO 8601 format.
     *                        Example : "2020-12-29T15:15:13+00:00"
     * @return string HTML
     */
    private function date($isoDate) {

        $dt = new DateTime($isoDate);
        return '<span class="date">'.$dt->format('H:i').'</span>';
    }


    private function chatEventNewWords($utcDate, $authorName, $newWords) {

        $htmlNewWords = [];
        $totalPoints  = 0;

        foreach($newWords as $word) {
            $htmlNewWords[] = '<strong  class="word">'.ucfirst($word['word']).'</strong>';
            $totalPoints += $word['points'];
        }

        return '<p>'.$this->date($utcDate).' <strong>'.$authorName.'</strong> pose « '.join(', ', $htmlNewWords).' » '
             . '('.$totalPoints.' points)</p>';
    }
    
    
    private function chatEventBoardCleaned($isoDate, $authorName) {
        
        return '<p class="boardCleaned">'.$this->date($isoDate)
             . ' <strong>'.$authorName.'</strong> a touché un bord ! Remise à zéro du plateau...</p>';
    }
    
    
    private function chatEventOptionUsed($isoDate, $authorName, $optionAlias) {
        
        return '<p class="boardCleaned">'.$this->date($isoDate)
             . ' <strong>'.$authorName.'</strong> a utilisé l\'option <strong>'.$optionAlias.'</strong> !</p>';
    }
    
    
    private function chatEventMessage($isoDate, $authorName, $message) {
        
        return '<p class="green"><strong>'.$this->date($isoDate).' '.$authorName.'</strong>: '.$message.'</p>';
    }


    private function chatEventUnknown($eventType) {

        return '<p style="color:red">[Bug] Élément de tchat inconnu ("'.$eventType.'")</p>';
    }
}

