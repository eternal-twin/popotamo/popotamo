<?php

namespace Popotamo\Command;

use Popotamo\Config;
use Popotamo\Entity\Socket\Connection\AdminConnection;
use Popotamo\Entity\Socket\Connection\Connection;
use Popotamo\Entity\Socket\Connection\PopoConnection;
use Popotamo\Entity\Socket\Message;
use Popotamo\Entity\Socket\Response\JsonResponse;
use Popotamo\Entity\Socket\Response\Response;
use Popotamo\Entity\Socket\Server;
use Popotamo\Model\DbPopotamo;
use Popotamo\Model\GameProfile;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: "popotamo:socket", description: "Handle realtime events")]
class SocketCommand extends Command
{
    private InputInterface  $input;
    private OutputInterface $output;
    private Server          $server;
    private DbPopotamo      $db;

    public const COMMAND_QUIT           = 'quit';
    public const COMMAND_AUTH           = 'auth';
    public const COMMAND_INFO           = 'info';
    public const COMMAND_GAME_PROFILE   = 'newGameProfile';
    public const COMMAND_GAME           = 'setGame';
    public const COMMAND_CHAT           = 'chatAction';
    public const COMMAND_NEW_WORD       = 'newWord';
    public const COMMAND_PROFILE        = 'profile';
    public const COMMAND_BOARD_CLEAN    = 'boardClean';

    private const COMMAND_LIST = [
        self::COMMAND_QUIT => [
            'required_class' => Connection::class,
            'callback'       => 'quitCommand'
        ],
        self::COMMAND_AUTH => [
            'required_class' => Connection::class,
            'callback'       => 'authenticateCommand'
        ],
        self::COMMAND_INFO => [
            'required_class' => AdminConnection::class,
            'callback'       => 'infoCommand'
        ],
        self::COMMAND_GAME_PROFILE => [
            'required_class' => AdminConnection::class,
            'callback'       => 'newGameProfileCommand'
        ],
        self::COMMAND_GAME => [
            'required_class' => PopoConnection::class,
            'callback'       => 'setGameIDCommand'
        ],
        self::COMMAND_CHAT => [
            'required_class' => AdminConnection::class,
            'callback'       => 'chatActionCommand'
        ],
        self::COMMAND_NEW_WORD => [
            'required_class' => AdminConnection::class,
            'callback'       => 'newWordCommand'
        ],
        self::COMMAND_PROFILE => [
            'required_class' => AdminConnection::class,
            'callback'       => 'updateProfileCommand'
        ],
        self::COMMAND_BOARD_CLEAN => [
            'required_class' => AdminConnection::class,
            'callback'       => 'cleanBoardCommand'
        ]
    ];

    protected function configure()
    {
        $this
            ->addOption(name: 'port', mode: InputOption::VALUE_REQUIRED)
            ->addOption(name: 'time-limit', mode: InputOption::VALUE_REQUIRED)
            ->addOption(name: 'log', mode: InputOption::VALUE_REQUIRED)
            ->addOption(name: 'debug', mode: InputOption::VALUE_NONE)
        ;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input  = $input;
        $this->output = $output;

        if ($this->input->getOption('debug')) {
            ini_set('display_errors', 1);
            error_reporting(E_ALL);
        }

        //-- Empty log file
        if ($this->input->getOption('log')) {
            if (file_exists($this->input->getOption('log'))) {
                file_put_contents($this->input->getOption('log'), "");
            }
        }

        $port = intval($this->input->getOption('port')??Config::get('server.socketPort'));
        if ($port < 1 || $port > 65535) {
            $this->writeln("<error>You must choose a valid port to listen to.</error>");
            return self::FAILURE;
        }

        $this->db = new DbPopotamo();

        $this->writeln("<info>Welcome to the world of MCA Discovision</info>");
      
        $timeLimit = $input->hasOption('time-limit') ? $input->getOption('time-limit') : -1;
        $startTime = $_SERVER['REQUEST_TIME'];

        if ($timeLimit > 0) {
            $this->writeln("<info>This server is asked to stop in {$timeLimit} seconds</info>");

            $killTime  = $startTime + $timeLimit;
        }

        $this->server = new Server([
            'port' => $port
        ]);

        //-- Boot sequence. Will yield messages as it tries to bind the socket to the requested port
        foreach ($this->server->init() as $message) {
            if (is_string($message)) {
                $this->output->write("\r{$message}");
            }

            //-- Boot done. Either bound or failed
            if (is_bool($message)) {
                $this->writeln("");

                if (!$message) {
                    $this->writeln("<error>Failed to bind address</error>");
                    return self::FAILURE;
                }
            }
        }
        $this->writeln("Address bound. Listening to port {$this->server->getPort()}");

        while (true) {
            if ($timeLimit > 0 && time() >= $killTime) {
                $this->writeln("My time here is done. See ya !");
                break;
            }

            //-- Accepts new sockets
            if ($connection = $this->server->accept()) {
                $this->writeln("New socket connected");
            }

            foreach ($this->server->receive() as $message) {
                $this->writeln("<info>RECEIVED :</info> {$message->getContent()}");

                $response   = $this->handleCommand($message);
                $connection = $message->getConnection();

                if ($response instanceof JsonResponse) {
                    $this->writeln("<info>Status :</info> " . ($response->getContent(true)['success'] ? "Success" : "Failed"));
    
                    if ($response->getContent()['message']??false) {
                        $this->writeln("<info>Status :</info> {$response->getContent(true)['message']}");
                    }
                }

                if ($response->getCode() & Response::CODE_RESPONSE) {
                    $connection->send($response);
                }

                if ($response->getCode() & Response::CODE_KILL) {
                    if ($connection instanceof PopoConnection) {
                        $this->writeln("Player leaving");
                        unset($this->clients[$connection->getProfile()->getFrontToken()]);
                    }
                    
                    $this->writeln("Socket closing");
                    $this->server->closeConnection($connection);
                }
            }
        }
        
        $this->server->close();
        return self::SUCCESS;
    }

    public function handleCommand(Message $message): Response
    {
        //-- Reserved commands namespace
        switch ($message->getContent()) {
            case 'PING':
                $this->writeln("Got PING");
                return new Response("PONG", Response::CODE_RESPONSE + Response::CODE_PONG);

            case 'PONG':
                $this->writeln("Got PONG");
                return new Response("", Response::CODE_RESPONSE);
        }

        $content = $message->json();
        if (!is_array($content)) {
            $this->writeln("<error>Invalid raw content :</error> {$message->getContent()}");
            return new JsonResponse(['success' => false, 'code' => 1, 'message' => "Invalid command"]);
        }

        $this->writeln("<info>Command issued :</info> {$content['command']}");

        $command = self::COMMAND_LIST[$content['command']] ?? false;
        $this->writeln("<info>Class :</info> ".get_class($message->getConnection()));

        if ($command === false) {
            return new JsonResponse(['success' => false, 'code' => 1, 'message' => "What did you say ?"]);
        }

        if (!($message->getConnection() instanceof $command['required_class'])) {
            return new JsonResponse(['success' => false, 'code' => 1, 'message' => "Unauthorized access"]);
        }

        return call_user_func_array([$this, $command['callback']], [$message]);
    }

    /*
     * Socket commands 
     */

    private function authenticateCommand(Message $message): Response
    {
        $jsonData = $message->json();
        $auth     = $jsonData['auth'];

        //-- Admin/Supervisor authentication
        //TODO check private key for authentication
        if ($auth['username'] === 'supervisor') {
            if ($message->getConnection()->getAddress() !== '127.0.0.1') {
                return new Response("Authentication failed", Response::CODE_RESPONSE + Response::CODE_KILL);
            }

            $adminConnection = $this->server->upgradeConnection($message->getConnection(), "AdminConnection");
            $adminConnection->setUsername("Supervisor");

            return new JsonResponse(['success' => true, 'eventType' => 'auth', 'message' => "Welcome back, Supervisor !", "private" => $adminConnection->getId()]);
        }

        $this->db->connect();
        $profile = $this->db->getProfileFromFrontToken($auth['password']);

        if ($profile === null) {
            return new Response("Authentication failed", Response::CODE_RESPONSE + Response::CODE_KILL);
        }

        if ($profile->getDisplayName() !== $auth['username']) {
            return new Response("Authentication failed", Response::CODE_RESPONSE + Response::CODE_KILL);
        }

        $popoConnection = $this->server->upgradeConnection($message->getConnection(), "PopoConnection")
            ->setProfile($profile)
        ;

        if (isset($jsonData['payload']['gameID'])) {
            $gameID = $jsonData['payload']['gameID'];
            
            $this->writeln("Attempting to join game {$gameID}");
            /** @var GameProfile $gameProfile */
            $gameProfile = $popoConnection->getProfile()->getGameProfileFromGameId($gameID);
    
            if ($gameProfile) {
                $this->server->room("game/{$gameID}")->send(new JsonResponse(['eventType' => 'playerConnected', 'profile' => [
                    'id' => $popoConnection->getProfile()->getId()
                ]]));
                $this->server->room("game/{$gameID}")->addConnection($popoConnection);

                $activeProfiles = array_map(function($connection) { return $connection->getProfile()->getId(); }, $this->server->room("game/{$gameID}")->jsonSerialize()['connections']);

                $popoConnection->send(new JsonResponse(['eventType' => 'activePlayers', 'profiles' => $activeProfiles]));
            }
        }

        return new JsonResponse(['success' => true, 'eventType' => 'auth', 'message' => "Hello, {$popoConnection->getProfile()->getDisplayName()} !"]);
    }

    private function quitCommand(Message $message): Response
    {
        return new Response("Goodbye", Response::CODE_RESPONSE + Response::CODE_KILL);
    }

    private function infoCommand(Message $message): Response
    {
        return new JsonResponse(['success' => true, 'eventType' => 'info', 'message' => "Server status", 'data' => $this->server]);
    }

    private function newGameProfileCommand(Message $message): Response
    {
        $jsonData = $message->json();

        $jsonData['payload']['eventType'] = 'newProfile';

        $this->server->room("game/{$jsonData['payload']['gameID']}")->send(new JsonResponse($jsonData['payload']));

        return new JsonResponse(['success' => true, 'eventType' => 'newProfile', 'message' => "OK"]);
    }

    private function setGameIDCommand(Message $message): Response
    {
        $gameID      = $message->json()['payload']['gameID'];
        $gameProfile = $message->getConnection()->getProfile()->getGameProfileFromGameId($gameID);

        if ($gameProfile) {
            $this->server->room("game/{$gameID}")->addConnection($message->getConnection());
        }
        
        return new JsonResponse(['success' => true, 'eventType' => 'setGame', 'message' => "OK"]);
    }

    private function chatActionCommand(Message $message): Response
    {
        $jsonData = $message->json();
        $room     = $jsonData['room'];

        foreach ($jsonData['payload'] as $payload) {
            $this->server->room($room)->send(new JsonResponse([
                'eventType' => 'chatMessage',
                'payload'   => [$payload]
            ]));
        }
   
        return new JsonResponse(['success' => true, 'eventType' => 'chatMessage', 'message' => "OK"]);
    }

    private function newWordCommand(Message $message): Response
    {
        $jsonData = $message->json();
        $room     = $jsonData['room'];

        $this->server->room($room)->send(new JsonResponse(['eventType' => 'newWords', 'newWords' => $jsonData['payload']]));
        return new JsonResponse(['success' => true, 'eventType' => 'newWord', 'message' => "OK"]);
    }

    private function cleanBoardCommand(Message $message): Response
    {
        return new JsonResponse(['success' => true, 'eventType' => 'cleanBoard', 'message' => "OK"]);
    }

    private function updateProfileCommand(Message $message): Response
    {
        $jsonData = $message->json();

        $this->server->room($jsonData['room'])->send(new JsonResponse(['eventType' => 'updateProfile', 'profile' => $jsonData['payload']]));
        return new JsonResponse(['success' => true, 'eventType' => 'updateProfile', 'message' => "OK"]);
    }

    private function writeln(string $message) 
    {
        $this->output->writeln($message ? "[".date('H:i:s')."] {$message}" : "");
    }
}