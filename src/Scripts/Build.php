<?php declare(strict_types=1);

namespace Popotamo\Scripts;

use Popotamo\Config;
use Popotamo\Kernel;
use Popotamo\Service\Console;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

final class Build {
    /**
     * Build the website. Currently this only corresponds to pre-processing
     * the Popotamo dictionary
     */
    final public static function build(): void {
        $projectRoot = realpath(__DIR__ . "/../..");

        Console::setChannel('console', [
            'output' => Console::OUTPUT_CLI,
            'format' => Console::FORMAT_CLI
        ]);
        Console::setChannel('healthcheck', [
            'output' => Console::OUTPUT_FILE,
            'format' => Console::FORMAT_LOG,
            'path'   => Kernel::getLogDir()."/healthcheck.log"
        ]);

        self::resetDico(true);
        self::buildAssets();
    }

    private static function resetDico(bool $forceCut = false): void {
        Console::info("Building popodico", ['console']);
        $projectRoot = __DIR__ . "/../..";
        $dicoDir = $projectRoot . "/_POPODICO";
        $dico = file_get_contents($dicoDir . "/popodico-ODS5.txt");
        $cutDir = $dicoDir . "/cut-by-letter";
        if (file_exists($cutDir)) {
            if (!$forceCut) {
                $confirmed = Console::confirm("Target directory " . realpath($cutDir) . " already exists. Delete and recreate it?");
                if (!$confirmed) {
                    Console::writeln("Aborting.", ['console']);
                    Console::warning("Popodico generation aborted.", ['healthcheck']);
                    return;
                }
            }
            self::removeDirRec($cutDir);
        }
        mkdir($cutDir);
        $words = explode("\n", $dico);
        /** @var string? $letter */
        $letter = null;
        $letterList = [];
        foreach($words as $word) {
            if ($word == "") {
                continue;
            }
            $curLetter = $word[0];
            if ($curLetter != $letter) {
                if ($letter != null) {
                    self::writeLetterFile($cutDir, $letter, $letterList);
                }
                $letter = $curLetter;
                $letterList = [];
            }
            $letterList[] = $word;
        }
        if ($letter != null) {
            self::writeLetterFile($cutDir, $letter, $letterList);
        }
        Console::success("Building popodico: done", ['console']);
        Console::success("Popodico successfully generated", ['healthcheck']);
    }

    private static function buildAssets(): void {
        Console::info("Building assets: gulp deploy", ['console']);
        $projectRoot = __DIR__ . "/../..";
        $command = ["yarn", "run", "build"];
        $descriptors = [
            0 => STDIN,
            1 => STDOUT,
            2 => STDERR,
        ];
        $pipes = [];
        $proc = proc_open(
            $command,
            $descriptors,
            $pipes,
            $projectRoot,
            null,
            [
                "suppress_errors" => false,
                "bypass_shell" => true,
                "blocking_pipes" => false,
                "create_process_group" => true,
                "create_new_console" => false,
            ]
        );

        if ($proc === false) {
            Console::error("Failed to spawn gulp child process", ['console', 'healthcheck']);
            die(1);
        }

        $out = proc_close($proc);
        if ($out != 0) {
            Console::error("`gulp deploy` closed with non-zero exit code: " . $out, ['console']);
            Console::error("Gulp process closed with non-zero exit code", ['healthcheck']);
            die(1);
        }

        Console::success("Building assets: done", ['console']);
        Console::success("Assets deployed", ['healthcheck']);
    }

    private static function removeDirRec(string $dir): void {
        $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator($it, RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);
    }

    private static function writeLetterFile(string $dir, string $letter, array $words): void {
        $file = $dir . "/" . $letter . ".txt";
        file_put_contents($file, implode("\n", $words) . "\n");
    }
}
