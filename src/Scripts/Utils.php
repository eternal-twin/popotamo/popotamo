<?php

namespace Popotamo\Scripts;

use Popotamo\Service\Console;

final class Utils
{
    /**
     * Take the raw password and returns a hashed one using Argon
     *
     * TODO Prevent this command from being saved in host history
     * 
     * @param \Composer\Script\Event $event
     * @return void
     */
    public static function hashPassword(\Composer\Script\Event $event)
    {
        $rawPassword = $event->getArguments()[0];

        Console::setChannel('console', []);

        Console::writeln(">---------------------------------<");
        Console::writeln("");
        Console::writeln("Your hashed password :");
        Console::writeln(password_hash($rawPassword, PASSWORD_ARGON2ID));
        Console::writeln("");
        Console::writeln(">---------------------------------<");
        
        return 0;
    }
}