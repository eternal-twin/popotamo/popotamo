<?php declare(strict_types=1);

namespace Popotamo\Scripts;

use Pgsafari\MigrationDirection;
use Pgsafari\Safari;
use Pgsafari\SchemaMeta;
use Popotamo\Config;
use Popotamo\Model\Skill;
use Popotamo\Service\Console;

final class Db {
    /**
     * Reset the database: drop the whole database, then initialize it.
     *
     * This may be useful during development, avoid using it in production...
     */
    final public static function reset(): void {
        self::innerSync(true);
    }

    /**
     * Ensure the database has the latest schema and is populated with the initial data.
     */
    final public static function sync(): void {
        self::innerSync(false);
    }

    private static function innerSync(bool $dropData): void {
        Console::setChannel('console', [
            'output' => Console::OUTPUT_CLI,
            'format' => Console::FORMAT_CLI
        ]);
        
        $safari = self::getSafari();
        $pdo = new \PDO(Config::dsn(), Config::get('db.login'), Config::get('db.password'));
        $curMeta = SchemaMeta::read($pdo);
        if ($curMeta == null) {
            echo "Current database was never initialized.\n";
            $curVersion = $safari->emptyVersion();
        } else {
            $curVersion = $curMeta->version;
            echo "Current database schema version: " . $curVersion . "\n";
            if ($dropData) {
                echo "DANGER:\n";
                echo "THE WHOLE DATABASE WILL BE DROPPED.\n";
                echo "ALL DATA WILL BE LOST.\n";
                $confirmed = Console::confirm("Drop the database?");
                if (!$confirmed) {
                    echo "Aborting.\n";
                    die(0);
                }
                echo "Dropping the database\n";
                $emptyVersion = $safari->emptyVersion();
                $migration = $safari->createMigration($curVersion, $emptyVersion, MigrationDirection::downgrade());
                $migration->exec($pdo);
                echo "Database dropped.\n";
                $curVersion = $emptyVersion;
                echo "Current database schema version: " . $curVersion . "\n";
            }
        }
        $latestVersion = $safari->latestVersion();
        echo "Latest available schema version: " . $latestVersion . "\n";
        if ($curVersion == $latestVersion) {
            echo "Database schema is up-to-date.\n";
        } else {
            $migration = $safari->createMigration($curVersion, $latestVersion, MigrationDirection::upgrade());
            $states = [$migration->start];
            foreach ($migration->transitions as $t) {
                $states[] = $t->end;
            }
            echo "Schema migration plan:\n";
            echo implode(" -> ", $states) . "\n";
            $confirmed = Console::confirm("Apply schema migration?");
            if (!$confirmed) {
                echo "Aborting.\n";
                die(0);
            }
            echo "Applying schema migration.\n";
            $migration->exec($pdo);
            echo "Schema migration complete.\n";
        }
        echo "Synchronizing initial database data.\n";
        self::populate($pdo);
        echo "Initial data synchronization complete.\n";
    }

    private static function populate(\PDO $pdo): void {
        if (!$pdo->beginTransaction()) {
            $info = $pdo->errorInfo();
            throw new \Exception("failed to begin transaction: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
        }
        self::txPopulate($pdo);
        if (!$pdo->commit()) {
            $info = $pdo->errorInfo();
            throw new \Exception("failed to commit transaction: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
        }
    }

    private static function txPopulate(\PDO $pdo): void {
        self::txPopulateSkills($pdo);
    }

    private static function txPopulateSkills(\PDO $pdo): void {
        $allSkills = [];
        foreach (Skill::all() as $skill) {
            $allSkills[$skill->id] = $skill;
        }
        $stmt = $pdo->query("SELECT skill_id FROM popo_skills;");
        if ($stmt === false) {
            $info = $pdo->errorInfo();
            throw new \Exception("failed to create current skill query: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
        }
        $rows = $stmt->fetchAll();
        if ($rows === false) {
            $info = $pdo->errorInfo();
            throw new \Exception("failed to fetch current skills: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
        }
        $stmt = $pdo->prepare("DELETE FROM popo_skills WHERE skill_id = :skillId;");
        if ($stmt === false) {
            $info = $pdo->errorInfo();
            throw new \Exception("failed to create extra skill deletion statement: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
        }
        foreach ($rows as $row) {
            if (array_key_exists($row["skill_id"], $allSkills)) {
                continue;
            }
            $res = $stmt->execute([
                "skillId" => $row["skill_id"],
            ]);
            if ($res === false) {
                $info = $pdo->errorInfo();
                throw new \Exception("failed to delete extra skill: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
            }
        }

        $stmt = $pdo->prepare("
            INSERT INTO popo_skills(skill_id, skill_type, alias, name, icon_text, icon_img, description, popup_type, quids_cost, max_level)
            VALUES(:skillId, :skillType, :alias, :name, :iconText, :iconImg, :description, :popupType, :quidsCost, :maxLevel)
            ON CONFLICT (skill_id) DO UPDATE SET
              skill_id = :skillId,
              skill_type = :skillType,
              alias = :alias,
              name = :name,
              icon_text = :iconText,
              icon_img = :iconImg,
              description = :description,
              popup_type = :popupType,
              quids_cost = :quidsCost,
              max_level = :maxLevel;
        ");
        if ($stmt === false) {
            $info = $pdo->errorInfo();
            throw new \Exception("failed to create skill statement: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
        }
        foreach ($allSkills as $skillId => $skill) {
            $res = $stmt->execute([
                "skillId" => $skillId,
                "skillType" => $skill->type,
                "alias" => $skill->alias,
                "name" => $skill->name,
                "iconText" => $skill->iconText,
                "iconImg" => $skill->iconImg,
                "description" => $skill->description,
                "popupType" => $skill->popupType,
                "quidsCost" => $skill->quidsCost,
                "maxLevel" => $skill->maxLevel,
            ]);
            if ($res === false) {
                $info = $pdo->errorInfo();
                throw new \Exception("failed to create skill statement: " . $info[0] . ", " . $info[1] . ", " . $info[2]);
            }
        }
    }

    private static function getSafari(): Safari {
        $projectRoot = __DIR__ . "/../..";
        $dbDir = $projectRoot . "/db";
        return Safari::fromDirectory($dbDir);
    }
}
