<?php

namespace Popotamo\Service;

class Session
{
    public function __construct()
    {
        
    }

    public function __destruct()
    {
        if (session_status() === PHP_SESSION_ACTIVE) {
            session_write_close();
        }
    }

    private function checkSessionState()
    {
        if (session_status() === PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function empty()
    {
        $this->checkSessionState();
 
        return empty($_SESSION);
    }

    public function has($key): bool
    {
        $this->checkSessionState();

        return array_key_exists($key, $_SESSION);
    }

    public function get($key, $default=null)
    {
        $this->checkSessionState();

        return array_key_exists($key, $_SESSION) ? $_SESSION[$key] : $default;
    }

    public function all()
    {
        return $_SESSION;
    }

    public function set($key, $value)
    {
        $this->checkSessionState();

        return $_SESSION[$key] = $value;
    }

    /**
     * We automatically disconnect the user after 3600 seconds
     *
     * @return void
     */
    public function unsetIfStale()
    {
        if ($this->has('sessionStart') && (time() - (int)$this->get('sessionStart') > 60*60)) {
            $this->destroy();
        }
    }

    public function destroy()
    {
        session_destroy();
        unset($_SESSION);
    }

    public function setReadOnly()
    {
        session_write_close();
    }
}