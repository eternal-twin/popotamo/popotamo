<?php

namespace Popotamo\Service;

use PDO;
use Popotamo\Model\DbPopotamo;

class MessageQueue
{
    public const EVENT_CHAT        = 'new_chat';
    public const EVENT_WORD        = 'new_word';
    public const EVENT_PROFILE     = 'update_profile';
    public const EVENT_BOARD_CLEAN = 'board_clean';
    public const EVENT_GAME_START  = 'game_start';

    private $pdo;
    private $lastChecked;

    public function __construct()
    {
        $this->pdo         = (new DbPopotamo())->connect();
        $this->lastChecked = time();
    }

    public function pushMessage($channel, $event, $payload)
    {
        $this->pdo->exec("NOTIFY {$channel}, {$this->pdo->quote(json_encode([['timestamp' => time(), 'event' => $event, 'data' => $payload]]))}");
    }

    public function subscribe($channel)
    {
        $this->pdo->exec("LISTEN {$channel}");
    }

    public function getNewMessages()
    {
        $messages          = $this->pdo->pgsqlGetNotify(PDO::FETCH_ASSOC); 
        $newMessages       = []; 

        if ($messages === false) {
            return false;
        }

        foreach ($messages as $message) {
            if ($message['timestamp'] < $this->lastChecked) {
                continue;
            }

            $this->lastChecked = $message['timestamp'];
            $newMessages[] = $message;
        }

        return $newMessages;
    }

    public function waitForNewMessage($timeout)
    {
        $envelope = $this->pdo->pgsqlGetNotify(PDO::FETCH_ASSOC, $timeout); 

        if ($envelope === false) {
            return false;
        }
        
        return $envelope;
    }  
}