<?php

namespace Popotamo\Service;

use SplFileObject;

class Console {
    private static $maxLength  = 0;

    public const OUTPUT_CLI  = 1;
    public const OUTPUT_FILE = 2;

    public const FORMAT_CLI = 1;
    public const FORMAT_LOG = 2;

    protected const COLORS = [
        self::FORMAT_CLI => [
            'debug'   => ["\033[1;32;40m", "\033[0m"],
            'success' => ["\033[0;32m", "\033[0m"],
            'info'    => ["\033[0;35m", "\033[0m"],
            'warning' => ["\033[0;33m", "\033[0m"],
            'error'   => ["\033[0;31m", "\033[0m"],
        ],
        self::FORMAT_LOG => [
            'debug'   => ["[DEBUG]   ", ""],
            'success' => ["[SUCCESS] ", ""],
            'info'    => ["[INFO]    ", ""],
            'warning' => ["[WARNING] ", ""],
            'error'   => ["[ERROR]   ", ""],
        ]
    ];

    protected static array $channels = [];

    public static function setChannel($channel, $config) 
    {
        self::$channels[$channel] = array_merge([
            'output' => self::OUTPUT_CLI,
            'format' => self::FORMAT_CLI,
        ], $config);

        if (self::$channels[$channel]['output'] == self::OUTPUT_FILE) {
            self::$channels[$channel]['fileObject'] = new SplFileObject(self::$channels[$channel]['path'], 'w+');
        }
    }

    public static function output($message, ?array $channels) {
        if (empty($channels)) {
            $channels = array_keys(self::$channels);
        }

        foreach ($channels as $channel) {
            $config = self::$channels[$channel]??false;

            if ($config) {
                self::format($message, $config);
            }
        }
    }

    public static function debug($message, ?array $channels=null) {
        self::writeln("<debug>$message</debug>", $channels);
    }

    public static function info($message, ?array $channels=null) {
        self::writeln("<info>$message</info>", $channels);
    }

    public static function warning($message, ?array $channels=null) {
        self::writeln("<warning>$message</warning>", $channels);
    }

    public static function error($message, ?array $channels=null) {
        self::writeln("<error>$message</error>", $channels);
    }

    public static function success($message, ?array $channels=null) {
        self::writeln("<success>$message</success>", $channels);
    }

    public static function write(string $message, ?array $channels=null) {
        self::$maxLength += strlen($message);
        self::output($message, $channels);
    }

    public static function writeln($message, ?array $channels=null) {
        self::$maxLength = 0;
        if(!is_array($message)) {
            $message = [$message];
        }

        self::write(implode("\n", $message)."\n", $channels);
    }

    public static function writeReplace(string $message, ?array $channels=null) {
        self::output("\r".str_repeat(" ", self::$maxLength), $channels);
        self::output("\r{$message}", $channels);
        self::$maxLength = max(self::$maxLength, strlen($message));
    }

    public static function confirm(string $message, bool $defaultYes = false): bool {
        if(in_array("--no-confirm", $_SERVER['argv'])) {
            return true;
        }

        for (;;) {
            self::write("{$message} [yN] ");
            $input = trim(fgets(STDIN));
            switch (strtolower($input)) {
                case "n":
                case "no":
                    return false;
                case "y":
                case "yes":
                    return true;
                default:
                    break;
            }
        }
    }

    public static function format(string $message, array $config): void {
        switch ($config['output']) {
            case self::OUTPUT_CLI:
                echo self::walk($message, $config['output']);
                break;

            case self::OUTPUT_FILE:
                $config['fileObject']->fwrite(self::walk($message, $config['output']));
        }
    }

    protected static function walk(string $str, int $output): string {
        $colorDepth = [];
        $depth = 0;
        $message = "";
        foreach(str_split($str) as $char) {
            $message .= $char;

            foreach(self::COLORS[$output] as $key => [$openColor, $closeColor]) {
                if(preg_match("/<$key>$/", $message)) {
                    $depth++;

                    $colorDepth[$depth] = $openColor;
                    $message = preg_replace("/<$key>$/", $colorDepth[$depth], $message);
                }
                elseif(preg_match("/<\/$key>$/", $message)) {
                    $depth--;

                    if($depth > 0) {
                        $message = preg_replace("/<\/$key>$/", $colorDepth[$depth], $message);
                    }
                    else {
                        $message = preg_replace("/<\/$key>$/", $closeColor, $message);
                    }
                }
            }
        }

        return $message;
    }
}