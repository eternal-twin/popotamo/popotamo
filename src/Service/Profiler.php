<?php

namespace Popotamo\Service;

use PDOException;

class Profiler
{
    private static array $queries = [];

    public static function pushQuery($sql, $params, $duration, PDOException $e = null)
    {
        self::$queries[] = [
            'sql'      => $sql,
            'params'   => $params,
            'duration' => $duration,
            'error'    => $e ? $e->getMessage() : false
        ];
    }

    public static function getMemoryPeakUsage()
    {
        return self::bytesToHuman(memory_get_peak_usage(true));
    }

    public static function getMemoryLimit(string $unit = null)
    {
        $memoryLimit = ini_get('memory_limit');

        switch (substr($memoryLimit, -1)) {
            case "M":
                $memoryLimit = ((int) substr($memoryLimit, 0, strlen($memoryLimit) -1 )) * 1024 * 1024;
                break;

            case "K":
                $memoryLimit = ((int) substr($memoryLimit, 0, strlen($memoryLimit) -1 )) * 1024;
                break;

            default:
                $memoryLimit = intval($memoryLimit);
        }

        return self::bytesToHuman($memoryLimit, $unit??false);
    }

    public static function getExecutionTime()
    {
        return round(microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'], 6);
    }

    public static function getQueries()
    {
        return self::$queries;
    }

    private static function bytesToHuman(int $bytes, int $dec = 0, string|false $forceUnit = false)
    {
        $size   = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor == 0) $dec = 0;


        return sprintf("%.{$dec}f %s", $bytes / (1024 ** $factor), $size[$factor]);
    }
}