<?php

namespace Popotamo\Service;

class Response 
{
    public const TYPE_HTML = 1;
    public const TYPE_JSON = 2;

    public function __construct(private mixed $data = "", private int $type = self::TYPE_HTML) {}

    public function append(mixed $data)
    {
        switch ($this->type) {
            case self::TYPE_HTML:
                $this->data .= $data;
                break;

            case self::TYPE_JSON:
                $this->data = array_merge($this->data, $data);
                break;
        }
    }

    public function send()
    {
        ob_end_clean();
        ob_start();
        switch ($this->type) {
            case self::TYPE_HTML:
                echo $this->data;
                break;

            case self::TYPE_JSON:
                echo json_encode($this->data);
                break;
        }

        ob_end_flush();
    }
}