<?php

namespace Popotamo\Service;

class Request 
{
    private array $request;
    private array $query;
    private array $headers;

    public function __construct()
    {  
        $this->load();
    }

    private function load()
    {
        $this->headers = getallheaders();
        $this->query   = $_GET;
        $this->request = $_POST;
    }

    public function getRequest($key, $default=null)
    {
        return $this->request[$key]??$default;
    }

    public function getQuery($key, $default=null)
    {
        return $this->query[$key]??$default;
    }

    public function getHeader($key, $default=null)
    {
        return $this->headers[$key]??$default;
    }

    public function hasHeader($key)
    {
        return array_key_exists($key, $this->headers);
    }

    public function hasQuery($key)
    {
        return array_key_exists($key, $this->query);
    }

    public function hasRequest($key)
    {
        return array_key_exists($key, $this->request);
    }

    public function getScheme()
    {
        return $_SERVER['REQUEST_SCHEME'];
    }
}