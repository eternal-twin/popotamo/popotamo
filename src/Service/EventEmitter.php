<?php

namespace Popotamo\Service;

use Exception;
use WebSocket\Client;
use WebSocket\ConnectionException;

class EventEmitter
{
    private static $client    = null;
    private static $failstate = false;

    public static function emit(array $payload)
    {
        if (self::$failstate) {
            return false;
        }

        if (!self::$client) {
            try {
                self::$client = new Client("ws://127.0.0.1:12000/", [
                    'timeout'    => 10
                ]);
                
                //TODO change username/password to be secure
                self::$client->text(json_encode(['command' => 'auth', 'auth' => ['username' => 'supervisor', 'password' => 'test'], 'payload' => []]));
                self::$client->receive();
            } catch (ConnectionException $e) {
                self::$failstate = true;
                return false;
            }
        }

        try {
            self::$client->text(json_encode($payload));
            return json_decode(self::$client->receive(), true);
        } catch (Exception) {}

        return false;
    }

    public static function __callStatic($name, $arguments=null)
    {
        return self::emit(['command' => $name, 'payload' => $arguments[0]??[]]);
    }

    public function __destruct()
    {
        if (self::$client) {
            self::$client->disconnect();
        }
    }
}