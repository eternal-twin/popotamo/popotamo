<?php

use \Popotamo\Model\DbPopotamo;
use \Popotamo\View\Html;
use \Popotamo\Controller\Calc;
use \Popotamo\Controller\Filter;
use Popotamo\Model\Inventory;
use Popotamo\Router;

// Basic score required to win, without any handicap
$basicScore = 250;
// Action points of the player when he starts a new game
$startingAP = 160;

$profileId = $this->getUser()->getProfile()->getId();

$DbPopotamo = new DbPopotamo();
$Html       = new Html();
$Calc       = new Calc();
$Filter     = new Filter();

$DbPopotamo->connect();
$profile = $DbPopotamo->getProfile($profileId);

// If the player wants to join a game
if(!empty($_POST)) {
    $gameId         = $_POST['gameId']??null;
    $skills         = $Filter->getProfileSkills($profile, $profileId);
    $nbrCompetences = $Calc->competencesPoints($skills['competences']);
    $scoreGoal      = $Calc->scoreToWin($basicScore, $nbrCompetences);

    if ($_POST['action'] === 'launch') {
        $gameId = $DbPopotamo->findRandomAvailableGame($profileId);

        if (!$gameId) {
            $gameId = $DbPopotamo->createNewGame();
        }
    } elseif ($_POST['action'] === 'join') {
        // Initialize a new game if it doesn't exist
        $DbPopotamo->ensureGameExists($gameId);
    }

    // Register the player in this game
    $DbPopotamo->joinGame($gameId, $profileId, $scoreGoal, $startingAP);
    $DbPopotamo->disconnect();

    Router::redirectTo("/game/{$gameId}");
}
else {
    // Set default values for the skills the player doesn't own
    $profileSkills = $Filter->getProfileSkills($profile);
}

$html = [
    'profile'   => $Html->profileInsert($profile, $profileSkills),
    'menu'      => $Html->menu(),
];

$headers = <<<HTML
<link rel="preload" as="image" href="/resources/img/currentgame.gif" />
<link rel="preload" as="image" href="/resources/img/currentgame_hover.gif" />
HTML;

echo $Html->header($headers);
echo $html['profile'];
echo $html['menu'];
?>
    <main id="newgame">

        <h2>Jouer</h2>

        <h3>Parties en cours</h3>

        <div id="currentgames">
            <?php foreach ($profile->getGameProfiles() as $currentGameProfile) { 
                $inventory    = new Inventory(null, $profileId, $currentGameProfile->game->gameId);
                $gameDuration = "";
                ?>
                <a href="/game/<?= $currentGameProfile->game->gameId ?>">
                    <table>
                        <tr>
                            <td style="width:50%">
                                <div class="">
                                    <div class="icon"></div>
                                    <div class="value"><?= $gameDuration ?></div>
                                </div>
                                <div class="">
                                    <div class="icon"></div>
                                    <div class="value"><?= $currentGameProfile->getCurrentActionPoints() ?></div>
                                </div>
                            </td>
                            <td>
                                <div class="letters">
                                    <span><?= implode('</span><span>', $inventory->getInventory()) ?></span>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a style="padding-left:4em">Afficher</a></td>
                        </tr>
                    </table>
                </a>
                <?php
            } ?>
        </div>

        <br>

        <h3>Démarrer une nouvelle partie </h3>

        <form method="post" action="">

            <input type="hidden" name="action" value="launch">

            <button type="submit" name="type" value="normal">
                <figure>
                    <img src="resources/img/game_normal.gif"
                           alt="Lancer une partie normale"
                           width="161" height="105">
                    <figcaption>En partie normale, vous obtenez 1 point d'action par minute</figcaption>
                </figure>
            </button>

            <button type="submit" name="type" value="rapide" disabled>
                <figure>
                    <img src="resources/img/game_fast_disabled.gif"
                           alt="Lancer une partie rapide"
                           title="Les parties rapides ne sont pas encore implémentées. A suivre..."
                           width="161" height="105">
                    <figcaption>En partie rapide, vous obtenez 5 points d'action par minute</figcaption>
                </figure>
            </button>

            <button type="submit" name="type" value="amical_normal" disabled>
                <figure>
                    <img src="resources/img/game_friendlynormal_disabled.gif"
                           alt="Lancer une partie amicale normale"
                           title="Les parties amicales ne sont pas encore implémentées. A suivre..."
                           width="159" height="50">
                    <figcaption>Invitez les joueurs de votre choix dans une partie normale</figcaption>
                </figure>
            </button>

            <button type="submit" name="type" value="amical_rapide" disabled>
                <figure>
                    <img src="resources/img/game_friendlyfast_disabled.gif"
                           alt="Lancer une partie amicale rapide"
                           title="Les parties amicales ne sont pas encore implémentées. A suivre..."
                           width="159" height="50">
                    <figcaption>Invitez les joueurs de votre choix dans une partie rapide</figcaption>
                </figure>
            </button>

        </form>
    </main>

<?php echo $Html->footer();
    $DbPopotamo->disconnect();
?>
