<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Internal Server Error</title>
        <style>
            html, body {
                margin: 0;
                padding: 0;
                text-align: center;
            }

            h1 {
                margin-top: 100px;
                font-size: 86px;
                margin-bottom: 0;
            }
        </style>
    </head>
    <body>
        <h1>500</h1>
        <h2>Error</h2>
        <h3>Internal Server Error</h3>
    </body>
</html>