<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Not Found</title>
        <style>
            html, body {
                margin: 0;
                padding: 0;
                text-align: center;
            }

            h1 {
                margin-top: 100px;
                font-size: 86px;
                margin-bottom: 0;
            }
        </style>
    </head>
    <body>
        <h1>404</h1>
        <h2>Error</h2>
        <h3>Not Found</h3>
    </body>
</html>