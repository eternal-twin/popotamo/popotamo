<?php

use Popotamo\View\Html;

$Html = new Html();

?>
<?= $Html->header('
    <link rel="stylesheet" href="/resources/admin.css">
'); ?>
<?php include 'menu.php'; ?>
<main>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>état</th>
                <th>création</th>
                <th>Joueurs</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($games as $game) { ?>
                <tr>
                    <td><?= $game['game_id'] ?></td>
                    <td><?= ($game['nb_players'] == 5 ? ($game['date_end'] ? 'Terminée' : 'En cours') : 'En attente') ?></td>
                    <td><?= $game['date_start'] ?></td>
                    <td>
                        <table>
                            <thead>
                                <tr>
                                    <th>Display name</th>
                                    <th>Score/Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($game['players'] as $player) { ?>
                                    <tr>
                                        <td><?= $player['display_name'] ?></td>
                                        <td><?= $player['score'] ?>/<?= $player['score_goal'] ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</main>
<?= $Html->footer(); ?>
