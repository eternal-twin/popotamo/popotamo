<?php

use Popotamo\View\Html;

$Html = new Html();

function secondsToHuman($time)
{
    $nbDays     = 0;
    $nbHours    = 0;
    $nbMinutes  = 0;
    $nbSeconds  = 0;

    if ($time >= 86400) {
        $nbDays = floor($time / 86400);
        $time -= ($nbDays * 86400);
    }
    if ($time >= 3600) {
        $nbHours = floor($time / 3600);
        $time -= ($nbHours * 3600);
    }
    if ($time >= 60) {
        $nbMinutes = floor($time / 60);
        $time -= ($nbMinutes * 60);
    }

    $nbSeconds = $time;

    return ($nbDays ? "{$nbDays}j " : "").($nbHours ? "{$nbHours}h " : "").($nbMinutes ? "{$nbMinutes}m " : "")."{$nbSeconds}s";
}


function byteToHuman(int $bytes, int $dec = 0)
{
    $size   = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor == 0) $dec = 0;


    return sprintf("%.{$dec}f %s", $bytes / (1024 ** $factor), $size[$factor]);
}

?>
<?= $Html->header('
    <link rel="stylesheet" href="/resources/admin.css">
'); ?>
<?php include 'menu.php'; ?>
<main id="adminSocket">
    <h1>WebSocket</h1>
    <div class="info-wrapper">
        <p><label>État</label><span><?= $server ? "En ligne" : "Hors service" ?></span><?= !$server ? ' <a href="/admin/socket?start-server">Démarrer</a>' : '' ?></p>
        <p><label>Port</label><span><?= $server['port']??"" ?></span></p>
        <?php if ($pid) { ?><p><label>PID</label><span><?= $pid ?></span></p><?php } ?>
        <p><label>Uptime</label><span><?= $server ? secondsToHuman($server['uptime']) : "" ?></span></p>
        <p><label>Mémoire</label><span><?= $server ? byteToHuman($server['memory']) : "" ?></span></p>
    </div>

    <div class="logs">
        <h2>Logs</h2>
        <?php if ($lastLogs) { ?>
            <pre>
<?= $lastLogs ?>
            </pre>
        <?php } else { ?>
            No logs
        <?php } ?>
    </div>

    <?php if ($server) { ?>
        <h2>Connexions actives (<?= $server['activeConnections'] ?>)</h2>
        <table>
            <thead>
                <tr>
                    <th>Utilisateur</th>
                    <th>IP</th>
                    <th>Port</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($server['connections'] as $connection) { ?>
                <?php if ($connection['state'] == 'dead') { continue; } ?>

                <tr>
                    <td><?= $connection['username'] ?></td>
                    <td><?= $connection['address'] ?></td>
                    <td><?= $connection['port'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>

        <h2>Salons</h2>
        <table>
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Connections</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($server['rooms'] as $room) { ?>
                <tr>
                    <td><?= $room['name'] ?></td>
                    <td>
                    <table>
                        <thead>
                            <tr>
                                <th>Utilisateur</th>
                                <th>IP</th>
                                <th>Port</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($room['connections'] as $connection) { ?>
                            <?php if ($connection['state'] == 'dead') { continue; } ?>

                            <tr>
                                <td><?= $connection['username'] ?></td>
                                <td><?= $connection['address'] ?></td>
                                <td><?= $connection['port'] ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</main>
<?= $Html->footer(); ?>