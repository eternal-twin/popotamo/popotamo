<?php

use Popotamo\Kernel;

function logToHtml($logLine) {
    preg_match("/^\[([A-Z]*)\]/", $logLine, $matches);
    $severity    = strtolower($matches[1]);
    $stripedLine = trim(preg_replace("/^\[([A-Z]*)\]/", "", $logLine));

    return "<div class=\"log {$severity}\"><span class=\"severity\">{$severity}</span><span class=\"message\">{$stripedLine}</span></div>";
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Healthcheck - ePopotamo</title>
        <style>
            html, body {
                margin: 0;
                padding: 0;
                font-size: 16px;
                font-family: Roboto, sans-serif;
            }

            main {
                margin: 1rem;
            }

            h1 {
                text-align: center;
            }

            .log {
                border: 1px solid #666;
                background-color: #EEE;
                display: grid;
                grid-template-columns: 80px auto;
                margin: .5rem 0;

                .severity {
                    text-transform: capitalize;
                    padding: 1rem 0;
                    text-align: center;
                }

                .message {
                    padding: 1rem;
                }

                &.success {
                    border-color: #386a1f;
                    .severity {
                        background-color: #386a1f;
                        color: #FFF;
                        font-weight: 700;
                    }
                }

                &.info {
                    border-color: #0055a2;
                    .severity {
                        background-color: #0055a2;
                        color: #FFF;
                        font-weight: 700;
                    }
                }

                &.warning {
                    border-color: #f39c11;
                    .severity {
                        background-color: #f39c11;
                        color: #FFF;
                        font-weight: 700;
                    }
                }

                &.error {
                    border-color: #DC3545;
                    .severity {
                        background-color: #DC3545;
                        color: #FFF;
                        font-weight: 700;
                    }
                }

                &.critical {
                    border-color: #B00020;
                    .severity {
                        background-color: #B00020;
                        color: #FFF;
                        font-weight: 700;
                    }
                }
            }
        </style>
    </head>
    <body>
        <main>
            <h1>Healthcheck</h1>
    
            <div class="log-wrapper">
            <?php
                try {
                    $logFileInfo = new SplFileInfo(Kernel::getLogDir()."/healthcheck.log");
                    $logFile     = new SplFileObject(Kernel::getLogDir()."/healthcheck.log");

                    $buildTime = date('Y-m-d H:i:s T', $logFileInfo->getMTime());
                    echo logToHtml("[INFO] ePopotamo built on {$buildTime}.");
    
                    while ($logLine = $logFile->fgets()) {
                        echo logToHtml($logLine);
                    }
                } catch (RuntimeException $e) {
                    echo logToHtml("[CRITICAL] Healthcheck cannot be done. Log file is missing");
                }
            ?>
            </div>
        </main>
    </body>
</html>