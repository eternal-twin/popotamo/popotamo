<?php
/**
 * This is a technical page to simulate a big amount of drawings, to check that 
 * the letters appear at the expected frequencies.
 * This page is not secret, as the drawing probabilities are public.
 */

use \Popotamo\View\Html;
use \Popotamo\Controller\Random;

// Set here the amount of letter drawings you want to simulate
$nbrDrawings = 10000;

$Html   = new Html();
$Random = new Random();
// Get random letters in the alphabet
$randomLetters = $Random->getRandomLetters($nbrDrawings);
// Example of result: [A => 12, B => 15, C => 11, ...]
$lettersFrequencies = array_count_values($randomLetters);
arsort($lettersFrequencies);


echo $Html->header();
?>


<h2>Statistiques techniques</h2>

<table>
    <caption>Simulation de <?php echo $nbrDrawings ?> tirages de 1 lettre</caption>
    <tr>
        <th>#</th>
        <th>Lettre</th>
        <th>Nombre de fois tirée</th>
        <th>Fréquence (en %)</th>
    </tr>
    <?php
    $i = 0;
    foreach($lettersFrequencies as $letter=>$frequency) {
        $i++;
        echo '
        <tr>
            <td>#'.$i.'</td>
            <td>'.$letter.'</td>
            <td>'.$frequency.'</td>
            <td>'.($frequency/$nbrDrawings*100).'</td>
        </tr>';
    } ?>
</table>


<?php
echo $Html->footer();
