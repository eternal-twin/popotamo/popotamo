<?php

use \Popotamo\Model\DbPopotamo;
use \Popotamo\View\Html;
use \Popotamo\View\HtmlPoposhop;
use \Popotamo\Controller\Filter;


$profileId = (int)$_SESSION['popotamoProfileId'];

$DbPopotamo = new DbPopotamo();
$Html = new Html();
$HtmlPoposhop = new HtmlPoposhop();
$Filter = new Filter();


$pdo = $DbPopotamo->connect();
// TODO: too many DB requests here?
$profiles = $DbPopotamo->getGameProfilesFromGameId($gameId);
$gameProfile = null;
foreach ($profiles as $p) {
    if ($p->profile->id == $profileId) {
        $gameProfile = $p;
        break;
    }
}
assert($gameProfile !== null);
$skillsCharacs = $DbPopotamo->getSkillsCharacs();
$DbPopotamo->disconnect();

$profileSkills = $Filter->getProfileSkills($gameProfile->profile);

$html = [
    'menu' => $Html->menu(),
    'profile' => $Html->profileInsert($gameProfile->profile, $profileSkills),
    'poposhop' => $HtmlPoposhop->options($skillsCharacs, $profileSkills),
];
?>


<?php
echo $Html->header();
echo $html['profile'];
echo $html['menu'];
?>

    <main id="poposhop">

        <br>
        <br>
        <h2>Poposhop</h2>
        <?php echo $html['poposhop'] ?>

    </main>

<?php
echo $Html->footer();
