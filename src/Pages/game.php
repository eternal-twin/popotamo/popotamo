<?php

use Popotamo\Config;
use \Popotamo\Model\DbPopotamo;
use \Popotamo\Model\BoardReader;
use \Popotamo\Model\ChatReader;
use \Popotamo\Model\Inventory;
use \Popotamo\View\Html;
use \Popotamo\View\HtmlChat;
use \Popotamo\View\HtmlModal;
use \Popotamo\Controller\Filter;
use Popotamo\Model\GameProfile;

$Calc   = new \Popotamo\Controller\Calc();
$Filter = new \Popotamo\Controller\Filter();

$DbPopotamo     = new DbPopotamo();
$Html           = new Html(['gameId' => $gameId]);
$HtmlModal      = new HtmlModal();
$HtmlChat       = new HtmlChat();
$Filter         = new Filter();

$pdo = $DbPopotamo->connect();
$BoardReader = new BoardReader($pdo, $gameId);
$ChatReader  = new ChatReader($pdo, $gameId);

$Inventory = new Inventory($pdo, $profileId, $gameId);
// TODO: too many DB requests here.
// => Include getInventory() in getProfiles()?
// => Or put getSkillsCharacs() in a cache file?
$profiles  = $DbPopotamo->getGameProfilesFromGameId($gameId);
$ranking   = $DbPopotamo->getGameCurrentRanking($gameId);

// Game profile of the current user
/** @var GameProfile $gameProfile */
$gameProfile = null;
foreach ($profiles as $p) {
    if ($p->profile->getId() == $profileId) {
        $gameProfile = $p;
        break;
    }
}

//$profilesSkills = $DbPopotamo->getProfilesSkills($profiles[$profileId]['game_id']);
$skillsCharacs  = $DbPopotamo->getSkillsCharacs();
$profileLetters = $Inventory->getInventory();
$isGameReady    = $DbPopotamo->isGameReady($gameId);
$game           = $DbPopotamo->getGame($gameId);

$DbPopotamo->disconnect();

//-- TODO Allow board view after endgame ?
if ($game['date_end']) {
    header('Location: /', true);
    exit;
}

$profileSkills = $Filter->getProfileSkills($gameProfile->profile);
$HtmlModal->setOptionsCharacs($skillsCharacs);

// Work with ISO-8601 formatted dates to avoid confusions caused by time zones
$lastRefreshIso = $gameProfile->lastApRefreshUtc;

$html = [
    'menu'      => $Html->menu(),
    'profile'   => $Html->profileInsert($gameProfile->profile, $profileSkills),
    'profiles'  => $Html->profiles($profiles),
    'inventory' => $Html->inventory([]),
];

$frontProfiles = [];
/** @var GameProfile $profile */
foreach ($profiles as $profile) {
    $frontProfiles[] = [
        'id'              => $profile->profile->getId(),
        'isActivePlayer'  => $gameProfile === $profile,
        'username'        => $profile->profile->getDisplayName(),
        'actionPoints'    => $profile->getCurrentActionPoints(),
        'score'           => $profile->score,
        'scoreGoal'       => $profile->scoreGoal,
        'rank'            => $profile->winnerRank,
        'inventoryCount'  => $profile->letterCount,
        'optionsCount'    => $Calc->availableOptions($Filter->getProfileSkills($profile->profile)['options'])
    ];
}

echo $Html->header();
echo $html['profile'];
echo $html['menu'];

        // The modal windows to activate options (tornado...)
        $skillAliases = array_keys($skillsCharacs);
        foreach($skillAliases as $skillAlias) {
            echo $HtmlModal->modalOption($skillAlias, $profiles);
        }

        ?>

        <main id="gamepage">
            <div id="leftColumn">
                <div id="inventoryLine">
                    <?php
                    echo $html['inventory'];
                    echo '<div id="actionPointsBlock">
                            &#x26A1;<span class="actionPoints"><noscript>'.$gameProfile->actionPoints.'</noscript></span>
                        </div>';
                    ?>
                </div>
                <?php echo $Html->board(Config::BOARD_COLS, Config::BOARD_ROWS, $BoardReader->getBoard());?>
            </div>
            <div id="rightColumn">
                <div id="buttons">
                    <button id="buttonDraw" class="z-depth-2" onclick="Popotamo.drawLetter()">Piocher</button>
                    <button id="validateWords" class="z-depth-2" type="button" onclick="Popotamo.sendNewLetters()">Popotamo</button>
                    <div id="estimatedScore" class="tooltipped" data-tooltip="L'estimation de votre score selon les lettres que vous poserez">
                        <span class="normal"><?= $gameProfile->score ?>/<?= $gameProfile->scoreGoal ?></span>
                        <span class="switched"><?= $gameProfile->scoreGoal - $gameProfile->score ?> ⏵ 🏆</span>
                    </div>
                </div>
                <div id="skills">
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionTrash"
                            data-tooltip="Corbeille : Jeter toutes les lettres présentes sur votre réglette">&#x1F5D1;&#xFE0F;</button>
                    <button class="skill modal-trigger tooltipped"  data-target="modalOptionRecycling"
                            data-tooltip="Recyclage : permet de changer une de vos lettres pour 15 points d'action">&#x267B;&#xFE0F;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionTornado"
                            data-tooltip="Tornade : permet d'échanger toutes vos lettres par un nombre de lettres équivalent (utilisable une seule fois par partie).">&#x1F32A;&#xFE0F;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionBoost"
                            data-tooltip="Boost : permet de piocher 3 lettres au moment de votre choix (utilisable une seule fois par partie)">&#x2604;&#xFE0F;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionConsonant"
                            data-tooltip="Consonne :  piochez une consonne pour 25 points d'action">&#x1F524;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionVowel"
                            data-tooltip="Voyelle : piochez une voyelle pour 25 points d'action.">&#x1F524;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionJoker"
                            data-tooltip="Joker : vous permet d'ajouter une lettre de votre choix à votre stock de lettres (utilisable une seule fois par partie)">&#x1F0CF;</button>
                    <button class="skill modal-trigger tooltipped"  data-target="modalOptionThief"
                            data-tooltip="Vol : volez une lettre à l'adversaire de votre choix pour 30 points d'action (ne peut être utilisé que trois fois par partie)">&#x1F99D;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionRevolution"
                            data-tooltip="Révolution : échangez votre jeu contre un adversaire ayant le même nombre de lettres que vous (utilisable une seule fois par partie)">&#x1F4E2;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionBinoculars"
                            data-tooltip="Jumelles : vous permet de voir le jeu de l'adversaire de votre choix pour 5 points d'action.">&#x1F453;</button>
                    <button class="skill modal-trigger tooltipped" data-target="modalOptionShield"
                            data-tooltip="Bouclier : vous protège pour toute la partie des options Vol et Révolution (30 points d'action pour activer le bouclier).">&#x1F6E1;&#xFE0F;</button>
                </div>
                <?php echo $html['profiles'] ?>
                <div id="chat">
                    <template data-type="chat-message"><p class="green"><span data-timestamp class="date"></span> <strong data-username></strong>: <span data-message></span></p></template>
                    <template data-type="chat-new_word"><p><span data-timestamp class="date"></span> <strong data-username></strong> pose « <strong data-words class="word"></strong> » (<strong data-points></strong> points)</p></template>
                    <template data-type="chat-skill_use"><p class="boardCleaned"><span data-timestamp class="date"></span> <strong data-username></strong> a utilisé l'option <strong data-skill></strong> !</p></template>
                    <template data-type="chat-board_clean"><p class="boardCleaned"><span data-timestamp class="date"></span> <strong data-username></strong> a touché un bord ! Remise à zéro du plateau...</p></template>

                    <div class="history"></div>
                    <form class="chat-form">
                        <input name="message">
                        <button class="send icon"></button>
                    </form>
                </div>
            </div>
        </main>

        <script>
            document.addEventListener('appLoaded', () => {
                initGame({
                    game: {
                        id: <?= $gameId ?>,
                        started: <?= $isGameReady ? 'true' : 'false' ?>,
                        ended: <?= $game['date_end'] ? 'true' : 'false' ?>
                    },
                    player: {
                        username: '<?= htmlentities($gameProfile->profile->getDisplayName()) ?>',
                        actionPoints: <?= $gameProfile->actionPoints ?>,
                        actionPointsLastRefresh: '<?= $lastRefreshIso->format(DateTime::W3C) ?>',
                        maxActionsPoints: <?= Config::MAX_ACTION_POINTS ?>,
                        actionPointsPerSecond: 1/60, //-- TODO A varier selon le type de partie
                        inventory: <?= json_encode($profileLetters); ?>,
                        score: <?= $gameProfile->score ?>,
                        scoreGoal: <?= $gameProfile->scoreGoal ?>
                    },
                    profiles: <?= json_encode($frontProfiles); ?>,
                    board: {
                        letters: <?= json_encode($BoardReader->getBoard()); ?>
                    },
                    chat: <?= json_encode($ChatReader->getChatEvents()) ?>,
                    frontToken: '<?= $gameProfile->profile->getFrontToken() ?>',
                    wsUrl: 'ws<?= $isHttps ? 's' : '' ?>://<?= Config::get('server.popotamoDomain') ?>:<?= Config::get('server.socketPort') ?>'
                });
            })
        </script>
<?php
echo $Html->footer();
