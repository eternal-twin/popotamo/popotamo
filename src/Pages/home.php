<?php

use \Popotamo\View\Html;

$Html = new Html();
?>

<?php echo $Html->header(); ?>

<main id="homepage">
    <nav class="valign-wrapper">
        <a href="/login" class="btn z-depth-2 green">Connexion</a>
    </nav>
    
    <h2>Bienvenue sur <strong>ePopotamo</strong> !</h2>
    
    <section>
        <img src="resources/img/art.jpg" alt="Plateau du jeu">
        <div>
            <h3>Le jeu de mots relax jouable en toute situation !</h3>
            <ul>
                <li>Un jeu fun qui s'adapte à votre rythme</li>
                <li>Affrontez d'autres joueurs</li>
                <li>De 7 à 77 ans (et plus)</li>
            </ul>
            <br>
            <a href="login.php"><img src="/resources/img/register_fr.gif" alt="S'inscrire"></a>
        </div>
    </section>
    
    <section>
        <img src="/resources/img/screen1.jpg" alt="Plateaux du partie normale et partie rapide" style="order:2">
        <div>
            <h3>Un jeu de mots qui s'adapte à votre emploi du temps !</h3>
            <ul>
                <li><strong>Partie Normale :</strong> lancez une partie et venez jouer 
                    pendant vos pauses tout au long de la journée, relax !</li>
                <li><strong>Partie Rapide :</strong> attention et rigueur sont nécessaires, 
                    ça joue vite et c'est grisant !</li>
                <li><strong>C'est sur votre navigateur internet :</strong> connectez-vous
                    au travail, à la maison, rien à télécharger, rien à installer.</li>
            </ul>
        </div>
    </section>
    
    <section>
        <img src="/resources/img/screen2.jpg" alt="progression du profil du joueur" style="float:left">
        <div>
            <h3>Le jeu progresse avec vous</h3>
            <p>Remportez des victoires et utilisez les quids gagnés pour améliorer 
                votre jeu !</p>
            <ul>
                <li><strong>Vitesse, Imagination, Sagesse :</strong> ces compétences  
                    développent vos opportunités lors des parties,</li>
                <li><strong>Des mouvements spéciaux</strong> vous permettront de surprendre 
                    les autres joueurs et de rendre le jeu encore plus exaltant !</li>
            </ul>
        </div>
    </section>
    
    <section>
        <img src="/resources/img/screen3.jpg" alt="Gains de points et coupes" style="order:2">
        <div>
            <h3>Des joueurs à rencontrer !</h3>
            <ul>
                <li><strong>Un classement mensuel</strong> avec des titres honorifiques</li>
                <li><strong>Un chat de partie</strong> vous permettant de discuter avec les autres joueurs</li>
                <li><strong>Un forum communautaire</strong> pour échanger vos astuces et mieux 
                    connaître vos adversaires</li>
            </ul>
        </div>
    </section>
    
    <section style="justify-content:center;text-align:center">
        <div>
            <p>Tentez l'aventure, l'inscription est rapide et gratuite !</p>
            <a href="/login"><img src="/resources/img/register_fr.gif" alt="S'inscrire"></a>
        </div>
    </section>
    
</main>

<?php echo $Html->footer(); ?>
