<?php

namespace Popotamo\Security;

use Popotamo\Config;
use Popotamo\Entity\Profile;
use Popotamo\Entity\User;
use Popotamo\Exception\RestrictedAccessException;
use Popotamo\Exception\UnauthenticatedException;
use Popotamo\Model\Database;

class Firewall
{
    private const RULES = [
        "#^/$#"                 => self::ACCESS_PUBLIC,
        "#^/login$#"            => self::ACCESS_PUBLIC,
        "#^/oauth/callback#"    => self::ACCESS_PUBLIC,
        "#^/healthcheck/?#"     => self::ACCESS_ADMIN,
        "#^/admin/?#"           => self::ACCESS_ADMIN,
        "#^/#"                  => self::ACCESS_AUTHENTICATED,
    ];

    public const ACCESS_PUBLIC        = 0;
    public const ACCESS_AUTHENTICATED = 1;
    public const ACCESS_ADMIN         = 2;

    public const ROLE_USER       = "ROLE_USER";
    public const ROLE_MODERATOR  = "ROLE_MODERATOR";
    public const ROLE_ADMIN      = "ROLE_ADMIN";

    private ?User $user = null;

    public function __construct(private Database $db) {}

    public function getUser(): ?User
    {
        return $this->user;
    }
    
    public function loadUser(string $uuid)
    {
        $result = $this->db->selectOne("SELECT * FROM popo_users U WHERE U.user_id = :uuid;", ['uuid' => $uuid]);
        if ($result) {
            $this->user = User::load($result);

            if ($uuid === Config::get('server.permanentAdmins', "")) {
                $this->user->addRole(self::ROLE_ADMIN);
            }
            
            $result = $this->db->selectOne("SELECT * FROM popo_profiles P WHERE P.user_id = :uuid;", ['uuid' => $uuid]);
            if ($result) {
                $profile = Profile::load($result);
                $this->user->setProfile($profile);
            }
        }
    }

    public function isGranted($role) 
    {
        return in_array($role, $this->user->getRoles());
    }

    public function auth() 
    {
        if (isset($_SESSION['popotamoUserId'])) {
            $this->loadUser($_SESSION['popotamoUserId']);
        }
    }

    public function check($route): bool
    {
        foreach (self::RULES as $rule => $access) {
            if (preg_match($rule, $route)) {
                switch ($access) {
                    case self::ACCESS_PUBLIC:
                        //-- Public access. No further check required
                        return true;

                    case self::ACCESS_AUTHENTICATED:
                        if (!isset($this->user)) {
                            throw new UnauthenticatedException();
                        }

                        return true;

                    case self::ACCESS_ADMIN:
                        if (!isset($this->user)) {
                            throw new UnauthenticatedException();
                        }

                        if (!$this->isGranted(self::ROLE_ADMIN)) {
                            throw new RestrictedAccessException();
                        }
                        
                        return true;
                }
            } 
        }

        return false;
    }
}