<?php
namespace Popotamo\Model;

use PDO;
use \Popotamo\Controller\Random;


class Inventory extends Database {
    private $profileId;
    private $gameId;
    private $maxLetters;

    function __construct($pdo, $profileId, $gameId) {
        $this->profileId = $profileId;
        $this->gameId    = $gameId;

        parent::__construct($pdo);
    }

    function setMaxLetters($basicMaxLetters, $imagination) {
        $this->maxLetters = $basicMaxLetters + $imagination;
    }

    function getMaxLetters() {
        return $this->maxLetters;
    }

    /**
     * Get the letters of the player's inventory
     *
     * @param int $profileId The ID of the player whose you want the inventory.
     *                    If not defined, will return the inventory of the connected player.
     * @return array
     */
    function getInventory($profileId=null) {
        // If no user id given, we take the one set when initializing the class Inventory()
        $profileId = ($profileId !== null) ? $profileId : $this->profileId;

        return $this->select('SELECT letter_id, letter
            FROM popo_profiles_inventories
            WHERE profile_id = :profileId AND game_id = :gameId
            ORDER BY letter_id',
            [
                'profileId' => $profileId,
                'gameId'    => $this->gameId
            ],
            PDO::FETCH_KEY_PAIR
        );
    }

    /**
     * Tells whether a player has activated an option or not
     * (useful for the option "Bouclier")
     *
     * @param int $profileId THE ID of the concerned player
     * @param string $optionAlias
     * @return bool True if the option is activated, False if not.
     */
    function isOptionActivated($profileId, $optionAlias) {
        $results = $this->select('SELECT is_activated FROM popo_profiles_skills
            WHERE profile_id = :profileId
            AND skill_id = (SELECT skill_id FROM popo_skills WHERE alias = :optionAlias)
            AND is_activated = 1',
            [
                'profileId'    => $profileId,
                'optionAlias'  => $optionAlias
            ]
        );

        return count($results) > 0;
    }

    /**
     * Activates an option for a player
     * (useful for the "Bouclier")
     *
     * @param string $optionAlias
     */
    function activateOption($optionAlias) {
        return $this->execute('UPDATE popo_profiles_skills SET is_activated = 1
            WHERE profile_id = :profileId
            AND skill_id = (SELECT skill_id FROM popo_skills WHERE alias = :optionAlias)',
            [
                'profileId'    => $this->profileId,
                'optionAlias'  => $optionAlias
            ]
        );
    }

    /**
     * Add a given amount of random letters in the inventory
     *
     * @param array $inventoryLetters The letter currently in the player's inventory
     * @param array|int $toAdd The number of letters to add randomly,
     *                         or a predefined array of letters to add
     * @return bool
     */
    function addLetters($inventoryLetters, $toAdd) {
        $newLetters = false;
        // $toAdd can ba an array of letters or just an amount of random letters
        $amountToAdd = is_array($toAdd) ? count($toAdd) : $toAdd;

        if(count($inventoryLetters)+$amountToAdd <= $this->maxLetters) {

            if(is_array($toAdd)) {
                // If we have set a fixed array of letters
                $newLetters = $toAdd;
            }
            else {
                // Get random letters in the alphabet
                $Random = new Random();
                $newLetters = $Random->getRandomLetters($toAdd);
            }

            $lastNewLetterId = $this->dbAddInventoryLetters($newLetters);
            
            // Quick fix to calculate the IDs of all the new letters without a new SQL SELECT
            // TODO: make a more reliable method
            foreach($newLetters as $key=>$newLetter) {
                $newLettersIndexed[$lastNewLetterId-$key] = $newLetter;
            }
            $newLetters = $newLettersIndexed;
        }
        
        return $newLetters;
    }

    /**
     * Request to add letters to a player in the database
     *
     * @param array $lettersToAdd The list of letters to add
     */
    private function dbAddInventoryLetters($lettersToAdd) {
        $values = [];
        $params = [];
        foreach($lettersToAdd as $letter) {
            $values[] = '(?, ?, ?)';
            array_push($params, $this->profileId, $this->gameId, $letter);
        }

        $this->execute('INSERT INTO popo_profiles_inventories (profile_id, game_id, letter)
            VALUES '.join(',', $values).'',
            $params
        );
        
        return $this->pdo->lastInsertId();
    }

    /**
     * Replace one letter by another in the player's inventory
     *
     * @param string $oldLetterId The database ID of the letter to be replaced
     * @param string $newLetter The letter that will replace the old one
     */
    private function dbReplaceInventoryLetter($oldLetterId, $newLetter) {
        $this->execute('UPDATE popo_profiles_inventories SET letter = :newLetter
            WHERE profile_id = :profileId AND game_id = :gameId AND letter_id = :oldLetterId',
            [
                'profileId'     => $this->profileId,
                'gameId'        => $this->gameId,
                'oldLetterId'   => $oldLetterId,
                'newLetter'     => $newLetter,
            ]
        );
    }

    /**
     * Remove all the letters in the player's inventory
     *
     * @return bool
     */
    function emptyInventory() {
        $this->execute('DELETE FROM popo_profiles_inventories
            WHERE profile_id = :profileId AND game_id = :gameId',
            [
                'profileId' => $this->profileId,
                'gameId'    => $this->gameId,
            ]
        );
    }

    /**
     * Remove letters form the player's inventory.
     * Useful after validating new words on the board.
     *
     * @param array $lettersIds The IDs of the letters to remove. Each letter
     *                          in the inventory is identified by a unique ID
     *                          in the database table.
     */
    function removeLetters($lettersIds) {
        // Sanitization: the SQL "IN" clause must contain only integers
        $sanitizedLettersIds = array_map(function ($id) { return (int)$id; },
                                         $lettersIds);

        return $this->execute('DELETE FROM popo_profiles_inventories
            WHERE profile_id = :profileId AND game_id = :gameId
            AND letter_id IN('.implode(',', $sanitizedLettersIds).')',
            [
                'profileId' => $this->profileId,
                'gameId'    => $this->gameId,
            ]
        );
    }

    /**
     * Change all the letters in the player's inventory
     */
    function replaceAllLetters() {
        $Random = new Random();

        $this->emptyInventory();
        $this->dbAddInventoryLetters( $Random->getRandomLetters($this->maxLetters) );
    }

    /**
     * Replaces a random letter of the inventory by a random letter from the alphabet
     *
     * @param array $inventoryLetters The letters in the player's current inventory
     * @param array $vowels_or_consonants Set to "vowels" to return only the vowels
     *                                    Set to "consonants" to return only the consonants
     *                                    Any other value will return all the letters
     * @return bool
     */
    function replaceOneLetter($inventoryLetters, $vowels_or_consonants='') {
        $result = false;

        if(count($inventoryLetters) > 0) {

            // Get a random letter in the alphabet
            $Random = new Random();
            $newLetter = $Random->getRandomLetters(1, $vowels_or_consonants)[0];
            // Get a random letter in the player's inventory
            $randomKey = array_rand($inventoryLetters, 1);
            $oldLetterId = $randomKey;

            $result = $this->dbReplaceInventoryLetter($oldLetterId, $newLetter);
        }

        return $result;
    }

    /**
     * Steals a letter to a player
     *
     * @param int $oldProfileId The ID of the player to steal
     * @param int $newProfileId The ID of the player who steals the letter
     */
    function stealLetter($oldProfileId, $newProfileId) {
        return $this->execute('UPDATE popo_profiles_inventories
            SET profile_id = :newProfileId
            WHERE profile_id = :oldProfileId AND game_id = :gameId
            LIMIT 1',
            [
                'oldProfileId'     => $oldProfileId,
                'newProfileId'     => $newProfileId,
                'gameId'           => $this->gameId,
            ]
        );
    }

    /**
     * Switches the whole inventories of two players
     *
     * @param int $oldProfileId The ID of the current user of the inventory
     * @param int $newProfileId The ID of the new owner
     */
    function switchInventories($oldProfileId, $newProfileId) {
        return $this->execute('UPDATE popo_profiles_inventories AS inv1, popo_profiles_inventories AS inv2
            SET inv1.profile_id = :newProfileId, inv2.profile_id = :oldProfileId
            WHERE inv1.profile_id = :oldProfileId2 AND inv2.profile_id = :newProfileId2
            AND inv1.game_id = :gameId AND inv2.game_id = :gameId2',
            [
                'oldProfileId'     => $oldProfileId,
                'newProfileId'     => $newProfileId,
                'oldProfileId2'    => $oldProfileId,
                'newProfileId2'    => $newProfileId,
                'gameId'           => $this->gameId,
                'gameId2'          => $this->gameId
            ]
        );
    }
}
