<?php
namespace Popotamo\Model;

use DateTime;
use Popotamo\Config;
use Popotamo\Entity\Profile;

/**
 * Game-related data for a given profile.
 */
final class GameProfile {
    function __construct(
        public GameRef $game,
        public int $actionPoints,
        public ?\DateTimeImmutable $lastApRefreshUtc,
        public ?Profile $profile,
        public ?int $score,
        public ?int $scoreGoal,
        public ?int $letterCount,
        public ?int $winnerRank,
    ) {}

    public function getCurrentActionPoints()
    {
        $deltaActionPoints = ((new DateTime())->getTimestamp() - $this->lastApRefreshUtc->getTimestamp()) * Config::ACTION_POINTS_PER_SECOND;
        
        return min(Config::MAX_ACTION_POINTS, floor($this->actionPoints + $deltaActionPoints));
    }
}
