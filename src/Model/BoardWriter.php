<?php

namespace Popotamo\Model;

class BoardWriter extends Database {
    private $gameId;

    public function __construct($pdo, $gameId)
    {
        $this->gameId = $gameId;

        parent::__construct($pdo);
    }
    
    /**
     * Saves the new board in the database/file
     *
     * @param array $newBoard All the letters of the board (former+new letters)
     *                        Each letter must be indexed by coordinates, example :
     *                        [ 2_10 => S, 3_10 => I, ... ]
     * @return boolean
     */
    function updateBoard(array $newBoard): void {
        // language=PostgreSQL
        $this->execute("
            UPDATE game_boards
            SET board = :board
            WHERE game_id = :gameId",
            [
                'gameId' => $this->gameId,
                'board' => json_encode($newBoard),
            ]
        );
    }
}
