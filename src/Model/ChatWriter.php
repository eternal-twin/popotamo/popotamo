<?php

namespace Popotamo\Model;

use Popotamo\Config;
use Popotamo\Controller\Sanitize;

/**
 * Save the game events in the history of the chat
 * (new words placed, messages posted)
 */
class ChatWriter extends Database {
    private $gameId;

    public function __construct($pdo, $gameId)
    {
        $this->gameId = $gameId;

        parent::__construct($pdo);
    }
    
    /**
     * Save the new words placed
     *
     * @param string $authorName the pseudo of the player who placed the words
     * @param array $newWordsWithPoints The list of words placed, with their points
     *                                  [word1=>points, word2=>points, ...]
     */
    function addEventNewWords($authorName, $newWordsWithPoints) {
        
        $Sanitize = new Sanitize();
        
        // Respect the structure of the JSON file, otherwise it won't be read
        // correctly by the ChatReader() class
        $newEvent = [
            'eventType'  => 'newWords',
            'datetime'   => date('c'),
            'authorName' => filter_var($authorName, FILTER_SANITIZE_SPECIAL_CHARS),
            ];

        foreach($newWordsWithPoints as $word=>$points) {
            $newEvent['newWords'][] = [
                'word'   => $Sanitize->inputSpecialchars($word, null),
                'points' => $points,
                ];
        }

        $this->addEvent($newEvent);

        return $newEvent;
    }

    /**
     * Notes that the board has been cleaned (when a player reaches the border)
     *
     * @param string $authorName
     */
    function addEventBoardCleaned($authorName) {
        // Respect the structure of the JSON file, otherwise it won't be read
        // correctly by the ChatReader() class
        $newEvent = [
            'eventType'  => 'boardCleaned',
            'datetime'   => date('c'),
            'authorName' => filter_var($authorName, FILTER_SANITIZE_SPECIAL_CHARS),
        ];

        $this->addEvent($newEvent);
    }
    
    
    /**
     * Notes that a player has used an option (tornado...)
     *
     * @param string $authorName
     * @param string $optionAlias The internal name of the option used (tornado, binoculars...)
     */
    function addEventOptionUsed($authorName, $optionAlias) {
        // Respect the structure of the JSON file, otherwise it won't be read
        // correctly by the ChatReader() class
        $newEvent = [
            'eventType'  => 'optionUsed',
            'datetime'   => date('c'),
            'authorName' => filter_var($authorName, FILTER_SANITIZE_SPECIAL_CHARS),
        ];
        
        $newEvent['optionAlias'] = $optionAlias;
        
        $this->addEvent($newEvent);
    }
    
    
    /**
     * Notes that a player has sent a message
     *
     * @param string $authorName
     * @param string $message
     */
    public function addEventNewMessage($authorName, $message)
    {
        $Sanitize = new Sanitize();
        $newEvent = [
            'eventType'  => 'message',
            'datetime'   => date('c'),
            'authorName' => filter_var($authorName, FILTER_SANITIZE_SPECIAL_CHARS),
            'message'    => $Sanitize->inputSpecialchars($message, null)
        ];

        $this->addEvent($newEvent);
    }
    

    /**
     * Writes the new event at the end of the JSON file storing the chat
     *
     * @param array $newEvent The data of the nex event, as returned by addEventNewWords()
     */
    private function addEvent($newEvent): void {
        // language=PostgreSQL
        $this->execute("
            UPDATE game_chats
            SET chat = chat || (:chatEvent)::JSONB
            WHERE game_id = :gameId",
            [
                'gameId' => $this->gameId,
                'chatEvent' => json_encode($newEvent),
            ]
        );
    }
}
