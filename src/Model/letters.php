<?php

/**
 * Probabilité de sortie des lettres lors d'une pioche (en pourcentages)
 * (source : http://www.popotamo.com/forum/thread/1326703)
 * 
 * @param array $vowels_or_consonants Set to "vowels" to return only the vowels
 *                                    Set to "consonants" to return only the consonants
 *                                    Any other value will return all the letters
 */
function letters($vowels_or_consonants='') {
    
    $vowels = [
        'A' =>  972,
        'E' => 1473,
        'I' =>  941,
        'O' =>  596,
        'U' =>  367,
        'Y' =>   40,
        ];
    
    $consonants = [
        'S' => 1001,
        'R' =>  852,
        'N' =>  725,
        'T' =>  683,
        'L' =>  403,
        'C' =>  347,
        'M' =>  255,
        'P' =>  234,
        'D' =>  233,
        'G' =>  164,
        'B' =>  144,
        'F' =>  130,
        'H' =>  123,
        'Z' =>  105,
        'V' =>   93,
        'Q' =>   52,
        'X' =>   27,
        'J' =>   17,
        'K' =>    9,
        'W' =>    2,
        ];
    
    if($vowels_or_consonants === 'vowels') {
        return $vowels;
    }
    elseif($vowels_or_consonants === 'consonants') {
        return $consonants;
    }
    else {    
        return array_merge($vowels, $consonants);
    }
}
