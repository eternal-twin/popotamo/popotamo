<?php
namespace Popotamo\Model;

/**
 * Reference to a game.
 */
final class GameRef {
    function __construct(public int $gameId) {}
}
