<?php

namespace Popotamo\Model;

use DateTime;
use DateTimeImmutable;
use PDO;
use Popotamo\Entity\Profile;
use Popotamo\Model\Error\ProfileNotFound;
use Popotamo\Service\EventEmitter;
use Popotamo\Service\MessageQueue;

class DbPopotamo extends Database {
    /**
     * Update or insert a user.
     *
     * This caches the display name from the Eternaltwin's account.
     *
     * @param string $ETUuid The user's UUID returned by the Eternaltwin's API
     * @return string The Eternaltwin UUID
     */
    function upsertUser(string $ETUuid, string $displayName): string {
        $this->execute(
            "INSERT INTO popo_users(user_id, display_name)
            VALUES(:eternaltwinUuid, :displayName)
            ON CONFLICT (user_id)
            DO UPDATE SET
            display_name = :displayName
            ", 
            [
                'eternaltwinUuid' => $ETUuid,
                'displayName'     => $displayName
            ]
        );

        return $ETUuid;
    }

    function getGame($gameId)
    {
        $games = $this->select("SELECT * FROM popo_games WHERE game_id = :gameId",
            [
                'gameId' => $gameId,
            ],
            PDO::FETCH_UNIQUE
        );

        return $games[$gameId]??null;
    }

    /**
     * Create a Popotamo profile for the player
     * (each player can play on Popotamo with several profiles)
     *
     * @param string $userId The Popotamo's ID of the user (not the ET's UUID)
     * @param string $profileName The name of the profile
     * @param string $frontToken The token used for the realtime layer
     * @return int The ID of the profile (not to be confused with the user ID)
     */
    function createProfile($userId, $profileName, $frontToken) {
        $this->execute(
            'INSERT INTO popo_profiles(user_id, display_name, front_token)
            VALUES(:userId, :displayName, :frontToken)
            ',
            [
                'userId'      => $userId,
                'displayName' => $profileName,
                'frontToken'  => $frontToken,
            ]
        );

        return $this->pdo->lastInsertId();
    }

    /**
     * Update a Popotamo profile for the player
     * (each player can play on Popotamo with several profiles)
     *
     * @param string $profileId The Popotamo's ID of the user (not the ET's UUID)
     * @param string $profileName The name of the profile
     * @param string $frontToken The token used for the realtime layer
     * @return int The ID of the profile (not to be confused with the user ID)
     */
    function updateProfile($profileId, $profileName, $frontToken) {
        $this->execute(
            'UPDATE popo_profiles
                SET display_name = :displayName, front_token = :frontToken
            WHERE profile_id = :profileId',
            [
                'profileId'   => $profileId,
                'displayName' => $profileName,
                'frontToken'  => $frontToken,
            ]
        );

        return $profileId;
    }


    /**
     * Get all the game profiles for a given game.
     *
     * Example: `getGameProfilesFromGameId(341)` will return the profiles
     * of all the players of the game #341.
     *
     * @param int $gameId ID of the game.
     * @return GameProfile[] Game profiles
     */
    function getGameProfilesFromGameId(int $gameId): array {
        // language=PostgreSQL
        $rows = $this->select("
            WITH
            skills AS (
                SELECT
                    profile_id, ARRAY_AGG(ROW(skill_id, level, is_activated)) AS skills
                FROM popo_profiles_skills
                GROUP BY(profile_id)
            ),
            letter_count AS (
                SELECT game_id, profile_id, COUNT(*) AS letter_count
                FROM popo_profiles_inventories
                GROUP BY (game_id, profile_id)
            ),
            rank AS (
                SELECT game_id, profile_id, ROW_NUMBER() OVER (ORDER BY (score / score_goal) DESC) AS rank
                FROM popo_profiles_games
                WHERE game_id = :gameId
                GROUP BY (game_id, profile_id)
            )
            SELECT
                pg.action_points, pg.score, pg.score_goal, pg.last_ap_refresh_utc,
                COALESCE(lc.letter_count, 0) AS letter_count,
                p.profile_id, p.display_name, p.quids, p.front_token,
                ARRAY_TO_JSON(COALESCE(skills, '{}')) AS skills,
                rank
            FROM popo_profiles_games AS pg
                LEFT OUTER JOIN letter_count AS lc USING (game_id, profile_id)
                INNER JOIN popo_profiles AS p USING (profile_id)
                LEFT OUTER JOIN skills USING (profile_id)
                INNER JOIN rank USING (game_id, profile_id)
            WHERE pg.game_id = :gameId;
            ",
            [
                'gameId' => $gameId
            ],
            PDO::FETCH_DEFAULT    
        );

        $profiles = [];

        foreach ($rows as $row) {
            $skills = [];
            foreach(json_decode($row["skills"]) as $skill) {
                $skills[] = new ProfileSkill(
                    Skill::fromId($skill->f1),
                    $skill->f2,
                    $skill->f3,
                );
            }

            $row["skills"] = $skills;

            $profiles[] = new GameProfile(
                new GameRef($gameId),
                $row["action_points"],
                new \DateTimeImmutable($row["last_ap_refresh_utc"]),
                Profile::load($row),
                $row["score"],
                $row["score_goal"],
                $row["letter_count"],
                $row["rank"]
            );
        }

        return $profiles;
    }


    /**
     * Gets the skills of the player in a game ("competences" + "options")
     *
     * @param int $gameId
     * @return array
     */
    function getProfilesSkills(int $gameId) {
        return $this->select(
            'SELECT p.profile_id, p.display_name,
                ps.level, ps.is_activated,
                s.alias AS skill_alias, s.skill_type
            FROM popo_profiles_games AS pg
            RIGHT JOIN popo_profiles AS p ON p.profile_id = pg.profile_id
            RIGHT JOIN popo_profiles_skills AS ps ON ps.profile_id = pg.profile_id
            RIGHT JOIN popo_skills AS s ON s.skill_id = ps.skill_id
            WHERE pg.game_id = :gameId
            ',
            ['gameId' => $gameId], 
            PDO::FETCH_GROUP
        );
    }


    /**
     * Get a profile by id.
     *
     * @throws ProfileNotFound
     * @throws Error\UnknownSkillId
     */
    function getProfile(int $profileId): Profile {
        // language=PostgreSQL
        $row = $this->selectOne("
            WITH game_profiles AS (
                SELECT
                    ARRAY_AGG(ROW(pg.game_id, pg.action_points, pg.last_ap_refresh_utc, pg.score, pg.score_goal)) AS game_profiles
                FROM popo_profiles_games AS pg
                WHERE pg.profile_id = :profileId
                AND pg.winner_rank IS NULL
            ),
            skills AS (
                SELECT
                    ARRAY_AGG(ROW(skill_id, level, is_activated)) AS skills
                FROM popo_profiles_skills
                WHERE profile_id = :profileId
            )
            SELECT
                p.profile_id, p.display_name, p.quids, p.front_token,
                ARRAY_TO_JSON(COALESCE(game_profiles, '{}')) AS game_profiles,
                ARRAY_TO_JSON(COALESCE(skills, '{}')) AS skills
            FROM popo_profiles AS p, game_profiles, skills
            WHERE p.profile_id = :profileId;
        ", ['profileId' => $profileId]);

        /** @var GameProfile[] */
        $gameProfiles = [];
        /** @var ProfileSkill[] */
        $skills = [];
        foreach(json_decode($row["game_profiles"]) as $gameProfile) {
            $gameProfiles[] = new GameProfile(
                new GameRef($gameProfile->f1),
                $gameProfile->f2,
                new \DateTimeImmutable($gameProfile->f3),
                null,
                $gameProfile->f4,
                $gameProfile->f5,
                null,
                null,
            );
        }
        foreach(json_decode($row["skills"]) as $skill) {
            $skills[] = new ProfileSkill(
                Skill::fromId($skill->f1),
                $skill->f2,
                $skill->f3,
            );
        }

        $row['skills']       = $skills;
        $row['gameProfiles'] = $gameProfiles;

        return Profile::load($row);
    }

    function getProfileFromFrontToken(string $frontToken)
    {
        $row = $this->selectOne("
            SELECT p.profile_id
            FROM popo_profiles p
            WHERE p.front_token = :frontToken;
        ", ['frontToken' => $frontToken]);

        if ($row === null) {
            return null;
        }
        
        return $this->getProfile($row['profile_id']);
    }


    /**
     * Ensure the game exists, creating it is missing
     *
     * @param int $gameId Id of the game to create if missing
     */
    function ensureGameExists(int $gameId): void {
        $result = $this->selectOne("
            SELECT game_id FROM popo_games WHERE game_id = :gameId;",
            [
                'gameId' => $gameId,
            ]
        );
    }

    /**
     * Make the player enter a new game
     *
     * @param int $gameId The ID of the game to join
     * @param int $profileId The ID of the player profile (different from
     *                       the user ID: a user can own several profiles)
     * @param int $scoreGoal The score the player has to reach to win the game
     * @param int $startingAP The amount of action points when the player starts the game
     */
    function joinGame(int $gameId, int $profileId, int $scoreGoal, int $startingAP): void {
        $this->execute("
            INSERT INTO popo_profiles_games(profile_id, game_id, action_points, score_goal, last_ap_refresh_utc)
            VALUES(:profileId, :gameId, :actionPoints, :scoreGoal, :last_ap_refresh_utc);",
            [
                'profileId' => $profileId,
                'gameId' => $gameId,
                'scoreGoal' => $scoreGoal,
                'actionPoints' => $startingAP,
                'last_ap_refresh_utc' => (new DateTimeImmutable())->format(DateTime::W3C),
            ]
        );

        $profile     = $this->getProfile($profileId);
        $gameProfile = $profile->getGameProfileFromGameId($gameId);
        $Calc        = new \Popotamo\Controller\Calc();
        $Filter      = new \Popotamo\Controller\Filter();

        EventEmitter::newGameProfile([
            'gameID' => $gameId, 
            'profile' => [
                'id'              => $profile->getId(),
                'username'        => $profile->getDisplayName(),
                'actionPoints'    => $gameProfile->getCurrentActionPoints(),
                'score'           => $gameProfile->score,
                'scoreGoal'       => $gameProfile->scoreGoal,
                'rank'            => $gameProfile->winnerRank,
                'inventoryCount'  => $gameProfile->letterCount,
                'optionsCount'    => $Calc->availableOptions($Filter->getProfileSkills($profile)['options'])
            ]
        ]);

        $results = $this->select("
            SELECT COUNT(PG.game_id) AS nbplayers, G.date_start
            FROM popo_games G 
            LEFT JOIN popo_profiles_games PG ON PG.game_id = G.game_id
            WHERE G.game_id = :gameId 
            GROUP BY PG.game_id, G.date_start",
            [
                'gameId' => $gameId,
            ]
        )[0];

        $this->execute("
            UPDATE popo_games
            SET nb_players = {$results['nbplayers']}
            WHERE game_id = :gameId",
            [
                'gameId' => $gameId,
            ]
        );
        
        if ($results['nbplayers'] === 5) {
            $this->execute("
                UPDATE popo_profiles_games
                SET last_ap_refresh_utc = NOW()
                WHERE game_id = :gameId",
                [
                    'gameId' => $gameId,
                ]
            );

            EventEmitter::gameStart([
                'gameID' => $gameId
            ]);
        }
    }

    /**
     * Creates a new game.
     * Uses transaction and lock to prevent concurrency
     *
     * @return int
     */
    function createNewGame() {
        $this->exec("BEGIN WORK;");
        $this->exec("LOCK TABLE popo_games IN ROW EXCLUSIVE MODE;");

        $gameId = $this->selectOne("SELECT MAX(G.game_id)+1 as gameid FROM popo_games G", [])['gameid']??1;

        $this->execute("
            INSERT INTO popo_games(game_id, date_start, nb_players)
            VALUES (:gameId, :dateStart, 0);",
            [
                'gameId'    => $gameId,
                'dateStart' => (new DateTimeImmutable())->format(DATE_ATOM),
            ]
        );

        $this->exec("COMMIT WORK;");

        return $gameId;
    }

    function findRandomAvailableGame(int $profileId) {
        /*
        First SubQuery: Excludes all games where the player is already in
        */
        $row = $this->selectOne("
            SELECT G.game_id 
            FROM popo_games G
            WHERE G.nb_players < 5
            AND G.game_id NOT IN (
                SELECT PG.game_id FROM popo_profiles_games PG WHERE PG.profile_id = :profileId
            )
            ORDER BY RANDOM()
            LIMIT 1",
            [
                'profileId' => $profileId
            ]
        );
        
        return $row['game_id'] ?? false;
    }


    /**
     * Add game points to the given player
     *
     * @param int $profileId
     * @param int $pointsToAdd
     * @return bool
     */
    function addPoints($profileId, $gameId, $pointsToAdd) {
        $this->execute('UPDATE popo_profiles_games
            SET score = score + :pointsToAdd
            WHERE profile_id = :profileId
            AND game_id = :gameId',
            [
                'profileId'   => $profileId,
                'gameId'      => $gameId,
                'pointsToAdd' => $pointsToAdd
            ]
        );

        return true;
    }


    /**
     * Gets the characteristics of the "options" (tornado, boost...)
     *
     * @return array
     */
    function getSkillsCharacs() {
        return $this->select(
            'SELECT alias, skill_type, name, icon_text, description, popup_type, quids_cost, max_level
            FROM popo_skills', 
            [], 
            PDO::FETCH_UNIQUE
        );
    }


    /**
     * Increase the skill level by 1 for the corresponding profile/skill.
     *
     * A skill is an "option" (tornado, recycling...) or a "competence" (wisdom,
     * imagination...). This useful for buying in the Poposhop.
     * The addition is saturating: if already at max level, the value will not
     * be updated.
     *
     * @param int $profileId
     * @param int $skillAlias The alias of the skill
     * @return bool `true` if increased, `false` if already at the max level.
     */
    function addSkill($profileId, $skillAlias): bool {
        $skill = Skill::fromAlias($skillAlias);

        // language=PostgreSQL
        $row = $this->selectOne("
            WITH
            old_level AS (
                SELECT COALESCE(
                    (
                        SELECT level
                        FROM popo_profiles_skills
                        WHERE profile_id = :profileId AND skill_id = :skillId
                    ),
                    0
                ) AS old_level
            ),
            new_level AS (
                SELECT LEAST(old_level + 1, :maxLevel) AS new_level
                FROM old_level
            )
            INSERT INTO popo_profiles_skills(profile_id, skill_id, level)
            VALUES(:profileId, :skillId, (SELECT * FROM new_level))
            ON CONFLICT (profile_id, skill_id) DO UPDATE SET
                level = (SELECT * FROM new_level)
            RETURNING (SELECT * FROM old_level, new_level);
            ",
            [
                'profileId' => $profileId,
                'skillId'   => $skill->id,
                'maxLevel'  => $skill->maxLevel,
            ]
        );

        return $row["new_level"] != $row["old_level"];
    }


    /**
     * Modifies the amount of action points of a player
     *
     * @param int $profileId
     * @param int $newActionPoints
     * @return bool
     */
    function updateActionPoints($profileId, $gameId, $newActionPoints) {
        $query = $this->execute('UPDATE popo_profiles_games
            SET action_points = :newActionPoints, last_ap_refresh_utc = :last_ap_refresh_utc
            WHERE profile_id = :profileId
            AND game_id = :gameId',
            [
                'profileId' => $profileId,
                'gameId' => $gameId,
                'newActionPoints' => $newActionPoints,
                // Don't use the SQL NOW() to make sure the timezone is UTC
                'last_ap_refresh_utc' => (new DateTimeImmutable())->format(DateTime::W3C),
            ]
        );

        return (bool)$query->rowCount();
    }


    /**
     * Remove a number of "Quids" to a player
     * (useful for buying in the Poposhop)
     *
     * @param int $profileId
     * @param int $nbrToRemove
     * @return bool False if the player doesn't have enough Quids
     */
    function decrementQuids($profileId, $nbrToRemove) {
        $query = $this->execute(
            'UPDATE popo_profiles
            SET quids = quids - :nbrToRemove
            WHERE profile_id = :profileId AND (quids - :nbrToRemove >= 0)',
            [
                'profileId' => $profileId,
                'nbrToRemove' => $nbrToRemove
            ]
        );

        return (bool)$query->rowCount();
    }


    /**
     * Mark the rank of the player if he has reached his score goal
     *
     * @param int $profileId The ID of the player's profile
     * @param int $gameId The ID of the current game of the profile
     * @return bool Returns true if the player has reached his score goal
     */
    function updateWinnerRank($profileId, $gameId) {

        $row = $this->selectOne('SELECT MAX(winner_rank)+1 AS max_rank FROM popo_profiles_games PPG WHERE game_id = :gameId',
            [
                'gameId' => $gameId
            ]
        );
        $maxRank = $row['max_rank'] ?? 1;

        $query = $this->execute('UPDATE popo_profiles_games PPG
            SET winner_rank = :maxRank
            WHERE profile_id = :profileId AND game_id = :gameId
            AND score > score_goal',
            [
                'maxRank'   => $maxRank,
                'profileId' => $profileId,
                'gameId'    => $gameId,
            ]
        );

        return (bool)$query->rowCount();
    }

    /**
     * End the game 
     *
     * @param int $profileId The ID of the player's profile
     * @param int $gameId The ID of the current game of the profile
     * @return bool Returns true if one player has reached his score goal
     */
    function triggerEndGame($profileId, $gameId) {
        // No goal reached. Game continues
        if (!$this->updateWinnerRank($profileId, $gameId)) {
            return false;
        }

        // Game end trigger now. We rank each player and close the actual game
        $this->execute('
            UPDATE popo_profiles_games PPG1
            SET winner_rank = (
                WITH 
                PPG2 AS (
                    SELECT profile_id,ROW_NUMBER() OVER (ORDER BY (score / score_goal) DESC) AS rank
                    FROM popo_profiles_games
                    WHERE game_id = :gameId
                )
                SELECT rank FROM PPG2
                WHERE PPG1.profile_id = PPG2.profile_id
            )',
            [
                'gameId' => $gameId,
            ]
        );

        $this->execute('
            UPDATE popo_games
            SET date_end = NOW()
            WHERE game_id = :gameId',
            [
                'gameId' => $gameId,
            ]
        );
    }

    public function getGameCurrentRanking($gameId)
    {
        return $this->select('
            SELECT profile_id, ROW_NUMBER() OVER (ORDER BY (score / score_goal) DESC) AS rank
            FROM popo_profiles_games
            WHERE game_id = :gameId',
            [
                'gameId' => $gameId,
            ],
            PDO::FETCH_UNIQUE
        );
    }

    public function isGameReady($gameId) : bool
    {
        $row = $this->selectOne("
            SELECT G.game_id
            FROM popo_games G 
            LEFT JOIN popo_profiles_games PG ON PG.game_id = G.game_id 
            WHERE G.game_id = :gameId
            GROUP BY G.game_id 
            HAVING COUNT(G.game_id) = 5",
            [
                'gameId' => $gameId,
            ]
        );

        return $row !== null;
    }
}
