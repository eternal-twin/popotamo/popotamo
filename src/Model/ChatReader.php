<?php

namespace Popotamo\Model;

use Popotamo\Config;
use Popotamo\Controller\Sanitize;

/**
 * Get the data of the chat for further display
 */
class ChatReader extends Database {
    private $gameId;

    function __construct($pdo, $gameId) {
        $this->gameId    = $gameId;

        parent::__construct($pdo);
    }

    /**
     * Call this method to get the sanitized data of the chat
     * @return array
     */
    function getChatEvents() {
        $row = $this->selectOne("SELECT chat FROM game_chats WHERE game_id = (:gameId)",
            [
                'gameId' => $this->gameId,
            ] 
        );

        if (!$row) {
            $this->execute("INSERT INTO game_chats (game_id, chat) VALUES(:gameId, '[]'::JSONB)",
                [
                    'gameId' => $this->gameId,
                ]
            );
        }

        $chatEvents = json_decode($row["chat"]??"{}", true);
        foreach ($chatEvents as $chatEvent) {
            //-- Sanitize me
        }

        return $chatEvents;
    }
}
