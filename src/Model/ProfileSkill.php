<?php
namespace Popotamo\Model;

/**
 * The state of a given skill for a profile.
 */
final class ProfileSkill {
    function __construct(
        public Skill $skill,
        public int $level,
        public bool $isActivated,
    ) {}
}
