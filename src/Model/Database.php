<?php
namespace Popotamo\Model;

use Exception;
use PDO;
use PDOException;
use Popotamo\Config;
use Popotamo\Service\Profiler;

/**
 * Fonctions générales de la BDD, indépendantes du jeu
 */
class Database {
    public ?\PDO $pdo;

    function __construct($pdo=null) {
        // Permet de réutiliser la connexion à la BDD si celle-ci a été ouverte
        // à partir d'une autre classe
        $this->pdo = ($pdo !== null) ? $pdo : null;

        if ($this->pdo === null) {
            $this->connect();
        }
    }

    function connect() {
        // Si une connexion PDO est déjà ouverte, on n'en ouvre pas une autre
        if($this->pdo !== null) {
            return $this->pdo;
        }

        try {
            $pdo = new \PDO(Config::dsn(), Config::get('db.login'), Config::get('db.password'));
        }
        catch(\PDOException $e) {
            echo ('Impossible de se connecter à la base de données.');
            //On rethrow l'erreur, sinon elle ne sera pas dans le log d'erreurs de PHP
            throw $e;
        }
        catch(\Exception $e) {
            throw $e;
        }

        // Par défaut, les requêtes retournent un array associatif
        // (et non array associatif + array numérique)
        $pdo->setattribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        // Désactive la simulation de requêtes préparées
        // > Plus sécurisé (dans la config par défaut, le prepare() équivaut à un query() direct)
        // > Permet de retourner les valeurs dans leurs vrais types (int...) au lieu
        // de les avoir toutes en string (ce qui est problématique pour les
        // comparaisons strictes avec '===')
        $pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, FALSE);
        // Les erreurs SQL envoient des exceptions gérables par try/catch
        $pdo->setattribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        // Mémorise la connexion
        $this->pdo = $pdo;

        return $pdo;
    }

    function disconnect() {
        unset($this->pdo);
    }

    public function select(string $sql, array $params = [], int $fetchMode = PDO::FETCH_ASSOC) 
    {
        try {
            $startMicrotime = microtime(true);
    
            $query = $this->pdo->prepare($sql);
            $query->execute($params);
    
            Profiler::pushQuery($sql, $params, microtime(true) - $startMicrotime);
        } catch (PDOException $e) {
            Profiler::pushQuery($sql, $params, microtime(true) - $startMicrotime, $e);
            throw $e;
        }

        return $query->fetchAll($fetchMode);
    }

    public function selectOne(string $sql, array $params = [], int $fetchMode = PDO::FETCH_DEFAULT)
    {
        try {
            $startMicrotime = microtime(true);
    
            $query = $this->pdo->prepare($sql);
            $query->execute($params);
    
            Profiler::pushQuery($sql, $params, microtime(true) - $startMicrotime);
        } catch (PDOException $e) {
            Profiler::pushQuery($sql, $params, microtime(true) - $startMicrotime, $e);
            throw $e;
        }

        $result = $query->fetch($fetchMode);

        if ($query->rowCount() > 1) {
            // TODO Custom exception
            throw new Exception();
        }

        if ($query->rowCount() === 0) {
            return null;
        }

        return $result;
    }

    public function execute(string $sql, array $params = [])
    {
        try {
            $startMicrotime = microtime(true);
    
            $query = $this->pdo->prepare($sql);
            $query->execute($params);
    
            Profiler::pushQuery($sql, $params, microtime(true) - $startMicrotime);
        } catch (PDOException $e) {
            Profiler::pushQuery($sql, $params, microtime(true) - $startMicrotime, $e);
            throw $e;
        }

        return $query;
    }

    public function exec(string $sql, array $params = [])
    {
        return $this->execute($sql, $params);
    }
}
