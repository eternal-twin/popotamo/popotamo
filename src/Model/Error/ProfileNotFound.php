<?php

namespace Popotamo\Model\Error;

class ProfileNotFound extends \Exception {
    const CODE = 10000;

    public function __construct(int $profileId, \Throwable $previous = null) {
        $message = 'Profile ' . $profileId . ' not found';
        parent::__construct($message, self::CODE, $previous);
    }
}
