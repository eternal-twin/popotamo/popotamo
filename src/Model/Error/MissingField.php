<?php

namespace Popotamo\Model\Error;

class MissingField extends \Exception {
    const CODE = 10003;

    public function __construct(string $fieldName, \Throwable $previous = null) {
        $message = 'Failed to read missing field: ' . $fieldName;
        parent::__construct($message, self::CODE, $previous);
    }
}
