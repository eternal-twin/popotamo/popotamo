<?php

namespace Popotamo\Model\Error;

class UnknownSkillAlias extends \Exception {
    const CODE = 10001;

    public function __construct(string $alias, \Throwable $previous = null) {
        $message = 'Unknown skill alias ' . $alias;
        parent::__construct($message, self::CODE, $previous);
    }
}
