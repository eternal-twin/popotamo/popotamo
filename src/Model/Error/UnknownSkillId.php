<?php

namespace Popotamo\Model\Error;

class UnknownSkillId extends \Exception {
    const CODE = 10002;

    public function __construct(int $skillId, \Throwable $previous = null) {
        $message = 'Unknown skill id ' . $skillId;
        parent::__construct($message, self::CODE, $previous);
    }
}
