<?php

namespace Popotamo\Model;

use Popotamo\Config;

/**
 * Gets the letters on the board.
 * To analyze this content, see the class BoardAnalyzer().
 */
class BoardReader extends Database {
    private $gameId;

    public function __construct($pdo, $gameId)
    {
        $this->gameId = $gameId;

        parent::__construct($pdo);
    }

    /**
     * Returns the letters placed on the game board
     * @return array See the JSON file to know the structure of the data
     */
    function getBoard(): array {
        // language=PostgreSQL
        $row = $this->selectOne("
            WITH
            input AS (SELECT (:gameId)::INT AS game_id, '[]'::JSONB AS board),
            old_row AS (SELECT game_id, board FROM game_boards WHERE game_id = (:gameId)::INT),
            new_row AS (
              INSERT INTO game_boards(game_id, board)
              SELECT *
              FROM input
              WHERE NOT EXISTS(SELECT 1 FROM old_row INNER JOIN input USING(game_id))
              RETURNING game_id, board
            )
            SELECT * FROM old_row
            UNION ALL
            SELECT * FROM new_row;",
            [
                'gameId' => $this->gameId,
            ]
        );

        return json_decode($row["board"], true);
    }


    /**
     * Memorize the new letters placed by the player which touch letters placed
     * during the previous turns
     *
     * @param array $currentBoard The existing game board, without the letters added by the player
     *                           Example : [
     *                                      '8_5'  => 'C',
     *                                      '7_5'  => 'E',
     *                                      '10_5' => 'O'
     *                                      ]
     * @param array $newLetters The letters added by the player
     *                          (same data structure)
     */
    function getNewLettersWithBoardNeighbor($currentBoard, $newLetters) {
        $result = null;

        foreach($newLetters as $stringCoords=>$letter) {
            list($x, $y) = explode('_', $stringCoords);
            $x = intval($x, 10);
            $y = intval($y, 10);

            if( isset($currentBoard[self::writeCoordKey(($x-1), $y)]) or isset($currentBoard[self::writeCoordKey(($x+1), $y)]) or
                isset($currentBoard[self::writeCoordKey($x, ($y-1))]) or isset($currentBoard[self::writeCoordKey($x, ($y+1))])
                ) {
                $result[self::writeCoordKey($x, $y)] = $letter;
            }
        }

        return $result;
    }


    /**
     * Checks whether one of the given letters is placed in the central cell of the board.
     * (The first word of the game must cross this cell, see Popotamo's rules)
     *
     * @param array $letters The letters with coordinates [x_y=>letter, ...]
     * @return boolean
     */
    function isLetterInCenter($letters) {
        $centralCoords = $this->getCentralStringCoords();

        return (isset($letters[$centralCoords])) ? true : false;
    }


    /**
     * Returns the coordinates of the central cell of the game board
     *
     * @return string The coordinates as a string. Example : "8_10"
     */
    function getCentralStringCoords() {
        $centralX = (int) floor(Config::BOARD_COLS / 2);
        $centralY = (int) floor(Config::BOARD_ROWS / 2);

        return self::writeCoordKey($centralX, $centralY);
    }


    /**
     * Checks whether one of the given letters is in the first or last colums/rows of the game board.
     * (The Popotamo's rules say that the board must be cleaned in this case)
     *
     * @param array $letters The letters with coordinates [x_y=>letter, ...]
     * @param int $nbrCols  The width of the game board
     * @param int $nbrRows  The height of the game board
     */
    function isLetterInBorder($letters) {
        $result = false;

        foreach($letters as $stringCoord=>$letter) {

            list($x, $y) = self::readCoordKey($stringCoord);
            $x = (int)$x;
            $y = (int)$y;

            if($x === 0 or $x === Config::BOARD_COLS-1 or
               $y === 0 or $y === Config::BOARD_ROWS-1
                ) {
                $result = true;
                break;
            }
        }

        return $result;
    }


    /**
     * Checks that the new letters are not placed in already occupied cells
     *
     * @param array $currentBoard The game board, as returned by the method getBoard()
     * @param array $newLetters   The letters placed by the player, with coordinates
     *                            [x_y=>letter, ...]
     * @return boolean "True" if one or more cells are occupied, else "False"
     */
    function areCellsVacant($currentBoard, $newLetters) {
        return empty(array_intersect_key($currentBoard, $newLetters)) ? true : false;
    }


    /**
     * Calculates the points given by each word of the list
     *
     * @param array $words The characteristics of the new valid words placed,
     *                     as returned by BoardValidator->getNewWords()
     * @return array [word1=>points, word2=>points, ...]
     */
    function getWordsPoints($words) {
        $result = [];

        foreach($words as $word) {
            // General formula to count the points earned for each new word:
            // (Number of letters already on the board) + square(Number of new letters placed in the word)
            // But there are additional rules: see below.

            // Letters added by the player don't have the same value than letters already on the board
            $nbrNewLetters = (int)$word['nbrNewLetters'];
            $nbrExistingLetters = strlen($word['word']) - $nbrNewLetters;

            // Only the first 8 new letters must be squared
            $nbrLettersToSquare = min($nbrNewLetters, 8);
            // The additional letters (9 to 15) give 10 points per letter
            $nbrAdditionalLetters = $nbrNewLetters-$nbrLettersToSquare;
            $result[$word['word']] = $nbrExistingLetters + pow($nbrLettersToSquare, 2) + $nbrAdditionalLetters*10;
        }

        return $result;
    }

    public static function writeCoordKey($x, $y) {
        return (str_pad($x, 2, 0, STR_PAD_LEFT).'_'.str_pad($y, 2, 0, STR_PAD_LEFT));
    }

    public static function readCoordKey($coordKey) {
        return array_map('intval', explode('_', $coordKey));
    }
}
