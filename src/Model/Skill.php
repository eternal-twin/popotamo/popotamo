<?php

namespace Popotamo\Model;

use JetBrains\PhpStorm\Pure;
use Popotamo\Model\Error\UnknownSkillAlias;
use Popotamo\Model\Error\UnknownSkillId;

/**
 * A Popotamo skill.
 */
final class Skill {
    private function __construct(
        public int $id,
        public string $type,
        public string $alias,
        public string $name,
        public string $iconText,
        public ?string $iconImg,
        public string $description,
        public ?string $popupType,
        public int $quidsCost,
        public int $maxLevel,
    ) {
    }

    #[Pure]
    public static function Vowel(): self {
        return new self(1, 'options', 'vowel', 'voyelle', '&#x1F524;', NULL, 'Avec l\'option <strong class="bold">Voyelle</strong>, piochez une voyelle aléatoire pour 25 points d\'action.', 'confirm', 20, 1);
    }

    #[Pure]
    public static function Consonant(): self {
        return new self(2, 'options', 'consonant', 'consonne', '&#x1F524;', NULL, 'Avec l\'option <strong class="bold">Consonne</strong>, piochez une consonne aléatoire pour 25 points d\'action.', 'confirm', 20, 1);
    }

    #[Pure]
    public static function Recycling(): self {
        return new self(3, 'options', 'recycling', 'recyclage', '&#x267B;&#xFE0F;', NULL, 'L\'option <strong class="bold">Recyclage</strong> vous permet de changer une lettre de votre inventaire. Son utilisation vous coûtera 15 points d\'action.', 'confirm', 10, 1);
    }

    #[Pure]
    public static function Shield(): self {
        return new self(4, 'options', 'shield', 'bouclier', '&#x1F6E1;&#xFE0F;', NULL, 'L\'option <strong class="bold">Bouclier</strong> empêche les autres joueurs d\'utiliser les options <strong class="bold">Vol</strong> et <strong class="bold">Révolution</strong> contre vous, pendant toute la durée de la partie. Son activation vous coûtera 30 points d\'action.', 'confirm', 1000, 1);
    }

    #[Pure]
    public static function Boost(): self {
        return new self(5, 'options', 'boost', 'boost', '&#x2604;&#xFE0F;', NULL, 'L\'option <strong class="bold">Boost</strong> vous autorise à piocher 3 lettres d\'un coup. Elle n\'est est utilisable qu\'une seule fois par partie.', 'confirm', 50, 1);
    }

    #[Pure]
    public static function Joker(): self {
        return new self(6, 'options', 'joker', 'joker', '&#x1F0CF;', NULL, 'L\'option <strong class="bold">Joker</strong> vous permet d\'ajouter une lettre de votre choix à votre inventaire. Elle n\'est utilisable qu\'une seule fois par partie.', 'choseLetter', 1000, 1);
    }

    #[Pure]
    public static function Trash(): self {
        return new self(7, 'options', 'trash', 'corbeille', '&#x1F5D1;&#xFE0F;', NULL, 'En utilisant l\'option <strong class="bold">Corbeille</strong>, toutes les lettres de votre inventaire seront supprimées, sans contrepartie.', 'confirm', 0, 1);
    }

    #[Pure]
    public static function Revolution(): self {
        return new self(8, 'options', 'revolution', 'révolution', '&#x1F4E2;', NULL, 'L\'option <strong class="bold">Révolution</strong> vous permet d\'échanger votre inventaire contre un adversaire ayant le même nombre de lettres que vous. Elle n\'est utilisable qu\'une seule fois par partie.', 'choseProfile', 500, 1);
    }

    #[Pure]
    public static function Thief(): self {
        return new self(9, 'options', 'thief', 'vol', '&#x1F99D;', NULL, 'L\'option <strong class="bold">Vol</strong> vous permet de voler une lettre aléatoire au joueur de votre choix. Elle est utilisable 3 fois par partie.', 'choseProfile', 80, 1);
    }

    #[Pure]
    public static function Tornado(): self {
        return new self(10, 'options', 'tornado', 'tornade', '&#x1F32A;&#xFE0F;', NULL, 'L\'option <strong class="bold">Tornade</strong> permet d\'échanger toutes vos lettres par un nombre de lettres équivalent. Elle n\'est utilisable qu\'une seule fois par partie.', 'confirm', 200, 1);
    }

    #[Pure]
    public static function Binoculars(): self {
        return new self(11, 'options', 'binoculars', 'jumelles', '&#x1F453;', NULL, 'L\'option <strong class="bold">Jumelles</strong> vous permet de voir le jeu de l\'adversaire de votre choix. Son utilisation vous coûtera 5 points d\'action.', 'choseProfile', 10, 1);
    }

    #[Pure]
    public static function Wisdom(): self {
        return new self(12, 'competences', 'wisdom', 'sagesse', '&#x1F4D6;', NULL, 'Permet de commencer la partie avec davantage de points d\'action (+20 PA par point de sagesse).', NULL, 1, 10);
    }

    #[Pure]
    public static function Speed(): self {
        return new self(13, 'competences', 'speed', 'rapidité', '&#x1F4A8;', NULL, 'Diminue le coût en points d\'action d\'une pioche (-1 PA par point de rapidité).', NULL, 1, 10);
    }

    #[Pure]
    public static function Imagination(): self {
        return new self(14, 'competences', 'imagination', 'imagination', '&#x1F4A1;', NULL, 'Permet de posséder davantage de lettres dans votre inventaire (+1 lettre par point d\'imagination).', NULL, 1, 10);
    }

    /**
     * @return Skill[]
     */
    #[Pure]
    public static function all(): array {
        return [
            self::Vowel(),
            self::Consonant(),
            self::Recycling(),
            self::Shield(),
            self::Boost(),
            self::Joker(),
            self::Trash(),
            self::Revolution(),
            self::Thief(),
            self::Tornado(),
            self::Binoculars(),
            self::Wisdom(),
            self::Speed(),
            self::Imagination(),
        ];
    }

    /**
     * @throws UnknownSkillAlias
     */
    public static function fromAlias(string $alias): Skill {
        foreach (self::all() as $skill) {
            if ($skill->alias == $alias) {
                return $skill;
            }
        }
        throw new UnknownSkillAlias($alias);
    }

    /**
     * @throws UnknownSkillId
     */
    public static function fromId(string $skillId): Skill {
        foreach (self::all() as $skill) {
            if ($skill->id == $skillId) {
                return $skill;
            }
        }
        throw new UnknownSkillId($skillId);
    }
}
