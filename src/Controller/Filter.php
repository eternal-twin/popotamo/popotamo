<?php

namespace Popotamo\Controller;


use Popotamo\Entity\Profile;
use Popotamo\Model\Skill;

/**
 * Various ways to filter data
 */
class Filter {
    /**
     * Initializes default and structures existing values for the skills of the players
     * ("options" and "competences")
     */
    function getProfileSkills(Profile $profile): array {
        // Default values
        // TODO: don't duplicate the default structure and the filled data below
        $default = ['level' => 0, 'is_activated' => false, 'is_available' => true];
        $skills = [];
        foreach(Skill::all() as $skill) {
            $skills[$skill->type][$skill->alias] = $default;
        };

        foreach ($profile->getSkills() as $profileSkill) {
            $ref = &$skills[$profileSkill->skill->type][$profileSkill->skill->alias];
            $ref['level'] = $profileSkill->level;
            // For the options which have long effects (e.g. the shield)
            $ref['is_activated'] = $profileSkill->isActivated;
            // Will be false if the player can't use the option anymore in this game
            $ref['is_available'] = true;
        }
        return $skills;
    }
}
