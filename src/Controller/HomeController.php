<?php

namespace Popotamo\Controller;

use Popotamo\Kernel;
use Popotamo\Service\Response;

class HomeController extends Controller
{
    public function index()
    {
        ob_start();
        require $this->getUser() ? Kernel::getPagesDir().'/launchgame.php' : Kernel::getPagesDir().'/home.php';

        return new Response(ob_get_clean());
    }
}