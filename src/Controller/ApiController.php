<?php

namespace Popotamo\Controller;

use Popotamo\Command\SocketCommand;
use Popotamo\Controller\Filter;
use \Popotamo\Model\DbPopotamo;
use \Popotamo\Model\Inventory;
use \Popotamo\Model\ChatWriter;
use \Popotamo\Controller\Calc;
use \Popotamo\Controller\Sanitize;
use \Popotamo\Model\BoardReader;
use \Popotamo\Model\BoardWriter;
use \Popotamo\Model\DictReader;
use \Popotamo\Controller\BoardValidator;
use \Popotamo\Controller\InventoryValidator;
use Popotamo\Service\EventEmitter;
use Popotamo\Service\Response;

class ApiController extends Controller
{
    public function useSkill()
    {
        $Sanitize          = new Sanitize();
        $profileId         = (int)$this->getSession()->get('popotamoProfileId');
        $gameId            = (int)$_POST['gameId'];
        $profileName       = filter_var($this->getSession()->get('displayName'), FILTER_SANITIZE_SPECIAL_CHARS);
        $skillAlias        = $Sanitize->inputSpecialchars('skill', INPUT_POST);
        $letter            = $Sanitize->inputSpecialchars('letter', INPUT_POST);
        $targetedProfileId = filter_input(INPUT_POST, 'targetedProfileId', FILTER_VALIDATE_INT);

        // Basic amount of action points required to draw a letter
        // (without eventual reduction with he "speed" competence)
        $basicDrawCost   = 20;
        $basicMaxLetters = 6;

        $skillsCosts = [
            'binoculars'    => 5,
            'consonant'     => 25,
            'recycling'     => 15,
            'shield'        => 30,
            'thief'         => 30,
            'vowel'         => 25,
        ];

        $errorCode  = null;
        $apiData    = null;

        $Inventory  = new Inventory(null, $profileId, $gameId);
        $Calc = new Calc();
        $Filter = new Filter();
        
        $pdo = $Inventory->connect();

        $DbPopotamo = new DbPopotamo($pdo);

        if (!$DbPopotamo->isGameReady($gameId)) {
            http_response_code(423);
            exit;
        }

        $ChatWriter = new ChatWriter($pdo, $gameId);
        $inventoryLetters = $Inventory->getInventory();

        $profile = $DbPopotamo->getProfile($profileId);
        $profileSkills = $Filter->getProfileSkills($profile);
        $gameProfile = $profile->getGameProfileFromGameId($gameId);
        $actionPoints = $gameProfile->getCurrentActionPoints();

        // Piocher une lettre
        if($skillAlias === 'draw') {
            // The competence "imagination" increases the maximum letters in the inventory
            $Inventory->setMaxLetters($basicMaxLetters, $profileSkills['competences']['imagination']['level']);
            // The competence "speed" decreases the amount of AP required to draw a letter
            $drawCost = $basicDrawCost - $profileSkills['competences']['speed']['level'];

            if($actionPoints < $drawCost) {
                $errorCode = 'not_enough_action_points';
                $apiData = [
                    'requiredActionPoints' => $drawCost,
                    'currentActionPoints'  => $actionPoints,
                    ];
            }
            else {
                // Tries to add a letter in the inventory
                $newLetters = $Inventory->addLetters($inventoryLetters, 1);

                if($newLetters === false) {
                    $errorCode = 'full_inventory';
                }
                else {
                    // If the letter is added, substract the action points cost
                    $newActionPoints = $actionPoints-$drawCost;
                    $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                    $apiData = ['newLetters'        => $newLetters,
                                'newActionPoints'   => $newActionPoints
                                ];
                    $errorCode = 'success';
                }
            }
        }
        elseif($skillAlias === 'joker') {
            if(!preg_match('/^[A-Z]$/', $letter)) {
                $errorCode = 'invalid_letter';
            }
            else {
                // The competence "imagination" increases the maximum letters in the inventory
                $profile = $DbPopotamo->getProfile($profileId);
                $Inventory->setMaxLetters($basicMaxLetters, $profileSkills['competences']['imagination']['level']);

                $Inventory->addLetters($inventoryLetters, [$letter]);
                $errorCode = 'success';
            }
        }
        elseif($skillAlias === 'revolution') {
            //TODO : check that the player ID exists
            if($Inventory->isOptionActivated($targetedProfileId, 'shield')) {
                $errorCode = 'shield_activated';
            }
            else {
                $Inventory->switchInventories($profileId, $targetedProfileId);
                $errorCode = 'success';
            }
        }
        elseif($skillAlias === 'thief') {

            $newActionPoints = $actionPoints-$skillsCosts[$skillAlias];
            if($newActionPoints < 0) {
                $errorCode = 'not_enough_action_points';
            }
            // TODO: check that the player ID exists
            elseif($Inventory->isOptionActivated($targetedProfileId, 'shield')) {
                $errorCode = 'shield_activated';
            }
            else {
                $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                // TODO: check that the opponent has at least 1 letter to steal
                $Inventory->stealLetter($targetedProfileId, $profileId);
                $errorCode = 'success';
            }
        }
        elseif($skillAlias === 'shield') {

            $newActionPoints = $actionPoints-$skillsCosts[$skillAlias];
            if($newActionPoints < 0) {
                $errorCode = 'not_enough_action_points';
            }
            else {
                $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                $result = $Inventory->activateOption('shield');
                $errorCode = ($result === false) ? 'full_inventory' : 'success';
            }
        }
        elseif($skillAlias === 'trash') {

            $Inventory->emptyInventory();
            $errorCode = 'success';
        }
        elseif($skillAlias === 'tornado') {
            // The tornado replaces the amount of letters currently in the inventory
            $Inventory->setMaxLetters(count($inventoryLetters), 0);
            $Inventory->replaceAllLetters();
            $errorCode = 'success';
        }
        elseif($skillAlias === 'recycling') {

            $newActionPoints = $actionPoints-$skillsCosts[$skillAlias];
            if($newActionPoints < 0) {
                $errorCode = 'not_enough_action_points';
            }
            else {
                $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                $result = $Inventory->replaceOneLetter($inventoryLetters);
                $errorCode = ($result === false) ? 'empty_inventory' : 'success';
            }
        }
        elseif($skillAlias === 'consonant') {

            $newActionPoints = $actionPoints-$skillsCosts[$skillAlias];
            if($newActionPoints < 0) {
                $errorCode = 'not_enough_action_points';
            }
            else {
                $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                $Inventory->replaceOneLetter($inventoryLetters, 'consonants');
                $errorCode = 'success';
            }
        }
        elseif($skillAlias === 'vowel') {

            $newActionPoints = $actionPoints-$skillsCosts[$skillAlias];
            if($newActionPoints < 0) {
                $errorCode = 'not_enough_action_points';
            }
            else {
                $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                $Inventory->replaceOneLetter($inventoryLetters, 'vowels');
                $errorCode = 'success';
            }
        }
        elseif($skillAlias === 'binoculars') {

            $newActionPoints = $actionPoints-$skillsCosts[$skillAlias];
            if($newActionPoints < 0) {
                $errorCode = 'not_enough_action_points';
            }
            else {
                $DbPopotamo->updateActionPoints($profileId, $gameId, $newActionPoints);
                //TODO : check that the player ID exists
                $apiData['letters'] = $Inventory->getInventory($targetedProfileId);
                $errorCode = 'success';
            }
        }
        elseif($skillAlias === 'boost') {
            // The competence "imagination" increases the maximum letters in the inventory
            $profile = $DbPopotamo->getProfile($profileId);
            $Inventory->setMaxLetters($basicMaxLetters, $profileSkills['competences']['imagination']['level']);

            if(count($inventoryLetters) > $Inventory->getMaxLetters()) {
                $errorCode = 'full_inventory';
            }
            else {
                $Inventory->addLetters($inventoryLetters, 3);
                $errorCode = 'success';
            }
        }
        else {
            $errorCode = 'unknown_skill';
        }
        
        // The options used are logged in the chat
        if($skillAlias !== 'draw' and $errorCode=== 'success') {
            EventEmitter::emit([
                'command' => SocketCommand::COMMAND_CHAT,
                'room'    => "game/{$gameId}",
                'payload' => [['event' => 'optionUsed', 'authorName' => $_SESSION['displayName'], 'datetime' => date('Y-m-d\TH:i:sP'), 'optionAlias' => $skillAlias]]
            ]);
            $ChatWriter->addEventOptionUsed($profileName, $skillAlias);
        }
        
        // Gets the updated inventory of the player.
        // TODO: this query could be removed with custom treatments for each skill
        $apiData['newInventoryLetters'] = $Inventory->getInventory();
        $eventProfileData = [
            'id' => $profileId
        ];
        if (array_key_exists('newInventoryLetters', $apiData)) {
            $eventProfileData['inventoryCount'] = count($apiData['newInventoryLetters']);
        }
        if (array_key_exists('newActionPoints', $apiData)) {
            $eventProfileData['actionPoints'] = $apiData['newActionPoints'];
        }
        EventEmitter::emit([
            'command' => SocketCommand::COMMAND_PROFILE,
            'room'    => "game/{$gameId}",
            'payload' => [$eventProfileData]
        ]);
        
        $Inventory->disconnect();

        return new Response([
            'errorCode' => $errorCode,
            'data'      => $apiData
        ], Response::TYPE_JSON);
    }

    public function updateSkill()
    {
        $Sanitize          = new Sanitize();
        $profileId         = (int)$this->getSession()->get('popotamoProfileId');
        $skillAlias        = $Sanitize->inputSpecialchars('skill', INPUT_POST);
  
        // Get the player's profile
        $DbPopotamo = new DbPopotamo();
        $DbPopotamo->connect();
        $profile       = $DbPopotamo->getProfile($profileId);
        $skillsCharacs = $DbPopotamo->getSkillsCharacs();

        if(!isset($skillsCharacs[$skillAlias])) {

            $errorCode = 'unknown_skill';
            $apiData = [
                'unknown_skill'     => $skillAlias,
                'available_skills'  => array_keys($skillsCharacs)
                ];
        }
        else {

            $Calc      = new Calc();
            $skill     = $skillsCharacs[$skillAlias];
            $nextLevel = $profile[$skill['skill_type']][$skillAlias]['level'] + 1;
            $quidsCost = $Calc->quidsCost($skill['skill_type'], $skill['quids_cost'], $nextLevel);

            if($profile['quids'] < $quidsCost) {
                $errorCode = 'not_enough_quids';
                $apiData = [
                    'required_quids'    => $quidsCost,
                    'available_quids'   => $profile['quids']
                    ];
            }
            elseif($quidsCost === 0) {
                // Special for the competence "Trash" (native, so no need to buy it)
                $errorCode = 'not_upgradable';
            }
            else {
                // Adds the option to the player's profile
                $isSkillAdded = $DbPopotamo->addSkill($profileId, $skillAlias);
                if($isSkillAdded === false) {
                    $errorCode = 'max_level_reached';
                }
                else {
                    // Decrements the cost in Quids for buying the option
                    $areQuidsDecremented = $DbPopotamo->decrementQuids($profileId, $quidsCost);

                    if($areQuidsDecremented === false) {
                        $errorCode = 'not_enough_quids';
                    }
                    else {
                        $errorCode = 'success';
                    }
                }
            }
        }

        $DbPopotamo->disconnect();

        return new Response(
            [
                'message' => $errorCode
            ],
            Response::TYPE_JSON
        );
    }

    public function validateWord()
    {
        $profileId      = (int)$this->getSession()->get('popotamoProfileId');
        $gameId         = (int)$_POST['gameId'];
        // Sanitize the data sent by the browser (the new letters placed)
        $Sanitize       = new Sanitize();
        $newLettersIds  = $Sanitize->letters( json_decode($_POST['newLetters'], true) );
        $boardReader    = new BoardReader(null, $gameId);
        $boardReader->connect();
        // The board before the player placed his letters
        $currentBoard   = $boardReader->getBoard();
        $boardValidator = new BoardValidator($currentBoard, $newLettersIds);
        $errorCode      = 'success';
        $pdo            = null;
        $newWords       = null;
        $wordsInDict    = null;
        $wordsNotInDict = null;
        $withBoardNeighbor   = null;
        $wordsWithPoints     = null;
        $notConnectedLetters = null;


        if(empty($newLettersIds)) {
            // The player has clicked on "Popotamo" without placing any letter
            $errorCode = 'noNewLetters';
        }
        elseif($boardReader->areCellsVacant($currentBoard, $newLettersIds) === false) {
            $errorCode = 'lettersInOccupiedCells';
        }
        elseif(empty($currentBoard) and $boardReader->isLetterInCenter($newLettersIds) === false) {
            // The first letter of the game must cross the central cell
            $errorCode = 'mustStartInCenter';
        }
        elseif(empty($currentBoard) and count($boardValidator->futureBoard) === 1) {
            $errorCode = 'onlyOneLetter';
        }
        else {

            $Inventory = new Inventory(null, $profileId, $gameId);
            $InventoryValidator = new InventoryValidator();
            $pdo = $Inventory->connect();

            // Checks that the player's inventory really contains the letters he placed
            // on the board (he could try to cheat by modifying the javascript request)
            $inventoryLetters = $Inventory->getInventory($profileId);
            $areAllLettersInInventory = $InventoryValidator->areAllLettersInInventory(array_values($newLettersIds),
                                                                                    array_keys($inventoryLetters));

            // In the list of letters added on the board by the player, replace
            // the letter ID (e.g. "42") by the real letter (e.g. "D")
            $newLetters = [];
            if($areAllLettersInInventory === true) {
                foreach($newLettersIds as $boardCoord=>$letterId) {
                    $newLetters[$boardCoord] = $inventoryLetters[$letterId];
                }
            }

            // If the player places the first letters of the game
            if(empty($currentBoard)) {
                // Search the words only from the central cell
                $centralCoords = $boardReader->getCentralStringCoords();
                $withBoardNeighbor = [];
                $withBoardNeighbor[$centralCoords] = $newLetters[$centralCoords];
            }
            else {
                // Memorize the new letters placed by the player which touch letters placed during the previous turns
                $withBoardNeighbor = $boardReader->getNewLettersWithBoardNeighbor($currentBoard, $newLetters);
            }


            if($withBoardNeighbor !== null) {
                $boardValidator->setNewLetters($newLetters);
                $newWords = $boardValidator->getNewWords();

                $dictReader = new DictReader();
                // Validate the word with the dictionary
                foreach($newWords as $newWord) {

                    if($dictReader->isInDict($newWord['word']) === true) {
                        $wordsInDict[] = $newWord;
                    }
                    else {
                        $wordsNotInDict[] = $newWord;
                    }
                }
            }


            // Letters not connected to the already placed words
            // IMPORTANT: must be called *after* board analysis to work
            $notConnectedLetters = $boardValidator->getNotConnectedLetters();

            if($areAllLettersInInventory === false) {
                // Error if the player tries to use letters not in his inventory (cheat)
                $errorCode = 'lettersNotInInventory';
            }
            elseif(!empty($notConnectedLetters)) {
                // Error if letters are not connected to the already placed words
                $errorCode = 'lettersNotConnected';
            }
            elseif(!empty($wordsNotInDict)) {
                // Error if a word placed doesn't exist in the dictionary
                $errorCode = 'wordsNotInDict';
            }
            else {

                $DbPopotamo  = new DbPopotamo($pdo);
                $boardWriter = new BoardWriter($pdo, $gameId);
                $chatWriter  = new ChatWriter($pdo, $gameId);
                $wordsWithPoints = $boardReader->getWordsPoints($wordsInDict);

                // Add points to the player
                $DbPopotamo->addPoints($profileId, $gameId, array_sum($wordsWithPoints));

                // Remove the letters used from the inventory
                $Inventory->removeLetters($newLettersIds);

                // Save the new words in the chat history
                $newEvent = $chatWriter->addEventNewWords($_SESSION['displayName'], $wordsWithPoints);

                $gameProfile = $DbPopotamo->getProfile($profileId)->getGameProfileFromGameId($gameId);

                EventEmitter::emit([
                    'command' => SocketCommand::COMMAND_NEW_WORD,
                    'room'    => "game/{$gameId}",
                    'payload' => $newLetters
                ]);
                EventEmitter::emit([
                    'command' => SocketCommand::COMMAND_CHAT,
                    'room'    => "game/{$gameId}",
                    'payload' => [$newEvent]
                ]);
                EventEmitter::emit([
                    'command' => SocketCommand::COMMAND_PROFILE,
                    'room'    => "game/{$gameId}",
                    'payload' => [['id' => $profileId, 'score' => $gameProfile->score, 'inventoryCount' => count($Inventory->getInventory())]]
                ]);
                
                // Save the updated board in the database/file
                if($boardReader->isLetterInBorder($newLetters) === true) {
                    // When a letter is placed in the first or last colums/rows,
                    // all the letters are removed of the board (see Popotamo's rules)
                    $newBoard = [];
                    $chatWriter->addEventBoardCleaned($_SESSION['displayName']);
                    
                    EventEmitter::emit([
                        'command' => SocketCommand::COMMAND_BOARD_CLEAN,
                        'room'    => "game/{$gameId}",
                        'payload' => []
                    ]);
                } else {
                    $newBoard = $boardValidator->futureBoard;
                }

                // Memorize the new board with the new words
                $boardWriter->updateBoard($newBoard);

                // Mark if the player has won the game (= reached his score goal)
                if ($DbPopotamo->triggerEndGame($profileId, $gameId)) {
                    // TODO hum, lemme think
                }
            }

            // Close the database connection
            $Inventory->disconnect();
        }


        return new Response([
            'errorCode' => $errorCode,
            'data'      => [
                'withBoardNeighbor'     => $withBoardNeighbor,
                'notConnectedLetters'   => $notConnectedLetters,
                'newWords'              => $newWords,
                'wordsInDict'           => $wordsWithPoints,
                'wordsNotInDict'        => $wordsNotInDict,
            ]
        ], Response::TYPE_JSON);
    }

    public function getGameProfiles($gameId) 
    {
        $DbPopotamo = new DbPopotamo();
        $DbPopotamo->connect();
        $profiles   = $DbPopotamo->getGameProfilesFromGameId($gameId);
        $DbPopotamo->disconnect();
                
        $Calc   = new \Popotamo\Controller\Calc();
        $Filter = new \Popotamo\Controller\Filter();

        $frontProfiles = [];
        /** @var GameProfile $gameProfile */
        foreach ($profiles as $gameProfile) {
            $frontProfiles[] = [
                'id'              => $gameProfile->profile->id,
                'username'        => $gameProfile->profile->displayName,
                'actionPoints'    => $gameProfile->getCurrentActionPoints(),
                'score'           => $gameProfile->score,
                'scoreGoal'       => $gameProfile->scoreGoal,
                'rank'            => $gameProfile->winnerRank,
                'inventoryCount'  => $gameProfile->letterCount,
                'optionsCount'    => $Calc->availableOptions($Filter->getProfileSkills($gameProfile->profile)['options'])
            ];
        }

        return new Response([
            'errorCode' => null,
            'data'      => $frontProfiles
        ], Response::TYPE_JSON);
    }

    public function getEstimatedScore($gameId)
    {
        $profileId      = (int)$this->getSession()->get('popotamoProfileId');
        // Sanitize the data sent by the browser (the new letters placed)
        $Sanitize       = new Sanitize();
        $newLettersIds  = $Sanitize->letters( json_decode($_POST['newLetters'], true) );
        $boardReader    = new BoardReader(null, $gameId);
        $boardReader->connect();
        // The board before the player placed his letters
        $currentBoard   = $boardReader->getBoard();
        $boardValidator = new BoardValidator($currentBoard, $newLettersIds);
        $errorCode      = 'success';
        $newWords       = null;
        $wordsInDict    = null;
        $wordsNotInDict = null;
        $withBoardNeighbor   = null;
        $wordsWithPoints     = [];
        $notConnectedLetters = null;


        if(empty($newLettersIds)) {
            // The player has clicked on "Popotamo" without placing any letter
            $errorCode = 'noNewLetters';
        }
        elseif($boardReader->areCellsVacant($currentBoard, $newLettersIds) === false) {
            $errorCode = 'lettersInOccupiedCells';
        }
        elseif(empty($currentBoard) and $boardReader->isLetterInCenter($newLettersIds) === false) {
            // The first letter of the game must cross the central cell
            $errorCode = 'mustStartInCenter';
        }
        elseif(empty($currentBoard) and count($boardValidator->futureBoard) === 1) {
            $errorCode = 'onlyOneLetter';
        }
        else {
            $Inventory = new Inventory(null, $profileId, $gameId);
            $InventoryValidator = new InventoryValidator();
            $Inventory->connect();

            // Checks that the player's inventory really contains the letters he placed
            // on the board (he could try to cheat by modifying the javascript request)
            $inventoryLetters = $Inventory->getInventory($profileId);
            $areAllLettersInInventory = $InventoryValidator->areAllLettersInInventory(array_values($newLettersIds),
                                                                                    array_keys($inventoryLetters));

            // In the list of letters added on the board by the player, replace
            // the letter ID (e.g. "42") by the real letter (e.g. "D")
            $newLetters = [];
            if($areAllLettersInInventory === true) {
                foreach($newLettersIds as $boardCoord=>$letterId) {
                    $newLetters[$boardCoord] = $inventoryLetters[$letterId];
                }
            }

            // If the player places the first letters of the game
            if(empty($currentBoard)) {
                // Search the words only from the central cell
                $centralCoords = $boardReader->getCentralStringCoords();
                $withBoardNeighbor = [];
                $withBoardNeighbor[$centralCoords] = $newLetters[$centralCoords];
            }
            else {
                // Memorize the new letters placed by the player which touch letters placed during the previous turns
                $withBoardNeighbor = $boardReader->getNewLettersWithBoardNeighbor($currentBoard, $newLetters);
            }


            if($withBoardNeighbor !== null) {
                $boardValidator->setNewLetters($newLetters);
                $newWords = $boardValidator->getNewWords();

                $dictReader = new DictReader();
                // Validate the word with the dictionary
                foreach($newWords as $newWord) {

                    if($dictReader->isInDict($newWord['word']) === true) {
                        $wordsInDict[] = $newWord;
                    } else {
                        $wordsNotInDict[] = $newWord;
                    }
                }
            }


            // Letters not connected to the already placed words
            // IMPORTANT: must be called *after* board analysis to work
            $notConnectedLetters = $boardValidator->getNotConnectedLetters();

            if($areAllLettersInInventory === false) {
                // Error if the player tries to use letters not in his inventory (cheat)
                $errorCode = 'lettersNotInInventory';
            }
            elseif(!empty($notConnectedLetters)) {
                // Error if letters are not connected to the already placed words
                $errorCode = 'lettersNotConnected';
            }
            elseif(!empty($wordsNotInDict)) {
                // Error if a word placed doesn't exist in the dictionary
                $errorCode = 'wordsNotInDict';
            }
            else {
                $wordsWithPoints = $boardReader->getWordsPoints($wordsInDict);
            }

            // Close the database connection
            $Inventory->disconnect();
        }


        return new Response([
            'errorCode' => $errorCode,
            'data'      => [
                'words' => $wordsWithPoints,
                'score' => array_sum($wordsWithPoints),
            ]
        ], Response::TYPE_JSON);
    }

    public function postChatMessage($gameId)
    {
        $DbPopotamo = new DbPopotamo();
        $pdo = $DbPopotamo->connect();
        $chatWriter = new ChatWriter($pdo, $gameId);
        $chatWriter->addEventNewMessage($_SESSION['displayName'], $_POST['message']);
                    
        EventEmitter::emit([
            'command' => SocketCommand::COMMAND_CHAT,
            'room'    => "game/{$gameId}",
            'payload' => [['eventType' => 'message', 'authorName' => $_SESSION['displayName'], 'datetime' => date('Y-m-d\TH:i:sP'), 'message' => $_POST['message']]]
        ]);

        return new Response([
            'errorCode' => null,
            'data'      => []
        ], Response::TYPE_JSON);
    }
}