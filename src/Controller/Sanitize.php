<?php

namespace Popotamo\Controller;

class Sanitize
{
    
    /**
     * Sanitizes the couples coords/letters sent by the user
     *
     * @param type $board List of letters to sanitize, listed as 'coords'=>'letter'
     *      Example : [
     *                  '8_5'  => 'C',
     *                  '7_5'  => 'E',
     *                  '10_5' => 'O'
     *                  ]
     * @return array The same list with sanitized coords and sanitized letters
     */
    function letters($board) {

        $result = [];

        foreach($board as $unsafeCoord=>$unsafeLetter) {
            $letter = $this->inputSpecialchars($unsafeLetter, null);
            $coord  = filter_var($unsafeCoord, FILTER_VALIDATE_REGEXP, ['options' => ['regexp'=>'/[0-9]+_[0-9]+/'] ]);
            $result[$coord] = $letter;
        }

        return $result;
    }
    
    
    /**
     * Sanitize a $_GET parameter to prevent HTML injections.
     * Use this method to replace FILTER_SANITIZE_STRING (deprecated)
     *
     * @param string $stringToSanitize The name of the GET/POST parameter to sanitize.
     *                      Example: in $_GET["action"], it is "action".
     *                      Or directly the string to convert.
     * @param constant $inputConstant The PHP constant INPUT_GET or INPUT_POST,
     *                      depending on the source (url or form). If set to null, 
     *                      $stringToSanitize will be treated as the string to convert.
     * @return string|null The sanitized string, or null if the string is empty
     */
    function inputSpecialchars($stringToSanitize, $inputConstant) {
        
        // Can't simply use FILTER_SANITIZE_STRING: deprecated since PHP 8.1
        $input = ($inputConstant === null) 
                 ? filter_var($stringToSanitize, FILTER_UNSAFE_RAW)
                 : filter_input($inputConstant, $stringToSanitize, FILTER_UNSAFE_RAW);
        // Sanitize + cast to string to avoid a warning if $input is null
        $sanitized =  htmlspecialchars((string)$input);
        
        return ($sanitized === '') ? null : $sanitized;
    }
}
