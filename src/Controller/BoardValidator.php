<?php

namespace Popotamo\Controller;

use Popotamo\Model\BoardReader;

/**
 * Searches the words formed on the game board
 */
class BoardValidator
{

    public  $futureBoard  = [];
    private $validWords   = [];
    private $testedCoords = [ 'vertical' => [], 'horizontal' => [] ];

    private array $currentBoard         = [];
    private array $newLetters           = [];

    /**
     * @param array $currentBoard The existing game board, without the letters added by the player
     *                           Example : [
     *                                      '8_5'  => 'C',
     *                                      '7_5'  => 'E',
     *                                      '10_5' => 'O'
     *                                      ]
     * @param array $newLetters The letters added by the player
     *                          (same data structure)
     */
    function __construct(array $currentBoard, array $newLetters) {
        $this->currentBoard     = $currentBoard;

        $this->setNewLetters($newLetters);
    }

    /**
     * Set the submitted letters to the validator and simulate the future result board
     *
     * @param array $newLetters Letters submitted by the player
     * @return self
     */
    public function setNewLetters(array $newLetters) {
        $this->newLetters       = $newLetters;
        $this->futureBoard      = $this->currentBoard + $newLetters;
        $this->testedCoords     = [ 'vertical' => [], 'horizontal' => [] ];
        $this->validWords       = [];

        return $this;
    }


    /**
     * Starting from the letters placed by the player, gets recursively all the words
     * connected to these letters
     *
     * @return array
     */
    function getNewWords() {
        foreach($this->newLetters as $coord=>$letter) {
            list($x, $y) = BoardReader::readCoordKey($coord);

            $this->getNewWord('horizontal', $x, $y);
            $this->getNewWord('vertical', $x, $y);
        }

        return $this->validWords;
    }


    /**
     * Returns the new letters not connected to any already placed word
     * (not allowed by the rules)
     *
     * @return array Example : [4_7 => F, 10_3 => L, ...]
     */
    public function getNotConnectedLetters() {

        $notConnected = null;

        //-- Get new letters as coordinates
        $newLettersCoords       = array_keys($this->newLetters);
        //-- Get board letters as coordinates
        $currentLettersCoords   = array_keys($this->currentBoard);
        //-- Merge vertical and horizontal found connected words
        $validatedLettersCoords = array_unique(array_merge($this->testedCoords['horizontal'], $this->testedCoords['vertical']));

        //-- Return new letters not processed by the words finder (i.e not connected to the main grid)
        $notConnectedCoords = array_diff($newLettersCoords, array_diff($validatedLettersCoords, $currentLettersCoords));

        foreach($notConnectedCoords as $stringCoord) {
            $notConnected[$stringCoord] = $this->newLetters[$stringCoord];
        }

        return $notConnected;
    }


    /**
     * Starting from the coordinates of any letter, gets the eventual word formed
     *
     * @param string $direction "horizontal" to search an horizontal word
     *                          "vertical" to search a vertical word
     * @param int $x The X coord (column) of any letter of the word
     * @param int $y The Y coord (line) of the same letter
     * @param string $inRecursion Set its value to "inRecursion" when the method
     *                            is called inside the method getHorizontalWord()
     * @return array List of the letters of the horizontal word found
     */
    private function getNewWord($direction, $x, $y, $inRecursion=false) {
        // Don't test the same letter several times
        if($this->isInTestedCoords($direction, $x, $y)) {
            return;
        }

        // In a recursion, stop as soon as we meet a word placed in a former turn
        if($inRecursion === 'inRecursion' and $this->isInFormerWord($direction, $x, $y)) {
            return;
        }

        // Search a word in the given direction (horizontal/vertical)
        $newWord = $this->getWordLetters($direction, $x, $y);
        
        // The algorithm checks each letter to find an eventual crossing word. 
        // If none, don't add this lonely letter in the list of new "words"
        if(count($newWord) <= 1) return;
        
        $nbrNewLetters = $this->nbrNewWordLetters($newWord, $direction, $x, $y);        
        // Memorize the word found
        $this->addValidWord($newWord, $nbrNewLetters);
        // Memorize the letters already analyzed to avoid infinite recursion (see below)
        $this->addLettersToTestedCoords($direction, $x, $y, $newWord);
        // Recursion: search the eventual words crossing the initial word
        $this->getCrossingWords($direction, $x, $y, $newWord);
    }

    
    /**
     * Counts the number of new letters in a word (excluding those already placed 
     * on the board at previous turns)
     * 
     * @param array $newWord The letters of the word, as returned by 
     *                       the method nbrNewWordLetters()
     *                       (pair X or Y coordinate => letter)
     * @param string $direction "horizontal" or "vertical"
     * @param int $x The X coordinate of any letter in the word
     * @param int $y The Y coordinate of any letter in the word
     * @return int
     */
    private function nbrNewWordLetters($newWord, $direction, $x, $y) {
        $nbrNewLetters = 0;
        
        if($direction === 'horizontal') {
            // Note that the $x coordinate is invariable in an horizontal word
            foreach($newWord as $coord => $word) {
                list($x, $y) = BoardReader::readCoordKey($coord);
                if(array_key_exists(BoardReader::writeCoordKey($x, $y), $this->newLetters)) {
                    $nbrNewLetters++;
                }
            }
        }
        elseif($direction === 'vertical') {
            // In a vertical word, the invariable coordinate is the $y
            foreach($newWord as $coord => $word) {
                list($x, $y) = BoardReader::readCoordKey($coord);
                if(array_key_exists(BoardReader::writeCoordKey($x, $y), $this->newLetters)) {
                    $nbrNewLetters++;
                }
            }
        }
        
        return $nbrNewLetters;
    }
    
    
    /**
     * Recursion: search all the words crossing the given word
     *
     * @param string $direction "horizontal" if the *given word* is horizontal
     *                          "vertical" if the *given word* is vertical
     * @param int $x
     * @param int $y
     * @param array $wordLetters
     */
    private function getCrossingWords($direction, $x, $y, $wordLetters) {

        if($direction === 'horizontal') {
            foreach($wordLetters as $coord=>$letter) {
                // With an horizontal word, we search vertical crossing words
                $this->getNewWord('vertical', BoardReader::readCoordKey($coord)[0], $y, 'inRecursion');
            }
        }
        elseif($direction === 'vertical') {
            foreach($wordLetters as $coord=>$letter) {
                $this->getNewWord('horizontal', $x, BoardReader::readCoordKey($coord)[1], 'inRecursion');
            }
        }
    }


    /**
     * Search a word starting from a given letter, in the given direction
     *
     * @param string $direction "horizontal" to search a word horizontally
     *                          "vertical" to search vertically
     * @param int $x The X coordinate of the letter
     * @param int $y the Y coordinate of the same letter
     * @return array The letters of the found word (not sorted)
     */
    private function getWordLetters($direction, $x, $y) {

        $newWord = [];

        if($direction === 'horizontal') {
            $xIncrement = 1;
            $yIncrement = 0;
        }
        elseif($direction === 'vertical') {
            $xIncrement = 0;
            $yIncrement = 1;
        }

        // Search the new letters to the left (if horizontal) or to the top (if vertical)
        for($i=0; $this->isInFutureBoard($x + $xIncrement * $i, $y + $yIncrement * $i); $i--) {
            // Index by the X coord to allow sorting the letters horizontally
            // (or by the Y to sort vertically)
            $newWord[BoardReader::writeCoordKey($x + $xIncrement * $i, $y + $yIncrement * $i)] = $this->getFutureBoardLetter($x + $xIncrement * $i, $y + $yIncrement * $i);
        }
        // Search the new letters to the right (if horizontal) or to the bottom (if vertical)
        for($i=0; $this->isInFutureBoard($x + $xIncrement * $i, $y + $yIncrement * $i); $i++) {
            $newWord[BoardReader::writeCoordKey($x + $xIncrement * $i, $y + $yIncrement * $i)] = $this->getFutureBoardLetter($x + $xIncrement * $i, $y + $yIncrement * $i);
        }

        // Put the letters of the word in the good order
        ksort($newWord);

        return $newWord;
    }


    /**
     * Determines if a letter exists in the given coordinates
     *
     * @param int $x
     * @param int $y
     * @return bool "True" if there is a letter in the cell
     */
    private function isInFutureBoard($x, $y) {
        return isset($this->futureBoard[BoardReader::writeCoordKey($x, $y)]);
    }


    /**
     * Gets the letter placed in the given coordinates
     *
     * @param int $x
     * @param int $y
     * @return string The letter in this cell
     */
    private function getFutureBoardLetter($x, $y) {
        return $this->futureBoard[BoardReader::writeCoordKey($x, $y)];
    }


    /**
     * Memorize the letters of the given word as tested
     *
     * @param string $direction "horizontal" or "vertical" according to the direction
     *                          of the word
     * @param int $x The X coordinate of any letter of the word
     * @param int $y The Y coordinate of the same letter
     * @param array $wordLetters The letters composing the word
     *                            For an horizontal word : [X=>letter, X=>letter, ...]
     *                            For a vertical word :    [Y=>letter, Y=>letter, ...]
     */
    private function addLettersToTestedCoords($direction, $x, $y, $wordLetters) {

        if($direction === "horizontal") {
            foreach($wordLetters as $coord=>$letter) {
                $x = BoardReader::readCoordKey($coord)[0];
                array_push($this->testedCoords[$direction], BoardReader::writeCoordKey($x, $y));
            }
        }
        elseif($direction === "vertical") {
            foreach($wordLetters as $coord=>$letter) {
                $y = BoardReader::readCoordKey($coord)[1];
                array_push($this->testedCoords[$direction], BoardReader::writeCoordKey($x, $y));
            }
        }
    }


    /**
     * Determines if the given letter is inside a word already existing on the board
     * (before the player places his letters)
     *
     * @param type $direction "horizontal" to search an horizontal word
     *                        "vertical" to search a vertical word
     * @param int $x The X coordinate of the letter
     * @param int $y The Y coordinate of the same letter
     * @return bool
     */
    private function isInFormerWord($direction, $x, $y) {

        if($direction === 'vertical') {
            return isset($this->currentBoard[BoardReader::writeCoordKey($x, $y-1)]) or
                   isset($this->currentBoard[BoardReader::writeCoordKey($x, $y+1)]);
        }
        elseif($direction === 'horizontal') {
            return isset($this->currentBoard[BoardReader::writeCoordKey($x-1, $y)]) or
                   isset($this->currentBoard[BoardReader::writeCoordKey($x+1, $y)]);
        }
    }


    /**
     * Determines if an horizontal or vertical word has already been searched
     * starting from the given coordinates
     *
     * @param string $direction "horizontal" or "vertical" to know if
     *                          an horizontal or vertical word has been already searched
     * @param int $x The X coordinate of the letter
     * @param int $y The Y coordinate of the same letter
     * @return bool
     */
    private function isInTestedCoords($direction, $x, $y) {

        return in_array(BoardReader::writeCoordKey($x, $y), $this->testedCoords[$direction]) ? true : false;
    }


    /**
     * Memorize a new word found on the grid
     *
     * @param array $wordLetters The letters composing the word
     *                           For an horizontal word : [X=>letter, X=>letter, ...]
     *                           For a vertical word :    [Y=>letter, Y=>letter, ...]
     * @param int $nbrNewLetters The amount of the letters in the word (excluding
     *                           those already placed on the board)
     */
    private function addValidWord($wordLetters, $nbrNewLetters) {

        // A word can't be a single letter
        if(count($wordLetters) > 1) {
            // The words will be returned in lower case (more esthetic)
            $this->validWords[] = [
                'word'          => strtolower(join($wordLetters)),
                'nbrNewLetters' => $nbrNewLetters
            ];
        }
    }
}
