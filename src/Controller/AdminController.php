<?php

namespace Popotamo\Controller;

use Popotamo\Kernel;
use Popotamo\Model\DbPopotamo;
use Popotamo\Router;
use Popotamo\Service\EventEmitter;
use Popotamo\Service\File;

class AdminController extends Controller
{
    public function dashboard()
    {
        // Temporaire
        return $this->games();
    }

    public function games()
    {
        $db    = $this->getDb();
        $games = $db->select(
            "SELECT 
                G.game_id, 
                G.date_start, 
                G.date_end, 
                G.nb_players,
                JSON_AGG(json_build_object('profile_id', PG.profile_id, 'display_name', P.display_name, 'score', PG.score, 'score_goal', PG.score_goal, 'winner_rank', PG.winner_rank)) AS players
            FROM popo_games G 
            LEFT JOIN popo_profiles_games PG ON G.game_id = PG.game_id
            LEFT JOIN popo_profiles P ON PG.profile_id = P.profile_id
            GROUP BY G.game_id
            ORDER BY G.date_start DESC
            LIMIT 50"
        );

        $games = array_map(function($game) { $game['players'] = json_decode($game['players'], true); return $game; }, $games);

        return $this->render("admin/dashboard", [
            'games' => $games
        ]);
    }

    public function game($gameId)
    {
        $db         = $this->getDb();
        $dbPopotamo = new DbPopotamo($db->pdo);

        return $this->render('game', [
            'isAdmin'   => true,
            'gameId'    => $gameId,
            'profileId' => $dbPopotamo->getGameProfilesFromGameId($gameId)[0]->profile->getId()
        ]);
    }

    public function socket()
    {
        $serverStatus = EventEmitter::info();
        $logFile      = Kernel::getLogDir()."/socket.log";
        $pid          = false;

        if ($this->getRequest()->hasQuery('start-server') && !$serverStatus) {
            $phpPath     = exec("which php");
            $consolePath = Kernel::getProjectDir()."/bin/console";
            $command     = "popotamo:socket";

            $pid = shell_exec("{$phpPath} {$consolePath} {$command} --log={$logFile} 2>&1 | tee -a {$logFile} 2>/dev/null >/dev/null  & echo $!; ");
            if ($pid) {
                file_put_contents(Kernel::getLogDir()."/socket.pid", $pid);
            }

            usleep(100000);

            Router::redirectTo("/admin/socket");
        }

        $lastLogs = false;
        if (file_exists($logFile)) {
            $lastLogs = File::tail($logFile, 50);
        }

        if ($serverStatus && file_exists(Kernel::getLogDir()."/socket.pid")) {
            $pid = file_get_contents(Kernel::getLogDir()."/socket.pid");
        } 

        return $this->render('admin/socket', [
            'server'   => $serverStatus['data'] ?? false,
            'pid'      => $pid,
            'lastLogs' => $lastLogs
        ]);
    }
}