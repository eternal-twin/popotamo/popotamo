<?php

namespace Popotamo\Controller;

use Popotamo\Config;

class Calc {

   /**
    * Calculates the cost in Quids  for a competence, according to its level
    *
    * @param type $skillLevel
    * @return type
    */
   function quidsCost($skillType, $quidsCost, $skillLevel) {

        if($skillType === 'competences') {
            // For the competences, the upgrade cost grows following this formula.
            // Example: the 4th wisdom level costs 4 x 4 x 10 = 160 quids
            return pow($skillLevel, 2) * 10;
        }
        else {
            // For the "options", we simply use a fix cost
            return $quidsCost;
        }
   }


    /**
     * Calculates the player's total score required to win, taking into account
     * his "handicap" level.
     * E.g.: 250 basic score points + 15 handicap points = 265 score points required to win.
     * The handicap is linked to the amount of "competences" (wisdom, imagination...)
     * of the player.
     *
     * @param int $basicScore The score required to win without any handicap.
     * @param int $competencePoints Sum of the levels of all the competences. Example:
     *                              Speed lvl 2 + Imagination lvl 6 + Wisdom lvl 3
     *                              = 11 competence points (2+6+3)
     */
    function scoreToWin($basicScore, $competencePoints) {

        // Amount of handicap points according to the competence points, starting
        // from 0 competence (0 handicap) to 30 competences (400 handicap).
        // This is the orginal Motion Twin's scale. There is no clean mathematical
        // formula behind this.
        $handicaps = [  0, 3, 9, 15, 23, 32, 42, 52, 62, 74, 85, 98,
                        110, 124, 137, 151, 165, 180, 195,
                        211, 226, 242, 259, 275, 292,
                        309, 327, 345, 363, 381,
                        400
                        ];

        return $basicScore + $handicaps[$competencePoints];
    }


    /**
     * Counts the amount of options the player can use in this game
     *
     * @param array $profileOptions The options of the players, as structured
     *                              by Filter->getProfileSkills()
     */
    function availableOptions($profileOptions) {

        $nbrAvailable = 0;
        foreach($profileOptions as $option) {
            if($option['is_available'] === true) {
                $nbrAvailable++;
            }
        }

        return $nbrAvailable;
    }


    /**
     * Sum of the total "competences" levels of the player
     *
     * @param array $profileCompetences The competences of the player, as structured
     *                                  by Filter->getProfileSkills()
     */
    function competencesPoints($profileCompetences) {

        $nbr = 0;
        foreach($profileCompetences as $competence) {
            $nbr += $competence['level'];
        }

        return $nbr;
    }


    /**
     * Calculates the amount of action points earned by the player since the given date.
     *
     * @param \DateTimeImmutable $lastApRefreshUtc The last time when the action points have been updated in the database.
     *                                 MUST be in UTC timezone (avoids confusions with timezones or/and summer hour)
     *                                 And MUST be in SQL datetime format, e.g. : "2021-10-31 17:28:00".
     */
    function actionPointsEarned(\DateTimeImmutable $lastApRefreshUtc) {
        $pointsPerSecond = Config::ACTION_POINTS_PER_SECOND;
        $maxPoints       = Config::MAX_ACTION_POINTS;

        // The current date to calculate the time gap. Warning: must use the same timezone
        // than the date registered in the database (UTC)
        $dateTimeNow = new \DateTimeImmutable();

        // Difference in seconds between the two dates
        $diffSeconds = ($dateTimeNow->getTimestamp() - $lastApRefreshUtc->getTimestamp());
        $newActionPoints = floor($pointsPerSecond * $diffSeconds);

        // You can't store more than XXX action points
        return min($maxPoints, $newActionPoints);
    }
}
