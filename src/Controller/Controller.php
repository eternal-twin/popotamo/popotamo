<?php

namespace Popotamo\Controller;

class Controller extends AbstractController
{
    public function error($code=500, $throwable=null)
    {
        http_response_code($code);

        return $this->render("errors/{$code}", [
            'error' => $throwable
        ]);
    }
}