<?php

namespace Popotamo\Controller;

use Popotamo\Kernel;
use Popotamo\Service\Response;
use Popotamo\Service\Session;

abstract class AbstractController
{
    private Session $session;
    private Kernel $kernel;

    public function __construct()
    {
        global $kernel;
        
        $this->kernel  = $kernel;
        $this->session = new Session();
    }

    protected function getSession()
    {
        return $this->session;
    }

    protected function getDb()
    {
        return $this->kernel->getDb();
    }

    protected function getUser()
    {
        return $this->kernel->getUser();
    }

    protected function getFirewall()
    {
        return $this->kernel->getFirewall();
    }

    protected function getRequest()
    {
        return $this->kernel->getRequest();
    }

    protected function render($template, array $vars = []) 
    {
        $renderer = new class {};
        $function = (function($template, $vars) {
            foreach ($vars as $name => $value) {
                $$name = $value; 
            }

            ob_start();
            require Kernel::getPagesDir()."/{$template}.php";
    
            return new Response(ob_get_clean());
        });

        $function = $function->bindTo($renderer);
        return $function($template, $vars);
    }
}