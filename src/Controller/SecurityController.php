<?php

namespace Popotamo\Controller;

use Eternaltwin\Client\Auth;
use Eternaltwin\Client\HttpEtwinClient;
use Eternaltwin\OauthClient\RfcOauthClient;
use Popotamo\Config;
use Popotamo\Exception\UnauthenticatedException;
use Popotamo\Model\DbPopotamo;
use Popotamo\Router;
use Throwable;

class SecurityController extends Controller
{
    public function login()
    {
        $OAUTH_CLIENT = new RfcOauthClient(
            Config::get('server.etwinUri') . "oauth/authorize",
            Config::get('server.etwinUri') . "oauth/token",
            Config::get('server.popotamoUri') . "oauth/callback",
            Config::get('server.etwinClientId'),
            Config::get('server.etwinClientSecret')
        );
        
        // No need to change this (sets the privileges and Eternaltwin has only
        // one level of permissions at the moment)
        $scope = 'base';
        
        $state = 'http://popotamo';
        
        Router::redirectTo($OAUTH_CLIENT->getAuthorizationUri($scope, $state));
    }

    public function callback()
    {
        $OAUTH_CLIENT = new RfcOauthClient(
            Config::get('server.etwinUri') . "oauth/authorize",
            Config::get('server.etwinUri') . "oauth/token",
            Config::get('server.popotamoUri') . "oauth/callback",
            Config::get('server.etwinClientId'),
            Config::get('server.etwinClientSecret')
        );


        $code  = $_GET["code"];
        $state = $_GET["state"];
        try {
            $accessToken = $OAUTH_CLIENT->getAccessTokenSync($code);
        } catch (Throwable $e) {
            throw new UnauthenticatedException($e->getMessage(), 0, $e);
        }

        /**
         * Authentificaton through Eternaltwin
         */
        $apiClient = new HttpEtwinClient(Config::get('server.etwinUri'));
        $self = $apiClient->getSelf(Auth::fromToken($accessToken->getAccessToken()));

        $user = $self->getUser();
        $userDisplayName = $user->getDisplayName()->getCurrent()->getValue()->toString();
        $userUuid        = $user->getId()->getInner()->toString();

        /**
         * Link Eternaltwin's UUID to the user saved in the Popotamo database
         */

        // If user not already connected
        if ($this->getSession()->empty()) {
            $DbPopotamo = new DbPopotamo();

            $DbPopotamo->connect();
            // Tries to get the user data registered in the Popotamo's database
            $this->getFirewall()->loadUser($userUuid);
            $user = $this->getUser();
            // Always ensure the user exists and is up-to-date.
            $DbPopotamo->upsertUser($userUuid, $userDisplayName);

            $profileFrontToken = bin2hex(random_bytes(20));
            // If the user does not exist (new account on ET, or old account but first visit
            // on Popotamo), we create it in the Popotamo's database
            if($user === null) {
                $popotamoUserId    = $userUuid;
                $popotamoProfileId = $DbPopotamo->createProfile($popotamoUserId, $userDisplayName, $profileFrontToken);
            } else {
                $popotamoUserId    = $user->getId();
                $popotamoProfileId = $DbPopotamo->updateProfile($user->getProfile()->getId(), $userDisplayName, $profileFrontToken);
            }

            $DbPopotamo->disconnect();

            // Memorize the user data in a PHP session
            $this->getSession()->set('eternaltwinUuid', $userUuid);
            $this->getSession()->set('popotamoUserId', $popotamoUserId);
            $this->getSession()->set('popotamoProfileId', $popotamoProfileId);
            $this->getSession()->set('displayName', $userDisplayName);
            $this->getSession()->set('authToken', $profileFrontToken);
            $this->getSession()->set('sessionStart', time()); // Useful for timeout
        }

        Router::redirectTo("/");
    }
}