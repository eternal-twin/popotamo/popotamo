<?php

namespace Popotamo\Controller;


/**
 * Verifications implying the inventories (= the letters each player owns)
 */
class InventoryValidator
{
    
    /**
     * Chacks that all the given letters exist in the player's inventory.
     * Useful to detect if the player cheats by sending custom letters through 
     * the javascript request.
     * 
     * @param array $newLetters List of the letters the plyers has placed on the board
     * @param array $inventoryLetters List of the letters in the player's inventory
     * @return bool Returns False if one or more letters placed are not in his inventory.
     */
    function areAllLettersInInventory($newLetters, $inventoryLetters) {

        $commonLettersIds = array_intersect( $newLetters, $inventoryLetters );
        
        return (count($commonLettersIds) === count($newLetters)) ? true : false;
    }
}
