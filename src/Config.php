<?php declare(strict_types=1);

namespace Popotamo;

final class Config {
    private static ?self $instance = null;

    const BOARD_COLS = 19;
    const BOARD_ROWS = 19;
    /**
     * The amount of AP the player automatically earns each second
     */
    const ACTION_POINTS_PER_SECOND = 1 / 60;

    /**
     * The player can't store more than XXX action points
     */
    const MAX_ACTION_POINTS = 500;

    final public function __construct(private array $config) {}

    /**
     * @return string The database DSN string.
     */
    final public static function dsn(): string {
        return "pgsql:"
            . "host=" . self::get('db.host') . ";"
            . "port=" . self::get('db.port') . ";"
            . "dbname=" . self::get('db.dbname');
    }

    final public static function get($key, $default=null)
    {
        self::getConfig();

        $subKeys = explode('.', $key);
        $configEntry = self::$instance->config;
        foreach ($subKeys as $subKey) {
            $configEntry = $configEntry[$subKey]??$default;
        }

        return $configEntry??null;
    }

    /**
     * Read the local configuration file and create a `Config` instance.
     *
     * @throws \JsonException
     */
    private static function getConfig(): self 
    {
        $config = [];
        if (self::$instance == null) {
            foreach ([
                'db'         => Kernel::getProjectDir() . "/_CONFIG/database-logins.json",
                'server'     => Kernel::getProjectDir() . "/_CONFIG/config-server.json"
            ] as $key => $configFile) {
                $configJson   = file_get_contents($configFile);
                $config[$key] = json_decode($configJson, true, 512, JSON_THROW_ON_ERROR);
            }

            self::$instance = new self($config);
        }

        return self::$instance;
    }
}
