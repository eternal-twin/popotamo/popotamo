<?php

namespace Popotamo;

use Popotamo\Controller\AdminController;
use Popotamo\Controller\ApiController;
use Popotamo\Controller\HomeController;
use Popotamo\Controller\SecurityController;
use Popotamo\Controller\SseController;
use Popotamo\Exception\RouteNotFoundException;
use Popotamo\Service\Response;
use Popotamo\Service\Session;

class Router 
{
    private array $routeCollection;
    private Session $session;
    private array $headers;
    private array $args;

    public function __construct(array $routeCollection)
    {
        $this->routeCollection = $routeCollection;    
        $this->session         = new Session();
        $this->headers         = getallheaders();
        $this->args            = [];

        $this->session->unsetIfStale();
    }

    private function requireInGame($gameId)
    { 
    }

    public function matchRoute($path): array|false
    {
        foreach ($this->routeCollection as $route => $method) {
            if (preg_match($route, $path, $matches)) {
                array_shift($matches);
                $args = [];
                foreach ($matches as $key => $match) {
                    if (is_numeric($key)) {
                        $args[] = $match;
                    } else {
                        $this->args[$key] = $match;
                    }
                }

                return [$method, $args]; 
            }   
        }

        return false;
    }

    public function run($path): Response
    {
        list($method, $args) = $this->matchRoute($path);

        if (!$method) {
            throw new RouteNotFoundException();
        }

        $response = call_user_func_array([$this, $method], $args);

        ob_end_clean();
        if (!($response instanceof Response)) {
            die('Should return Reponse instance');
        }
        
        return $response;
    }

    public static function redirectTo($url, int $redirectType=302)
    {
        http_response_code($redirectType);
        header("Location: {$url}");
        exit;
    }

    //----------------------------------------//
    //--------------   Routes   --------------//
    //----------------------------------------//

    /**
     * Route /api/skills/use
     *
     * @return void
     */
    protected function apiUseSkill() 
    {
        $controller = new ApiController();
        return $controller->useSkill();
    }

    /**
     * Route /api/popotamo
     *
     * @return void
     */
    protected function apiValidateWord()
    {
        $controller = new ApiController();
        return $controller->validateWord();
    }

    /**
     * Route /api/profiles
     *
     * @return void
     */
    protected function apiGameProfiles($gameId)
    {
        $this->requireInGame($gameId);

        $controller = new ApiController();
        return $controller->getGameProfiles($gameId);
    }
    
    /**
     * Route /api/games/{gameId}/score-estimate
     *
     * @return void
     */
    protected function apiScoreEstimate($gameId)
    {
        $this->requireInGame($gameId);

        $controller = new ApiController();
        return $controller->getEstimatedScore($gameId);
    }
    
    /**
     * Route /api/games/{gameId}/chat
     *
     * @return void
     */
    protected function apiChat($gameId)
    {
        $this->requireInGame($gameId);

        $controller = new ApiController();
        return $controller->postChatMessage($gameId);
    }

    /**
     * Route /login
     * 
     * @return void
     */
    protected function login()
    {
        $controller = new SecurityController();
        return $controller->login();
    }

    /**
     * Route /oauth/callback
     * 
     * @return void
     */
    protected function oAuthCallback()
    {
        $controller = new SecurityController();
        return $controller->callback();
    }

    /**
     * Route /game/{id}
     * 
     * Displays a running game, if the player is in it
     *
     * @return void
     */
    //TODO use a Controller
    protected function showGame($gameId) 
    {
        ob_start();
        $profileId = (int)$_SESSION['popotamoProfileId'];
        $isHttps   = $_SERVER['REQUEST_SCHEME'] === 'https';
        require PAGE_DIR.'/game.php';

        return new Response(ob_get_clean());
    }

    /**
     * Route /minishop
     * 
     * Displays a running game, if the player is in it
     *
     * @return void
     */
    protected function minishop($gameId) 
    {
        ob_start();
        require PAGE_DIR.'/minishop.php';

        return new Response(ob_get_clean());
    }

    /**
     * Route /logout
     * 
     * Destroys the user's session and redirects to landing page
     *
     * @return void
     */
    protected function logout()
    {
        $this->session->destroy();
        self::redirectTo("/");
    }

    protected function stats()
    {
        ob_start();
        require PAGE_DIR.'/stats.php';
        
        return new Response(ob_get_clean());
    }

    protected function healthcheck()
    {
        ob_start();
        require PAGE_DIR.'/healthcheck.php';

        return new Response(ob_get_clean());
    }

    /**
     * Route /
     * 
     * If logged in, will display their game selection page
     * If not, will display the landing page
     *
     * @return void
     */
    protected function index() 
    {
        $controller = new HomeController();
        return $controller->index();
    }

    /**
     * Route /admin
     * 
     * @return void
     */
    protected function adminDashboard() 
    {
        $controller = new AdminController();
        return $controller->dashboard();
    }

    /**
     * Route /admin/games
     * 
     * @return void
     */
    protected function adminGames() 
    {
        $controller = new AdminController();
        return $controller->games();
    }

    /**
     * Route /admin/games/{gameId}
     * 
     * @return void
     */
    protected function adminGame($gameId) 
    {
        $controller = new AdminController();
        return $controller->game($gameId);
    }

    /**
     * Route /admin/socket
     * 
     * @return void
     */
    protected function adminSocket() 
    {
        $controller = new AdminController();
        return $controller->socket();
    }
}