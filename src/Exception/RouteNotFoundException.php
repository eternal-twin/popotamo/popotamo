<?php

namespace Popotamo\Exception;

use Exception;

class RouteNotFoundException extends Exception
{}