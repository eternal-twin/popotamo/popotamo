<?php

namespace Popotamo\Entity;

use Popotamo\Security\Firewall;

class User extends Entity
{
    private string $id;
    private string $displayName;
    private array $roles;

    private Profile $profile;

    protected static function getColumns(): array
    {
        return [
            'user_id'      => 'setId',
            'display_name' => 'setDisplayName',
            'user_roles'   => 'setRoles',
            'profile_id'   => 'setProfile',
            'quids'        => 'setProfileQuids',
        ];
    }
    
    protected function transform($column, $value): mixed
    {
        switch ($column) {
            case 'user_roles':
                return json_decode($value, true);
                break;
        }

        return $value;
    }

    /**
     * Get the value of id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param string $id
     *
     * @return self
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of displayName
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * Set the value of displayName
     *
     * @param string $displayName
     *
     * @return self
     */
    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get the value of roles
     *
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * Set the value of roles
     *
     * @param array $roles
     *
     * @return self
     */
    public function setRoles(?array $roles): self
    {
        $this->roles = $roles ?? [Firewall::ROLE_USER];

        return $this;
    }

    public function addRole(?string $role): self
    {
        if (!in_array($role, $this->roles)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function getProfile(): ?Profile
    {
        return $this->profile;
    }

    public function setProfile(Profile $profile): self
    {
        $profile->setUser($this);
        $this->profile = $profile;

        return $this;
    }
}