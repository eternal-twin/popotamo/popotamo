<?php

namespace Popotamo\Entity\Socket;

use Generator;
use JsonSerializable;
use Popotamo\Entity\Socket\Connection\Connection;
use Popotamo\Entity\Socket\Response\JsonResponse;
use Popotamo\Entity\Socket\Response\Response;

class Server implements JsonSerializable
{
    private static $default_options = [
        'filter'        => ['text', 'binary'],
        'fragment_size' => 4096,
        'logger'        => null,
        'port'          => 12000,
        'return_obj'    => false,
        'timeout'       => null,
    ];

    private int $startTime;

    private array    $options;
    private          $socket;
    private int      $port;
    /**
     * @var Connection[]
     */
    private array   $connections = [];
    private array   $sockets     = [];
    /**
     * @var SocketRoom[]
     */
    private array   $rooms       = [];
    private int     $socketID    = 0;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options       = array_merge(self::$default_options, $options);
        $this->port          = $this->options['port'];
    }

    public function init() 
    {
        $count           = 0;
        $timeout         = 60;
        $this->startTime = time();

        while (true) {
            $stream = @stream_socket_server("tcp://0.0.0.0:{$this->port}", $errCode, $errMsg, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN);
            if ($stream) {
                $this->socket = $stream;
                break;
            }

            $count++;
            yield "({$count}/{$timeout}) {$errMsg}";
            sleep(1);
        }

        yield !!$stream;

        // stream_set_blocking($this->socket, false);

        $this->sockets      = [-1 => $this->socket];
        $this->connections  = [];
        $this->socketID     = 0;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function accept()
    {
        $newsock = @stream_socket_accept($this->socket, .2, $name);
        if ($newsock !== false) {
            [$address, $port] = explode(':', $name);
            $connection = new Connection($newsock, $address, intval($port));
            
            if ($this->handshake($connection)) {
                $this->socketID++;
                $connection->setId($this->socketID);
                $this->connections[$this->socketID] = $connection;
                $this->sockets[$this->socketID]     = $newsock;

                return $connection;
            }

            stream_socket_shutdown($connection, STREAM_SHUT_RDWR);
        }
    }

    /**
     * @return Message[]
     */
    public function receive(): ?Generator
    {
        $changed = $this->sockets;
        stream_select($changed, $null, $null, 0, 200000);

        foreach ($changed as $index => $activeSocket) {
            if (!array_key_exists($index, $this->connections)) {
                continue;
            }

            $connection = $this->connections[$index];
            $message    = $connection->receive();

            //-- Connection closed
            if (!$message) {
                $this->closeConnection($this->connections[$index]);
                continue;
            }

            yield $message;
        }
    }

    public function close()
    {
        stream_socket_shutdown($this->socket, STREAM_SHUT_RDWR);

        $this->socket      = null;
        $this->connections = [];
    }

    public function upgradeConnection(Connection $connection, string $class)
    {
        $this->connections[$connection->getId()] = $connection->transformInto($class);

        return $this->connections[$connection->getId()];
    }

    public function closeConnection(Connection $connection) 
    {
        $this->connections[$connection->getId()]->setDead(true);
        unset($this->sockets[$connection->getId()]);

        foreach ($this->connections[$connection->getId()]->getRooms() as $roomName) {
            $this->rooms[$roomName]->send(new JsonResponse(['eventType' => 'playerQuitting', 'profile' => ['id' => $connection->getProfile()->getId()]]));
        }
    }

    public function room(string $name)
    {
        if (!isset($this->rooms[$name])) {
            $this->rooms[$name] = new SocketRoom($name);
        }

        return $this->rooms[$name];
    }

    public function getRooms()
    {
        return $this->rooms;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'activeConnections' => array_reduce(
                $this->connections, 
                function($carry, $connection) {
                    $carry += ($connection->isDead() ? 0 : 1);
                    return $carry;
                }, 
                0
            ),
            'port'              => $this->port,
            'connections'       => $this->connections,
            'rooms'             => $this->rooms,
            'memory'            => memory_get_usage(true),
            'uptime'            => time() - $this->startTime
        ];
    }

    /**
     * Handles first exchange with the browser. 
     * Check HTTP header and return the socket protocol data
     *
     * @param Connection $socket
     * @return void
     */
    private function handshake(Connection &$connection)
    {
        $reading = true;
		$buffer  = "";
		while ($reading) {
			$buffer .= $connection->read(1);

			if (strpos($buffer, "\r\n\r\n") !== false){
				$reading = false; // Stop the reading loop
			}
		}

		// Split HTTP into individual lines
		$lines = explode("\r\n", $buffer);

		// Parse headers
		// Ignore first index - it's the HTTP type
		$headers = [];
		foreach ($lines as $index => $header) {
			if ($index > 0){
				// Trim the header of whitespace - is it blank?
				if (trim($header) !== ""){
					// Extract the header name and value
					preg_match("/([^\:]+): \s*(.+)/", $header, $matches);
					$headers[$matches[1]] = $matches[2];
				}
			}
		}

		// Assume Connection is "upgrade"
		$key = $headers['Sec-WebSocket-Key']; // Get the key
		$uuid = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"; // This is a constant
		$result = sha1($key . $uuid, true); // true to output raw
		$result = base64_encode($result);

		// Respond to the request with handshake headers
		$response = "HTTP/1.1 101 Switching Protocols\r\n";
		$response .= "Upgrade: websocket\r\n";
		$response .= "Connection: upgrade\r\n";
		$response .= "Sec-WebSocket-Accept: $result\r\n";

		$response .= "\r\n"; // End all HTTP headers with a set of \r\n (blank line)

        $connection->send(new Response($response));

        return true;
    }
}