<?php

namespace Popotamo\Entity\Socket\Connection;

use Error;
use JsonSerializable;
use Popotamo\Entity\Socket\Message;
use Popotamo\Entity\Socket\Response\Response;
use ReflectionClass;

class Connection implements JsonSerializable
{
	const WS_FIN =  128;
	const WS_MASK = 128;

	const WS_OPCODE_CONTINUATION = 0;
	const WS_OPCODE_TEXT =         1;
	const WS_OPCODE_BINARY =       2;
	const WS_OPCODE_CLOSE =        8;
	const WS_OPCODE_PING =         9;
	const WS_OPCODE_PONG =         10;

	const WS_PAYLOAD_LENGTH_16 = 126;
	const WS_PAYLOAD_LENGTH_63 = 127;

	const WS_READY_STATE_CONNECTING = 0;
	const WS_READY_STATE_OPEN =       1;
	const WS_READY_STATE_CLOSING =    2;
	const WS_READY_STATE_CLOSED =     3;

    protected string $id             = "";
    protected string $username       = "";
    protected int    $lastUpdateTime;
    protected bool   $isDead         = false;
    protected array  $rooms          = [];

    public function __construct(protected $socket, protected string $address, protected int $port) 
    {
        $this->lastUpdateTime = time();
    }

    public function setId(string $id) 
    {
        $this->id = $id;

        return $this;
    }

    public function setBlocking(bool $blocking)
    {
        call_user_func('socket_set_'.($blocking ? '' : 'non').'block', $this->socket);
    } 

    public function getId(): string
    {
        return $this->id;
    }

    public function isStale() 
    {
        return (time() - $this->lastUpdateTime) > 5;
    }

    public function setDead(bool $dead)
    {
        $this->isDead = $dead;

        return $this;
    }

    public function isDead()
    {
        return $this->isDead;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function addRoom(string $roomName)
    {
        $this->rooms[$roomName] = $roomName;
    }

    public function getRooms()
    {
        return $this->rooms;
    }

    public function touch() 
    {        
        return $this->lastUpdateTime = time();
    }

    public function read(int $length=4096)
    {
        try {
            $data = stream_socket_recvfrom($this->socket, $length, MSG_DONTWAIT);
        } catch (Error $e) {
            if (strpos($e->getMessage(), 'has already been closed') !== false) {
                return false;
            }

            return "";
        }

        return $data;
    }

    public function send(Response $response) 
    {
        $responseContent = $response->getContent();
        switch (true) {
            case $response->getCode() & Response::CODE_PONG:
                $opcode = self::WS_OPCODE_PONG;
                break;
                
            case $response->getCode() & Response::CODE_PING:
                $opcode = self::WS_OPCODE_PING;
                break;
                
            case $response->getCode() & Response::CODE_KILL:
                $opcode = self::WS_OPCODE_CLOSE;
                break;

            default:
                $opcode = self::WS_OPCODE_TEXT;
        }

        // fetch message length
		$messageLength = strlen($responseContent);

		// set max payload length per frame
		$bufferSize = 4096;

		// work out amount of frames to send, based on $bufferSize
		$frameCount = ceil($messageLength / $bufferSize);
		if ($frameCount == 0) $frameCount = 1;

		// set last frame variables
		$maxFrame = $frameCount - 1;
		$lastFrameBufferLength = ($messageLength % $bufferSize) != 0 ? ($messageLength % $bufferSize) : ($messageLength != 0 ? $bufferSize : 0);

		// loop around all frames to send
		for ($i=0; $i<$frameCount; $i++) {
			// fetch fin, opcode and buffer length for frame
			$fin = $i != $maxFrame ? 0 : self::WS_FIN;
			$opcode = $i != 0 ? self::WS_OPCODE_CONTINUATION : $opcode;

			$bufferLength = $i != $maxFrame ? $bufferSize : $lastFrameBufferLength;

			// set payload length variables for frame
			if ($bufferLength <= 125) {
				$payloadLength = $bufferLength;
				$payloadLengthExtended = '';
				$payloadLengthExtendedLength = 0;
			}
			elseif ($bufferLength <= 65535) {
				$payloadLength = self::WS_PAYLOAD_LENGTH_16;
				$payloadLengthExtended = pack('n', $bufferLength);
				$payloadLengthExtendedLength = 2;
			}
			else {
				$payloadLength = self::WS_PAYLOAD_LENGTH_63;
				$payloadLengthExtended = pack('xxxxN', $bufferLength); // pack 32 bit int, should really be 64 bit int
				$payloadLengthExtendedLength = 8;
			}

			// set frame bytes
			$buffer = pack('n', (($fin | $opcode) << 8) | $payloadLength) . $payloadLengthExtended . substr($responseContent, $i*$bufferSize, $bufferLength);

			$left = 2 + $payloadLengthExtendedLength + $bufferLength;
			do {
                try {
                    $sent = @stream_socket_sendto($this->socket, $buffer);
                } catch (Error $e) {
                    return false;
                }
				if ($sent === false) return false;

				$left -= $sent;
				if ($sent > 0) $buffer = substr($buffer, $sent);
			}
			while ($left > 0);
		}

		return true;
    }

    public function receive() 
    {
        $data = "";
        if ($string = $this->read()) {
            $data .= $string;
        }

        if (socket_last_error() && socket_last_error() != 11) {
            return false;
        }

        if (empty($data)) {
            return false;
        }

        // Extract the first byte which contains FIN, RSV1, RSV2, RSV3, and opcode
        $firstByte  = ord($data[0]);
        $secondByte = ord($data[1]);
        
        // Check if the frame is masked (the 1st bit of the 2nd byte)
        $isMasked = ($secondByte & 0b10000000) >> 7; // Extract mask bit

        // Get the payload length (7 least significant bits of the 2nd byte)
        $payloadLength = $secondByte & 0b01111111; // Extract 7-bit payload length

        $offset = 2; // Start reading after the first two bytes

        if ($payloadLength == 126) {
            // Payload length is in the next 2 bytes
            $payloadLength = unpack('n', substr($data, $offset, 2))[1]; // 'n' for 16-bit unsigned short
            $offset += 2;
        } elseif ($payloadLength == 127) {
            // Payload length is in the next 8 bytes (64-bit)
            $payloadLength = unpack('J', substr($data, $offset, 8))[1]; // 'J' for 64-bit unsigned long
            $offset += 8;
        }

        // If the message is masked, extract the 4-byte masking key
        $maskingKey = '';
        if ($isMasked) {
            $maskingKey = substr($data, $offset, 4); // Masking key is 4 bytes long
            $offset += 4;
        }

        // Extract the actual payload
        $payload = substr($data, $offset, $payloadLength);

        // If masked, unmask the payload
        if ($isMasked) {
            $unmaskedPayload = '';
            for ($i = 0; $i < $payloadLength; $i++) {
                // XOR each byte of the payload with the masking key, cycling through the key
                $unmaskedPayload .= $payload[$i] ^ $maskingKey[$i % 4];
            }
            $payload = $unmaskedPayload;
        }

        //FIXME When closing, the socket sends a weird character 
        if ($payloadLength < 4) {
            return false;
        }

        return new Message($this, $payload);
    }

    public function transformInto(string $class)
    {
        $class = "Popotamo\\Entity\\Socket\\Connection\\{$class}";

        $newConnection = new $class($this->socket, $this->address, $this->port);

        $reflectionConnection     = new ReflectionClass($this);
        $reflectionNewConnection  = new ReflectionClass($newConnection);

        foreach ($reflectionConnection->getProperties() as $property) {
            $property->setAccessible(true);
            $value = $property->getValue($this);

            $newProperty = $reflectionNewConnection->getProperty($property->getName());
            $newProperty->setAccessible(true);
            $newProperty->setValue($newConnection, $value);
        }

        return $newConnection;
    }

    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'username' => $this->getUsername(),
            'address'  => $this->address,
            'port'     => $this->port,
            'state'    => $this->isDead() ? 'dead' : 'open',
        ];
    }
}