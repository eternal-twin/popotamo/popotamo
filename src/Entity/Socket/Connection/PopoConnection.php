<?php

namespace Popotamo\Entity\Socket\Connection;

use Popotamo\Entity\Profile;

class PopoConnection extends Connection
{
    private ?Profile $profile = null;

    public function setProfile(Profile $profile)
    {
        $this->profile = $profile;

        return $this;
    }

    public function getProfile()
    {
        return $this->profile;
    }

    public function getUsername()
    {
        return $this->profile->getDisplayName();
    }
}