<?php

namespace Popotamo\Entity\Socket;

use JsonSerializable;
use Popotamo\Entity\Socket\Connection\Connection;
use Popotamo\Entity\Socket\Response\Response;

class SocketRoom implements JsonSerializable {
    /**
     * @var Connection[]
     */
    private array $connections = [];

    public function __construct(private string $name) {}

    public function addConnection(Connection $connection) {
        $connection->addRoom($this->name);
        
        $this->connections[] = $connection;

        return $this;
    }

    public function send(Response $response) {
        foreach ($this->connections as $index => $connection) {
            if ($connection->isDead()) {
                $this->connections[$index] = null;
                continue;
            }

            $connection->send($response);
        }

        $this->connections = array_filter($this->connections);
    }

    public function jsonSerialize(): mixed
    {
        return [
            'name'        => $this->name,
            'connections' => $this->connections
        ];
    }
};