<?php

namespace Popotamo\Entity\Socket;

use Popotamo\Entity\Socket\Connection\Connection;

class Message
{
    private float $timestamp;

    public function __construct(private Connection $connection, private string $content)
    {
        $this->timestamp = microtime(true);
    }

    public function json()
    {
        return json_decode($this->content, true);
    }

    public function getContent()
    {
        return $this->content;
    }

    public function hasContent()
    {
        return strlen($this->content) > 0;
    }

    public function getConnection()
    {
        return $this->connection;
    }
}