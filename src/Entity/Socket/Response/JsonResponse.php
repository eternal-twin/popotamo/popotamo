<?php

namespace Popotamo\Entity\Socket\Response;

class JsonResponse extends Response
{
    public function getContent($raw=false) 
    {
        return $raw ? $this->content : json_encode($this->content);
    }
}