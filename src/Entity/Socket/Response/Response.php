<?php

namespace Popotamo\Entity\Socket\Response;

class Response
{
    public const CODE_KILL          = 1;
    public const CODE_RESPONSE      = 2;
    public const CODE_PONG          = 4;
    public const CODE_PING          = 8;

    protected $content;
    protected int $code;

    public function __construct($content, ?int $code = null)
    {
        $this->content = $content;
        $this->code    = $code??self::CODE_RESPONSE;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getContent($raw=false) 
    {
        return $this->content;
    }
}