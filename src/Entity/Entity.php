<?php

namespace Popotamo\Entity;

abstract class Entity
{
    protected abstract static function getColumns(): array;
    protected abstract function transform($column, $value): mixed;

    public static function load(array $data)
    {
        $class  = get_called_class();
        $entity = new $class();

        foreach ($data as $key => $value) {
            $method = $class::getColumns()[$key]??false;
            if ($method && method_exists($entity, $method)) {
                $entity->$method($entity->transform($key, $value));
            }
        }

        return $entity;
    }

    public function save()
    {
        
    }
}