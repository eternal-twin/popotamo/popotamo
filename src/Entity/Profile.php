<?php

namespace Popotamo\Entity;

use Popotamo\Model\GameProfile;

class Profile extends Entity
{
    private int $id;
    private string $displayName;
    private int $quids;
    private ?string $frontToken;
    private array $skills       = [];
    private array $gameProfiles = [];
    private User $user;
    
    protected static function getColumns(): array
    {
        return [
            'profile_id'    => 'setId',
            'display_name'  => 'setDisplayName',
            'quids'         => 'setQuids',
            'skills'        => 'setSkills',
            'gameProfiles'  => 'setGameProfiles',
            'front_token'   => 'setFrontToken'
        ];
    }
    
    protected function transform($column, $value): mixed
    {
        return $value;
    }

    /**
     * Get the value of id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param int $id
     *
     * @return self
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of quids
     *
     * @return int
     */
    public function getQuids(): int
    {
        return $this->quids;
    }

    /**
     * Set the value of quids
     *
     * @param int $quids
     *
     * @return self
     */
    public function setQuids(int $quids): self
    {
        $this->quids = $quids;

        return $this;
    }

    /**
     * Get the value of displayName
     *
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }

    /**
     * Set the value of displayName
     *
     * @param string $displayName
     *
     * @return self
     */
    public function setDisplayName(string $displayName): self
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get the value of user
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of frontToken
     *
     * @return string
     */
    public function getFrontToken(): string
    {
        return $this->frontToken;
    }

    /**
     * Set the value of frontToken
     *
     * @param string $frontToken
     *
     * @return self
     */
    public function setFrontToken(?string $frontToken): self
    {
        $this->frontToken = $frontToken;

        return $this;
    }

    /**
     * Get the value of skills
     *
     * @return array
     */
    public function getSkills(): array
    {
        return $this->skills;
    }

    /**
     * Set the value of skills
     *
     * @param array $skills
     *
     * @return self
     */
    public function setSkills(array $skills): self
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get the value of gameProfiles
     *
     * @return array
     */
    public function getGameProfiles(): array
    {
        return $this->gameProfiles;
    }

    /**
     * Set the value of frontToken
     *
     * @param array $frontToken
     *
     * @return self
     */
    public function setGameProfiles(array $gameProfiles): self
    {
        $this->gameProfiles = $gameProfiles;

        return $this;
    }

    public final function getGameProfileFromGameId(int $gameId): ?GameProfile {
        foreach($this->gameProfiles as $gameProfile) {
            if ($gameProfile->game->gameId == $gameId) {
                return $gameProfile;
            }
        }
        return null;
    }
}