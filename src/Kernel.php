<?php

namespace Popotamo;

use Popotamo\Controller\Controller;
use Popotamo\Exception\RestrictedAccessException;
use Popotamo\Exception\RouteNotFoundException;
use Popotamo\Exception\UnauthenticatedException;
use Popotamo\Model\Database;
use Popotamo\Security\Firewall;
use Popotamo\Service\Profiler;
use Popotamo\Service\Request;
use Throwable;

class Kernel
{
    private Database $db;
    private Firewall $firewall;
    private Router $router;
    private Request $request;

    private array $routeCollection;

    public function __construct()
    {
        $this->db = new Database();
        $this->db->connect();
        
        $this->firewall = new Firewall($this->db);
        $this->request  = new Request();
    }

    public function getDb(): Database
    {
        return $this->db;
    }

    public function loadRouteCollection($routeCollection) 
    {
        $this->routeCollection = $routeCollection;
        $this->router          = new Router($this->routeCollection);
    }

    public function getRequest() 
    {
        return $this->request;
    }

    public function handle($route) 
    {
        try {
            $this->firewall->auth();
            $this->firewall->check($route);
            $response = $this->router->run($route);
        } catch (UnauthenticatedException $e) {
            $error = $e;
            $response = Router::redirectTo('/login');
        } catch (RestrictedAccessException $e) {
            $error = $e;
            $response = (new Controller())->error(403, $e);
        } catch (RouteNotFoundException $e) {
            $error = $e;
            $response = (new Controller())->error(404, $e);
        } catch (Throwable $e) {
            $error = $e;
            $response = (new Controller())->error(500, $e);
        }

        if (Config::get('server.deployment.profiling', false) == true) {
            if ($this->request->getHeader('X-Requested-With') === "XMLHttpRequest") {
                $response->append(['profiler' => [
                    "exec_time"    => Profiler::getExecutionTime(),
                    "memory"       => Profiler::getMemoryPeakUsage(),
                    "memory_limit" => Profiler::getMemoryLimit(),
                    "Queries"      => Profiler::getQueries(),
                ]]);
            } else {
                $response->append("<pre>
        Execution time  : ".Profiler::getExecutionTime()."s
        Memory          : ".Profiler::getMemoryPeakUsage()."/".Profiler::getMemoryLimit()."
        Queries         : ".count(Profiler::getQueries())."
        ".var_export(Profiler::getQueries(), true)."
        ".(!empty($error) ? "Error : ".var_export($error, true) : "")." 
                </pre>");
            }
        }

        $response->send();
    }

    public function getUser()
    {
        return $this->firewall->getUser();
    }

    public function getFirewall()
    {
        return $this->firewall;
    }

    public function isHttps(): bool
    {
        return $this->request->getScheme() === 'https';
    }

    final public static function getPagesDir(): string
    {
        return self::getProjectDir()."/src/Pages";
    }

    final public static function getProjectDir(): string
    {
        return realpath(__DIR__.'/../');
    }

    final public static function getLogDir(): string
    {
        $logDir = self::getProjectDir()."/var/log";
        if (!is_dir($logDir)) {
            mkdir($logDir, 0744, true);
        }

        return $logDir;
    }
}