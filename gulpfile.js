import gulp                                   from 'gulp';
const { watch, series, parallel, src, dest } = gulp;
import prefixer                               from 'gulp-autoprefixer';
import cleanCSS                               from 'gulp-clean-css';
import babel                                  from 'gulp-babel';
import uglify                                 from 'gulp-uglify';
import gulpSass                               from 'gulp-sass';
import * as sassCompiler                      from 'sass';

const sass = gulpSass(sassCompiler);

function cssMinify(cb) {
    src(['assets/scss/*.scss'])
        .pipe(sass())
        .pipe(prefixer())
        .pipe(cleanCSS())
        .pipe(dest('public/resources'))
    ;

    cb();
}

function jsApp(cb) {  
  src('assets/js/**/*.js')
      .pipe(babel())
      .pipe(uglify())
      .pipe(dest('public/resources'))
  ;

  cb();
}

function watchFiles(cb) {
    watch('assets/js/**/*.js', series(jsApp));
    watch('assets/scss/**/*.scss', series(cssMinify));

    cb();
}

const deploy  = series(jsApp, cssMinify);

export default series(jsApp, cssMinify, watchFiles);
export { deploy };