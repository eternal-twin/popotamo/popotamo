/**
 * This is a small lib to call an API and get the returned response
 */
class UnauthorizedAccessError extends Error {
    constructor(message) { super(message); this.name = "UnauthorizedAccessError"; }
}
class ForbiddenAccessError extends Error {
    constructor(message) { super(message); this.name = "ForbiddenAccessError"; }
}
class LockedAccessError extends Error {
    constructor(message) { super(message); this.name = "LockedAccessError"; }
}

/**
 * Sends data to a PHP script with the GET or POST method
 * 
 * @param {string} method     The HTTP method to send the data (GET or POST)
 * @param {string} scriptName The name of the PHP script to call
 * @param {string} params     The additional parameters to send, as a unique string
 *                            E.g.: "action=get&citizen_id=87"
 */
 async function sendData(method, url, params) {
    let apiUrl = url,
        option = {
            method: method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'X-Requested-With': 'XMLHttpRequest'
            }
        };

    if (params) {
        if(method==="GET") {
            apiUrl += "?"+params;
        } else {
            option.body= params;
        }
    }
    
    // For debugging : uncomment this line to watch in the browser console 
    // how many times you call the distant APIs, and eventually optimize the redundant calls
//    console.log("API call: "+scriptName);

    try {
        return await fetch(apiUrl, option).then(checkResponse).then(toJson);
    } catch (error) {
        if (error instanceof UnauthorizedAccessError) {
            console.warn("Merci de vous connecter pour accéder à cette commande");
            location.href = "/";
        }
        if (error instanceof LockedAccessError) {
            console.warn("La partie est verrouillée");
            return;
        }

        // Prevent further app processing
        // TODO Better way of handing ? Triggers Uncaught error in browser console
        throw error;
    }
}

async function checkResponse(apiResult) {
    switch (apiResult.status) {
        case 401:
            throw new UnauthorizedAccessError();

        case 403:
            throw new ForbiddenAccessError();

        case 404:
            break;
            
        case 423:
            throw new LockedAccessError();
    }

    return await apiResult;
}


/**
 * Converts a string to JSON and prints the malformed JSON in the console
 */
async function toJson(apiResult) {
    
    try {
        return await apiResult.clone().json();
    } catch (e) {
        await apiResult.clone().text().then(apiResult=>{
            console.error(e);
            console.groupCollapsed("See the result returned by the API:");
            console.log(apiResult);
            console.groupEnd();
            throw e;
        });
    }
}
