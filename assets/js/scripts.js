"use strict";

String.prototype.replaceBulk = function(findArray, replaceArray) {
    let str = this;

    var i, regex = [], map = {}; 
    for( i=0; i<findArray.length; i++ ){ 
      regex.push( findArray[i].replace(/([-[\]{}()*+?.\\^$|#,])/g,'\\$1') );
      map[findArray[i]] = replaceArray[i]; 
    }
    regex = regex.join('|');
    str = str.replace( new RegExp( regex, 'g' ), function(matched){
      return map[matched];
    });
    
    return str;
};

/**
 * Represents a letter in the game. Can be either in inventory or on board
 */
class Letter
{
    DOMElm;
    id;
    letter;
    coordX;
    coordY;
    status;
    templateElm;

    constructor(templateElm)
    {
        this.templateElm = templateElm;
    }

    setID(id)
    {
        this.id = id;
    }

    setStatus(status)
    {
        this.status = status;

        switch (status) {
            case 'selected':
                this.getElement().classList.add("selected");
                break;

            case 'unselected':
                this.getElement().classList.remove("selected");
                break;

            case 'pending':
                this.getElement().classList.add("newletter");
                break;

            case 'fixed':
                this.getElement().classList.remove("selected");
                this.getElement().classList.remove("newletter");
                break;
        }

        return this;
    }

    setLetter(letter)
    {
        this.letter = letter;
        
        if (this.DOMElm) {
            this.DOMElm.setAttribute('data-letter', this.letter);
            this.DOMElm.querySelector('[data-letter-holder]').innerText = this.letter;
        }

        return this;
    }

    setCoord(coordX, coordY)
    {
        this.coordX = coordX;
        this.coordY = coordY;
        
        if (this.DOMElm) {
            this.DOMElm.setAttribute('data-coord-x', this.coordX);
            this.DOMElm.setAttribute('data-coord-y', this.coordY);
        }

        return this;
    }

    hasElement()
    {
        return !!this.DOMElm;
    }

    setElement(DOMElm) 
    {
        this.DOMElm = DOMElm;

        this.id     = this.DOMElm.dataset.letterId;
        this.letter = this.DOMElm.dataset.letter;
        this.coordX = this.DOMElm.dataset.coordX;
        this.coordY = this.DOMElm.dataset.coordY;
    }

    getElement() 
    {
        if (!this.DOMElm) {
            this.updateElm();
        }

        return this.DOMElm;
    }

    updateElm()
    {
        const letterElm = this.templateElm.content.cloneNode(true);

        letterElm.firstChild.setAttribute('data-letter', this.letter);
        letterElm.querySelector('[data-letter-holder]').innerText = this.letter;

        letterElm.firstChild.setAttribute('data-letter-id', this.id);
        if (this.coordX && this.coordY) {
            letterElm.firstChild.setAttribute('data-coord-x', this.coordX);
            letterElm.firstChild.setAttribute('data-coord-y', this.coordY);
        }

        if (this.DOMElm && this.DOMElm.replaceWith) {
            this.DOMElm.replaceWith(letterElm.firstChild);
        } else {
            this.DOMElm = letterElm.firstChild;
        }

        this.dirty = false;

        return this.DOMElm;
    }

    addEventListener(event, callback) 
    {
        this.getElement().addEventListener(event, callback);
    }
}

/**
 * Represents player inventory
 */
class Inventory
{
    wrapperElm;
    letters = [];

    constructor(wrapperElm)
    {
        this.wrapperElm = wrapperElm;
    }

    setLetters(letters) 
    {
        this.letters = letters;

        this.updateView();
    }

    addLetter(letter) 
    {
        this.letters.push(letter);

        this.updateView();
    }

    removeLetter(letter) 
    {
        for (const i in this.letters) {
            if (this.letters[i] == letter) {
                delete this.letters[i];
            }
        }

        this.updateView();
    }

    updateView()
    {
        const documentFragment = document.createElement("div");

        for (const i in this.letters) {
            const letter = this.letters[i].getElement();
            documentFragment.appendChild(letter);
        }

        this.wrapperElm.innerHTML = '';
        this.wrapperElm.appendChild(documentFragment);
    }

    triggerLetter(letterElm)
    {
        for (const i in this.letters) {
            if (this.letters[i].DOMElm == letterElm) {
                this.letters[i].setStatus('selected');
            }
        }
    }
}

/**
 * Handles board letters
 */
class Board 
{
    wrapperElm;
    letters        = {};
    pendingLetters = {};

    constructor(wrapperElm)
    {
        this.wrapperElm = wrapperElm;
    }

    setLetters(letterTemplate, letters) 
    {
        for (const i in letters) {
            const coordList = i.split('_');

            const letter = new Letter(letterTemplate);
            letter.setLetter(letters[i]);
            letter.setCoord(parseInt(coordList[0]), parseInt(coordList[1]));
            this.addLetter(letter);
        }
    } 

    addLetter(letter) {
        if (this.letters[writeCoordKey(letter.coordX, letter.coordY)]) {
            return;
        }
        
        if (this.pendingLetters[writeCoordKey(letter.coordX, letter.coordY)]) {
            this.pendingLetters[writeCoordKey(letter.coordX, letter.coordY)].setStatus('fixed');
            return;
        }

        this.letters[writeCoordKey(letter.coordX, letter.coordY)] = letter;

        const boardBox = this.wrapperElm.querySelector(`[data-coord-x="${letter.coordX}"][data-coord-y="${letter.coordY}"]`)
        boardBox.appendChild(letter.getElement());
    }

    placeLetter(letter, coordX, coordY)
    {
        if (this.pendingLetters[writeCoordKey(coordX, coordY)]) {
            return;
        }

        if (this.letters[writeCoordKey(coordX, coordY)]) {
            return;
        }

        letter.setCoord(coordX, coordY);
        letter.setStatus('pending');

        this.pendingLetters[writeCoordKey(letter.coordX, letter.coordY)] = letter;

        const boardBox = this.wrapperElm.querySelector(`[data-coord-x="${letter.coordX}"][data-coord-y="${letter.coordY}"]`)
        boardBox.innerHTML = '';
        boardBox.appendChild(letter.getElement());
    }

    commitPendingLetters()
    {
        for (const i in this.pendingLetters) {
            this.pendingLetters[i].setStatus('fixed');
            this.letters[writeCoordKey(this.pendingLetters[i].coordX, this.pendingLetters[i].coordY)] = this.pendingLetters[i];
        }
        this.pendingLetters = {};
    }

    clear() 
    {
        for (const i in this.letters) {
            this.letters[i].DOMElm.remove();
        }
        
        this.clearPendingLetters();

        this.letters = {};
    }

    clearPendingLetters()
    {
        for (const i in this.pendingLetters) {
            this.pendingLetters[i].DOMElm.remove();
        }

        this.pendingLetters = {};
    }

    getConnectedLetters()
    {
        const connectedLetters = [];
        
        for (const i in this.pendingLetters) {
            const x = parseInt(i.split("_")[0], 10);
            const y = parseInt(i.split("_")[1], 10);

            if (this.letters[writeCoordKey(x-1, y)] !== undefined || this.letters[writeCoordKey(x+1, y)] !== undefined ||
                this.letters[writeCoordKey(x, y-1)] !== undefined || this.letters[writeCoordKey(x, y+1)] !== undefined) {
                connectedLetters.push(this.pendingLetters[i]);
            }
        }

        return connectedLetters;
    }
}

/**
 * Handles player action points and inventory
 */
class Player
{
    actionPoints;
    actionPointsLastRefresh;
    actionPointsPerSecond;
    adjustedActionsPoints;
    maxActionsPoints;
    inventory;
    score;
    scoreGoal;
    estimatedScoreElm;

    constructor(playerData, estimatedScoreElm) 
    {
        this.estimatedScoreElm = estimatedScoreElm;

        this.setActionPoints(playerData.actionPoints);
        this.actionPointsLastRefresh = new Date(playerData.actionPointsLastRefresh);
        this.actionPointsPerSecond   = playerData.actionPointsPerSecond;
        this.maxActionsPoints        = playerData.maxActionsPoints;
        this.score                   = playerData.score;
        this.scoreGoal               = playerData.scoreGoal;
        this.refreshActionPointsCounter();
    }

    update(playerData) 
    {
        for (const key in playerData) {
            switch (key) {
                case 'score':
                    this.score = playerData[key];
                    break;
            } 
        }
    }

    setInventory(inventory)
    {
        this.inventory = inventory;
    }

    setActionPoints(actionPoints)
    {
        if (actionPoints == this.actionPoints) {
            return;
        }

        this.actionPoints = actionPoints;
        document.querySelector("#inventoryLine .actionPoints").textContent = this.actionPoints;
    }

    setActionPointsLastRefresh(actionPointsLastRefresh)
    {
        this.actionPointsLastRefresh = actionPointsLastRefresh;
    }

    /**
     * Updates the number of action points displayed next to the inventory
     * (the player earns additional action points every X minutes)
     * 
     * @returns void
     */
    refreshActionPointsCounter() 
    {
        let nowLocaleObject = new Date();
        
        let diffSeconds = (nowLocaleObject - this.actionPointsLastRefresh)/1000;
        this.adjustedActionsPoints = Math.floor(this.actionPoints + (this.actionPointsPerSecond * diffSeconds));

        document.querySelector("#inventoryLine .actionPoints").textContent = Math.min(this.maxActionsPoints, this.adjustedActionsPoints);
    }

    refreshEstimatedScore(score) 
    {
        score = score || 0;

        this.estimatedScoreElm.classList.remove('error');
        this.estimatedScoreElm.setAttribute('data-tooltip', "Votre score");

        this.estimatedScoreElm.querySelector('.normal').innerText = (this.score + score) + "/" + this.scoreGoal + (score ? " (+" + score + ")" : "");
        this.estimatedScoreElm.querySelector('.switched').innerText = (this.scoreGoal - (score + this.score)) + "⏵ 🏆";
    }
}

class Profile 
{
    id;
    isActivePlayer = false;
    username;
    actionPoints = 0;
    rank;
    score = 0;
    scoreGoal = 0;
    scorePercent = 0;
    inventoryCount = 0;
    onlineStatus = "unknown";
    previousOnlineStatus = "unknown";
    DOMElm;

    constructor(profileData) 
    {
        this.id             = profileData.id;
        this.isActivePlayer = profileData.isActivePlayer;
        this.update(profileData);
    }

    update(profileData) 
    {
        this.setValue('previousOnlineStatus', this.onlineStatus);
     
        this.setValue('username', profileData.username);
        this.setValue('actionPoints', profileData.actionPoints);
        this.setValue('rank', profileData.rank);
        this.setValue('score', profileData.score);
        this.setValue('scoreGoal', profileData.scoreGoal);
        this.setValue('inventoryCount', profileData.inventoryCount);
        this.setValue('optionsCount', profileData.optionsCount);
        this.setValue('onlineStatus', profileData.onlineStatus);

        this.updateScorePercent();
    }

    setValue(field, value) 
    {
        if (value == undefined || !value) {
            return;
        }

        this[field] = value;
    }

    updateScorePercent()
    {
        this.scorePercent = Math.floor((parseInt(this.score) / this.scoreGoal) * 100);
    }

    domUpdate(profileTemplate) 
    {
        let profileElm;

        if (!this.DOMElm) {
            profileElm = profileTemplate.content.cloneNode(true);
    
            profileElm.firstChild.setAttribute('style', "order:"+this.rank);
            profileElm.firstChild.setAttribute('data-id', this.id);
        } else {
            profileElm = this.DOMElm;
    
            profileElm.setAttribute('style', "order:"+this.rank);
            profileElm.setAttribute('data-id', this.id);
        }

        profileElm.querySelector('[data-type="online-status"]').classList.remove(`${this.previousOnlineStatus}`);
        
        profileElm.querySelector('[data-type="username"]').innerText        = this.username;
        profileElm.querySelector('[data-type="score-percent"]').innerText   = this.scorePercent + " %";
        profileElm.querySelector('[data-type="score"]').innerText           = this.score;
        profileElm.querySelector('[data-type="score-goal"]').innerText      = this.scoreGoal;
        profileElm.querySelector('[data-type="inventory-count"]').innerText = this.inventoryCount;
        profileElm.querySelector('[data-type="action-points"]').innerText   = this.actionPoints;
        profileElm.querySelector('[data-type="options-count"]').innerText   = this.optionsCount;

        profileElm.querySelector('[data-type="online-status"]').classList.add(`${this.onlineStatus}`);

        const titleElmList = profileElm.querySelectorAll('[data-title]');
        titleElmList.forEach((elm) => {
            const elmTitle = elm.getAttribute('data-title');
            elm.setAttribute('title', elmTitle.replaceBulk(['{{username}}', '{{inventoryCount}}', '{{actionPoints}}', '{{optionsCount}}'], [this.username, this.inventoryCount, this.actionPoints, this.optionsCount]));
        });

        if (!this.DOMElm) {
            this.DOMElm = profileElm.firstChild;
        }

        return this.DOMElm;
    }
}

class ProfileCollection
{
    profiles;
    profileTemplate;
    player;

    constructor(profileCollectionData, profileTemplate, player) 
    {
        this.profiles        = {};
        this.profileTemplate = profileTemplate;
        this.player          = player;
        this.update(profileCollectionData);
    }

    updateAll(profileData, domUpdate, rankingUpdate)
    {
        for (const i in this.profiles) {
            profileData.id = this.profiles[i].id;
            this.updateProfile(profileData, domUpdate, rankingUpdate);
        }
    }

    updateProfile(profileData, domUpdate, rankingUpdate) 
    {
        if (this.profiles[profileData.id] === undefined) {
            this.profiles[profileData.id] = new Profile(profileData);
            document.querySelector('.js-profile-collection').appendChild(this.profiles[profileData.id].domUpdate(this.profileTemplate));
        } else {
            this.profiles[profileData.id].update(profileData);

            if (domUpdate == true) {
                this.profiles[profileData.id].domUpdate(this.profileTemplate);
            }

            if (this.profiles[profileData.id].isActivePlayer && profileData.score !== undefined) {
                this.player.update({
                    'score': this.profiles[profileData.id].score
                });

                this.player.refreshEstimatedScore();
            }

            if (rankingUpdate == undefined || rankingUpdate) {
                this.updateRanking();
            }
        }
    }

    update(profileCollectionData, rankingUpdate) 
    {
        for (const i in profileCollectionData) {
            if (!this.profiles[profileCollectionData[i].id]) {
                this.profiles[profileCollectionData[i].id] = new Profile(profileCollectionData[i]);
            } else {
                this.profiles[profileCollectionData[i].id].update(profileCollectionData[i]);
            }
            document.querySelector('.js-profile-collection').appendChild(this.profiles[profileCollectionData[i].id].domUpdate(this.profileTemplate));
        }

        if (rankingUpdate == undefined || rankingUpdate) {
            this.updateRanking();
        }
    }

    updateRanking()
    {
        const profiles = [];

        for (const i in this.profiles) {
            profiles.push(this.profiles[i]);
        }

        profiles.sort(function(a, b){
            return b.score - a.score;
        });

        var rank = 1;
        for (var i = 0; i < profiles.length; i++) {
            if (i > 0 && profiles[i].score < profiles[i - 1].score) {
                rank++;
            }

            profiles[i].rank = rank;
        }
        
        this.update(profiles, false);
    }

    getActiveProfile()
    {
        for (const i in this.profiles) {
            if (this.profiles[i].isActivePlayer) {
                return this.profiles[i];
            }
        }

        return false;
    }
}

/**
 * Handles chat
 */
class ChatBox
{
    wrapperElm;
    historyElm;
    templates = {};

    constructor(wrapperElm, chatData)
    {
        this.wrapperElm = wrapperElm;
        this.init(chatData);
    }

    init(chatData) 
    {
        this.wrapperElm.querySelector('.chat-form').addEventListener('submit', (e) => {
            e.preventDefault();

            if (e.currentTarget.querySelector('input').value.length <= 3) {
                e.currentTarget.querySelector('input').value = "";
                return;
            }

            sendData('POST', `/api/games/${Popotamo.gameID}/chat`, new URLSearchParams(new FormData(e.currentTarget)).toString());

            e.currentTarget.querySelector('input').value = "";
        });

        this.historyElm           = this.wrapperElm.querySelector('.history');
        this.templates.message    = this.wrapperElm.querySelector('template[data-type="chat-message"]');
        this.templates.newWord    = this.wrapperElm.querySelector('template[data-type="chat-new_word"]');
        this.templates.skillUse   = this.wrapperElm.querySelector('template[data-type="chat-skill_use"]');
        this.templates.boardClean = this.wrapperElm.querySelector('template[data-type="chat-board_clean"]');

        for (const i in chatData) {
            this.addEntry(chatData[i]);
        }
    }

    addEntry(data) 
    {
        let messageElm;

        const date = new Date(data.datetime);
        switch (data.eventType) {
            case 'newWords':
                let words = [];
                let points = 0;
                for (const i in data.newWords) {
                    words.push(data.newWords[i].word);
                    points += Number.parseInt(data.newWords[i].points)
                }

                messageElm = this.templates.newWord.content.cloneNode(true);
                messageElm.querySelector('[data-timestamp]').innerText = ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2);
                messageElm.querySelector('[data-username]').innerText  = data.authorName;
                messageElm.querySelector('[data-words]').innerText     = words.join(', ');
                messageElm.querySelector('[data-points]').innerText    = points;
                break;

            case 'message':
                messageElm = this.templates.message.content.cloneNode(true);
                messageElm.querySelector('[data-timestamp]').innerText = ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2);
                messageElm.querySelector('[data-username]').innerText  = data.authorName;
                messageElm.querySelector('[data-message]').innerText   = data.message;
                break;

            case 'optionUsed':
                messageElm = this.templates.skillUse.content.cloneNode(true);
                messageElm.querySelector('[data-timestamp]').innerText = ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2);
                messageElm.querySelector('[data-username]').innerText  = data.authorName;
                messageElm.querySelector('[data-skill]').innerText   = data.optionAlias;
                break;

            case 'boardCleaned':
                messageElm = this.templates.boardClean.content.cloneNode(true);
                messageElm.querySelector('[data-timestamp]').innerText = ("0" + (date.getHours())).slice(-2) + ":" + ("0" + (date.getMinutes())).slice(-2);
                messageElm.querySelector('[data-username]').innerText  = data.authorName;
                break;
        }

        if (!messageElm) {
            console.warn("Unknown chat entry type", data);
            return;
        }

        this.historyElm.appendChild(messageElm);
        this.scrollChatToBottom();
        
    }
    
    /**
     * Force the scroll of the chat to go to the bottom (the new messages are here) 
     * @return void
     */
    scrollChatToBottom() 
    {
        this.historyElm.scrollTop = this.historyElm.scrollHeight;
    }
}

function writeCoordKey(x, y) {
    return `${x}`.padStart(2, "0") + "_" + `${y}`.padStart(2, "0");
}

const initGame = ((gameData) => {
    const letterTemplate  = document.querySelector('template[data-type="letter"]');
    const profileTemplate = document.querySelector('template[data-type="profile"]');
    
    const inventoryElm      = document.querySelector('#inventory');
    const boardElm          = document.querySelector('#board');
    const chatElm           = document.querySelector('#chat');
    const binocularsElm     = document.querySelector("#binocularsProfiles");
    const gameAlertElm      = document.querySelector("#gameLockedAlert");
    const estimatedScoreElm = document.querySelector("#estimatedScore");

    let activeLetter    = null;

    //---- Utils -----//

    function createInventoryLetter(letterData)
    {
        let letter = new Letter(letterTemplate);
        letter.setID(letterData.id);
        letter.setLetter(letterData.letter);
        letter.addEventListener('click', () => {
            if (activeLetter) {
                activeLetter.setStatus('unselected');
            }

            if (activeLetter !== letter) {
                activeLetter = letter;
                letter.setStatus('selected');
            } else {
                activeLetter = null;
            }
        });

        return letter;
    }

    const player            = new Player(gameData.player, estimatedScoreElm);
    const inventory         = new Inventory(inventoryElm);
    const board             = new Board(boardElm);
    const profileCollection = new ProfileCollection(gameData.profiles, profileTemplate, player);
    const chatbox           = new ChatBox(chatElm, gameData.chat);

    board.setLetters(letterTemplate, gameData.board.letters);

    player.setInventory(inventory);
    player.setActionPoints(gameData.player.actionPoints);

    const inventoryLetters = [];
    for (let i in gameData.player.inventory) {
        const letter = createInventoryLetter({id: i, letter: gameData.player.inventory[i]});
        inventoryLetters.push(letter);
    }
    inventory.setLetters(inventoryLetters);

    // Initialize the pop-ups handled by Materialize.css
    M.Modal.init(document.querySelectorAll('.modal'), {'opacity':0.5});
    
    // Initialize the tooltips handled by Materialize.css
    M.Tooltip.init(document.querySelectorAll('.tooltipped'), {});
    
    // Scroll the chat bar to the bottom
    chatbox.scrollChatToBottom();

    //---- Events  -----//
    boardElm.addEventListener('click', (e) => {
        e.preventDefault();
        e.stopPropagation();
    
        let estimateScore = false;
        if (Object.values(e.target.classList).includes("newletter")) {
            const letter = board.pendingLetters[writeCoordKey(e.target.dataset.coordX, e.target.dataset.coordY)];
            
            letter.setStatus('unselected');
            inventory.addLetter(letter);
            activeLetter = null;
            
            //-- Clear the letter from the placed letters pool
            delete board.pendingLetters[writeCoordKey(e.target.dataset.coordX, e.target.dataset.coordY)];

            estimateScore = true;
        } else if (activeLetter !== null) {
            activeLetter.setStatus('unselected');
            board.placeLetter(activeLetter, e.target.dataset.coordX, e.target.dataset.coordY);
            inventory.removeLetter(activeLetter);
            activeLetter = null;
            estimateScore = true;
        }

        if (estimateScore) {
            const letterIdList = {};
            for (const i in board.pendingLetters) {
                letterIdList[writeCoordKey(board.pendingLetters[i].coordX, board.pendingLetters[i].coordY)] = board.pendingLetters[i].id;
            }

            sendData('POST', `/api/games/${gameData.game.id}/score-estimate`, `newLetters=${JSON.stringify(letterIdList)}`)
                .then((response) => {
                    if (response.errorCode !== 'success') {
                        estimatedScoreElm.querySelector('.normal').innerText = 'erreur';
                        estimatedScoreElm.querySelector('.switched').innerText = 'erreur';
                        estimatedScoreElm.classList.add('error');

                        switch (response.errorCode) {
                            case 'mustStartInCenter':
                                estimatedScoreElm.setAttribute('data-tooltip', "Vous devez commencer au centre du plateau");
                                break;

                            case 'onlyOneLetter':
                                estimatedScoreElm.setAttribute('data-tooltip', "Vous devez poser plus d'une lettre");
                                break;

                            case 'wordsNotInDict':
                                estimatedScoreElm.setAttribute('data-tooltip', "Au moins un mot n'est pas dans le dictionnaire");
                                break;
                        }
                        return;
                    }

                    const score = response.data.score;
                    const words = response.data.words;

                    player.refreshEstimatedScore(score);
                })
            ;
        }
    });

    estimatedScoreElm.addEventListener('click', () => {
        estimatedScoreElm.classList.toggle('toggled');
    });

    //---- Actions -----//
    window.Popotamo = {};
    window.Popotamo.gameID = gameData.game.id;

    window.Popotamo.drawLetter = async () => {
        let apiResult = await sendData('POST', '/api/skills/use', `gameId=${gameData.game.id}&skill=draw`);

        if (!apiResult) {
            return;
        }
        
        if(apiResult.errorCode === "not_enough_action_points") {
            let missingAP = apiResult.data.requiredActionPoints - apiResult.data.currentActionPoints;
            alert("Il vous manque "+missingAP+" points d'action avant de pouvoir "
                + "piocher une nouvelle lettre. Patience...");
        }
        else if(apiResult.errorCode === "full_inventory") {
            alert("Votre inventaire est plein, vous ne pouvez pas piocher de nouvelles lettres. "
                + "Utilisez vos lettres actuelles pour poser des mots sur le plateau.");
        }
        else if(apiResult.errorCode !== "success") {
            // Generic message to display something even when the error is unknown
            alert('Erreur inconnue : {'+apiResult.errorCode+'}');
        }
        else {
            // Add the new letters in the HTML inventory
            for(const [letterId, letter] of Object.entries(apiResult.data.newLetters)) {
                const letterObj = createInventoryLetter({id: letterId, letter: letter});
                inventory.addLetter(letterObj);
            }
            
            // Update the number of action points
            player.setActionPoints(apiResult.data.newActionPoints);
            player.setActionPointsLastRefresh(new Date());
        }
    };

    /**
     * Execute a skill (tornado...)
     * @param {string} skillAlias
     * @returns {undefined}
     */
    window.Popotamo.useOption = async (skillAlias) => {
        // Execute the skill
        let apiResult = await sendData('POST', '/api/skills/use', `gameId=${gameData.game.id}&skill=${skillAlias}`);
        
        // Update all the letters of the inventory
        const letterCollection = [];
        if (apiResult.data.newInventoryLetters) {
            board.clearPendingLetters();
        }

        for(let [letterId, newLetter] of Object.entries(apiResult.data.newInventoryLetters)) {
            const letter = createInventoryLetter({id: letterId, letter: newLetter});
            letterCollection.push(letter);
        }    
        inventory.setLetters(letterCollection);
    };

    /**
    * Sends to the server the new letters placed, to validate the new words
    */
    window.Popotamo.sendNewLetters = () => {
        if(Object.values(board.pendingLetters).length == 0) {
            alert("Posez au moins une lettre sur le plateau avant de valider.");
            return;
        } 

       const letterIdList = {};
       for (const i in board.pendingLetters) {
            letterIdList[writeCoordKey(board.pendingLetters[i].coordX, board.pendingLetters[i].coordY)] = board.pendingLetters[i].id;
       }
       
       let apiResult = sendData("POST", `/api/games/${gameData.game.id}/popotamo`, `gameId=${gameData.game.id}&newLetters=${JSON.stringify(letterIdList)}`);
       apiResult.then(function(api) {
           if(api.errorCode === "wordsNotInDict") {
               alert("Le mot « "+api.data["wordsNotInDict"][0]["word"]+" » n'est pas dans le dictionnaire !");
           } 
           else if(api.errorCode === "lettersInOccupiedCells") {
               alert("Vous ne pouvez pas placer des lettres sur des cases déjà occupées.");
           }
           else if(api.errorCode === "lettersNotConnected") {
               alert("Certaines lettres ne sont pas connectées aux mots déjà posés !");
           }
           else if(api.errorCode === "mustStartInCenter") {
               alert("La case centrale du plateau doit contenir une lettre !");
           }
           else if(api.errorCode === "onlyOneLetter") {
               alert("Vous devez former au moins 1 mot !");
           }
           else {
               board.commitPendingLetters();
               player.refreshEstimatedScore();
           }
       });
   };
   
    /**
     * Remove all the letters of the board.
     * Useful to clean the board after a player reaches a border.
     */
    window.Popotamo.cleanBoard = () => {
        board.clear();
    }

    /**
     * Use the option "Jumelles" in game (to see the leters of an opponent)
     * @param {Int} The ID of the player to spy
     */
    window.Popotamo.useOptionBinoculars = async (targetedProfileId) => {    
        let apiResult = await sendData('POST', '/api/skills/use', `gameId=${gameData.game.id}&skill=binoculars&targetedProfileId=${targetedProfileId}`);

        if (apiResult.data.letters.length) {
            const binocularWrapper        = document.createElement('p');
            const binocularLettersWrapper = binocularWrapper.querySelector('p');

            for (let letter of apiResult.data.letters) {
                const playerLetter = new Letter();
                playerLetter.setLetter(letter);
    
                binocularLettersWrapper.appendChild(playerLetter.getElement());
            }
            
            binocularsElm.innerHTML = '<p>Le joueur possède actuellement ces lettres :</p>';
            binocularsElm.appendChild(binocularWrapper);
        } else {
            binocularsElm.innerHTML = '<p>Le joueur ne possède actuellement aucune lettre</p>';
        }
    };

    /**
     * Use the option "Revolution" in game (to switch the inventories)
     * @param {Int} targetedProfileId The ID of the player to switch with.
     */
    window.Popotamo.useOptionRevolution = async (targetedProfileId) => {
        await sendData('POST', '/api/skills/use', `gameId=${gameData.game.id}&skill=revolution&targetedProfileId=${targetedProfileId}`);
    };

    /**
     * Use the option "Vol" in game (to steal a random letter to an opponent)
     * @param {Int} targetedProfileId The ID of the player to steal
     */
    window.Popotamo.useOptionThief = async (targetedProfileId) => {
        await sendData('POST', '/api/skills/use', `gameId=${gameData.game.id}&skill=thief&targetedProfileId=${targetedProfileId}`);
    };

    window.Popotamo.startGame = () => {
        gameAlertElm.classList.add('hidden');
        setInterval(() => { player.refreshActionPointsCounter(); }, 1000);
    };

    const createSocket = () => {
        profileCollection.updateAll({
            'onlineStatus': 'pending'
        }, true);

        let loggedIn   = false;
        let connection = new WebSocket(gameData.wsUrl);

        connection.onopen = function(event) {
            loggedIn = false;
            connection.send(JSON.stringify({
                'command': 'auth',
                'auth': {'username': gameData.player.username, 'password': gameData.frontToken},
                'payload': {
                    'gameID': gameData.game.id
                } 
            }));
        };

        connection.onmessage = function(event) {
            const messageData = JSON.parse(event.data);

            switch (messageData.eventType) {
                case 'auth':
                    loggedIn = messageData.success === true;
                    profileCollection.updateProfile({
                        'id': profileCollection.getActiveProfile().id,
                        'onlineStatus': loggedIn ? 'active' : 'error'
                    }, true);
                    break;

                case 'chatMessage':
                    for (const i in messageData.payload) {
                        chatbox.addEntry(messageData.payload[i]);
                    }
                    break;

                case 'playerConnected':
                    profileCollection.updateProfile({
                        'id': messageData.profile.id,
                        'onlineStatus': 'active'
                    }, true);
                    break;

                case 'activePlayers':
                    profileCollection.updateAll({
                        'onlineStatus': 'offline'
                    }, true);

                    for (const i in messageData.profiles) {
                        profileCollection.updateProfile({
                            'id': messageData.profiles[i],
                            'onlineStatus': 'active'
                        }, true);
                    }
                    break;

                case 'playerQuitting':
                    profileCollection.updateProfile({
                        'id': messageData.profile.id,
                        'onlineStatus': 'offline'
                    }, true);
                    break;

                case 'updateProfile':
                    for (const i in messageData.profile) {
                        profileCollection.updateProfile(messageData.profile[i], true);
                    }
                    break;

                case 'gameStart':
                    Popotamo.startGame();
                    break;

                case 'cleanBoard':
                    window.Popotamo.cleanBoard();
                    break;

                case 'newWords':
                    let newWords = messageData.newWords;
                    // Add the new letters on the board, without refreshing the whole board
                    for (let [coord, newLetter] of Object.entries(newWords)) {
                        const coordList = coord.split('_');

                        const letter = new Letter(letterTemplate);
                        letter.setLetter(newLetter);
                        letter.setCoord(parseInt(coordList[0]), parseInt(coordList[1]));

                        board.addLetter(letter);
                    }
                    break;

                default:
                    console.log("ACTION : " + messageData.eventType);
            }
        };

        connection.onerror = function(event) {
            console.log('WS Error', event);
        };

        connection.onclose = function(event) {
            setTimeout(createSocket, 10000);
        };

        window.addEventListener('beforeunload', (event) => {
            connection.send(JSON.stringify({
                'command': 'quit'
            }));
        });
    };

    setTimeout(function() {
        createSocket();
    }, 1000);

    if (gameData.game.started) {
        Popotamo.startGame();
    }
});

document.dispatchEvent(new CustomEvent('appLoaded'));
