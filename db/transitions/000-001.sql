-- Represents a point in time, with microsecond precision
CREATE DOMAIN instant AS TIMESTAMP(6) WITH TIME ZONE;

-- Helper domain types for common integer rangers
CREATE DOMAIN i8 AS INT2 CHECK (-128 <= value AND value < 128);
CREATE DOMAIN u8 AS INT2 CHECK (0 <= value AND value < 256);
CREATE DOMAIN i16 AS INT2;
CREATE DOMAIN u16 AS INT4 CHECK (0 <= value AND value < 65536);
CREATE DOMAIN i32 AS INT4;
CREATE DOMAIN u32 AS INT8 CHECK (0 <= value AND value < 4294967296);
CREATE DOMAIN i64 AS INT8;

-- User id type (from Eternaltwin)
CREATE DOMAIN user_id AS UUID;
-- User display name type (from Eternaltwin)
CREATE DOMAIN user_display_name AS VARCHAR(64);

-- Data from the Eternaltwin's users database. Popotamo has no proper accounts, all the authentifications are made via
-- OAuth.
CREATE TABLE popo_users (
  -- uuid of the user. Given by the ET's OAuth API.
  user_id USER_ID NOT NULL,
  -- The ET's public name of the user. Can be different from the name of his account (see Eternaltwin's doc).
  display_name USER_DISPLAY_NAME NOT NULL,
  PRIMARY KEY (user_id)
);

-- Stores the data of a Popotamo profile.
-- Not merged with the Eternaltwin users table ("popo_users") beause each ET user can own several Popotamo profiles.
-- Doesn't contain contain the data of the games (letters, action points...), because one Popotamo profile can play
-- simultaneously several games (see table "popo_profiles_games").
CREATE TABLE popo_profiles (
  -- Each physical player can play with multiple profiles. Don't confuse with user_id!
  profile_id SERIAL4 NOT NULL,
  -- ID of the physical player owning the profile (data from Eternal Twin). Don't confuse with profile_id!
  user_id USER_ID NOT NULL,
  -- Name of the player (data from Eternaltwin)
  display_name USER_DISPLAY_NAME NOT NULL,
  -- The "quids" are the in-game money, see the rules
  quids U16 NOT NULL DEFAULT 0,
  PRIMARY KEY (profile_id),
  CONSTRAINT fk_profile_userid FOREIGN KEY (user_id) REFERENCES popo_users(user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Minimal informations about the games (in french: "les parties")
CREATE TABLE popo_games (
  game_id SERIAL4 NOT NULL,
  date_start INSTANT NULL,
  date_end INSTANT NULL,
  nb_players U8 NOT NULL DEFAULT 0,
  PRIMARY KEY (game_id)
);

-- Stores the data of the games (letters, action points...).\r\nNot merged with the general table "popo_profiles",
-- because one profile can play simultaneously several games.
CREATE TABLE popo_profiles_games (
  -- Each physical player can play with multiple profiles. Don't confuse with user_id!
  profile_id INT4 NOT NULL,
  -- ID of the current game of the player
  game_id INT4 NULL,
  -- Amount of action points for the game
  action_points U16 NOT NULL DEFAULT 0,
  -- Points earned in the current game
  score U16 NOT NULL DEFAULT 0,
  -- The score the player has to reach to end his game
  score_goal U16 NOT NULL DEFAULT 0,
  -- Last time the action points have been update. Useful to calculate how many AP the player has earned since his last
  -- connection. Store UTC dates to avoid confusions with time zones.
  last_ap_refresh_utc INSTANT DEFAULT NULL,
  -- Note the player rank once he has finished the current game 1st, 2nd, etc.
  winner_rank U8 DEFAULT NULL,
  PRIMARY KEY (profile_id, game_id),
  CONSTRAINT fk_profilegame_gameid FOREIGN KEY (game_id) REFERENCES popo_games(game_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_profilegame_profileid FOREIGN KEY (profile_id) REFERENCES popo_profiles(profile_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

-- The letters the player owns in the game
CREATE TABLE popo_profiles_inventories (
  -- ID unique to each letter in the inventories, even when the user owns several copies of the same letter
  letter_id SERIAL4,
  profile_id INT4 NOT NULL,
  game_id INT4 NOT NULL,
  letter CHAR(1) NOT NULL,
  PRIMARY KEY (letter_id),
  CONSTRAINT fk_inventory_profileid FOREIGN KEY (profile_id) REFERENCES popo_profiles(profile_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_inventory_gameid FOREIGN KEY (game_id) REFERENCES popo_games(game_id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- Options = tornado, revolution... Competences = wisdom, imagination...
CREATE TYPE SKILL_TYPE AS ENUM ('options', 'competences');
-- Set the behavior of the pop-up when activating the skill
CREATE TYPE SKILL_POPUP_TYPE AS ENUM ('confirm', 'choseLetter', 'choseProfile');

CREATE TABLE popo_skills (
  skill_id INT4 NOT NULL,
  skill_type SKILL_TYPE NOT NULL,
  alias VARCHAR(20) NOT NULL,
  -- The name of the skill to display to the user. Write the first letter in lowercase.
  name VARCHAR(30) NOT NULL,
  -- HTML code for emoji icons (not image file)
  icon_text VARCHAR(30) NULL,
  icon_img VARCHAR(80) NULL,
  -- Description of the skill, displayed in the tooltip, pop-up, etc. HTML is allowed.
  description VARCHAR(1024) NULL,
  popup_type SKILL_POPUP_TYPE NULL,
  -- Amount of "Quids" required to buy the skill
  quids_cost U16 NOT NULL DEFAULT 0,
  -- Max upgrade level allowed for this skill
  max_level U8 NOT NULL,
  PRIMARY KEY (skill_id)
);

CREATE TABLE popo_profiles_skills (
  profile_id INT4 NOT NULL,
  skill_id INT4 NOT NULL,
  level U8 NOT NULL DEFAULT 0,
  is_activated BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (profile_id, skill_id),
  CONSTRAINT fk_option_profileid FOREIGN KEY (profile_id) REFERENCES popo_profiles(profile_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_option_skillid FOREIGN KEY (skill_id) REFERENCES popo_skills(skill_id) ON DELETE RESTRICT ON UPDATE CASCADE
);

CREATE TABLE game_boards (
  game_id INT4 NOT NULL,
  board JSONB NOT NULL,
  PRIMARY KEY (game_id)
);

CREATE TABLE game_chats (
  game_id INT4 NOT NULL,
  chat JSONB NOT NULL,
  PRIMARY KEY (game_id)
);
