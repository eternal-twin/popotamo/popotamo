# Popotamo/Street writer

## Pré-requis

- PHP
- [PostgreSQL](https://eternaltwin.org/docs/db)
- Composer
- [Node.js 14.13.1 ou plus](https://eternaltwin.org/docs/tools/node)
- [Yarn](https://eternaltwin.org/docs/tools/yarn)

## Démarrer

1. Clonez le dépôt Git
2. Installation des dépendances: `composer install`
3. Démarrez le serveur Eternaltwin local : `composer run-script -- eternaltwin start`
4. Création de la base de données :
   1. Créez une base de données Postgres. [Aide Eternaltwin](https://eternal-twin.net/docs/db).
   2. Copiez le fichier `/_BUILD/samples/database-logins.json` et collez-le dans le dossier `/_CONFIG/`
   3. Ouvrez le fichier `/_CONFIG/database-logins.json` que vous venez de créer. Remplacez les exemples
      d'identifiants par les identifiants de connexion à votre base de données (host/login/password).
   4. Initialisez la base de données : `composer run-script db:sync`
5. Copiez `/_BUILD/samples/config-server.json` vers `/_CONFIG/config-server.json`. Éditez-le si nécessaire.
6. Démarrez le serveur.
   ```
   php -S localhost:50370 -t ./public/
   ```

## Commandes composer

### `build`

Génère le dictionnaire Popotamo.

#### Utilisation

```
composer run-script -- build [--no-confirm]
```

### `db:sync`

Synchronise le schéma de base de données avec la dernière version.

#### Utilisation

```
composer run-script -- db:sync [--no-confirm]
```

### `db:reset`

Réinitialise l'état de la base de données. Attention, CETTE COMMANDE SUPPRIME
LA BASE DE DONNÉES.
Cette commande permet de tester les modification du schéma de données plus
rapidement.

#### Utilisation

```
composer run-script -- db:reset [--no-confirm]
```

### `eternaltwin`

Server Eternaltwin local.
Utilisez `--help` pour voir les commandes disponibles.

```
composer run-script -- eternaltwin --version
composer run-script -- eternaltwin --help
composer run-script -- eternaltwin start
...
```

## Comment conserver son compte sur la version locale

Par défaut, sur votre version locale de développement, les comptes que vous créez sont effacés à chaque extinction du PC.
La version locale d'Eternatwin utilise en effet le mode ''in-memory'' (tout est stocké dans la RAM).
Vous pouvez modifier ce comportement en stockant les comptes dans un base de données Postgres :
1. Créez une BDD Postgres locale (une autre instance que la BDD de Popotamo)
2. Mettez à jour le fichier etwin.toml avec les identifiants Postgres pour Eternaltwin
3. Lancez ''etwin db check'' pour vérifier qu'il arrive à se connecter à la BDD
4. Lancez ''etwin db create'' pour initialiser la BDD
5. C'est fait : à présent, les comptes que cvous créez sur votre version de développement sont conservées en BDD et non plus en RAM.

Doc complémentaire : https://eternal-twin.net/docs/app/etwin-integration


## Documentation sur le temps réel (WebSocket)
La couche de temps réel de ePopotamo repose sur les WebSocket. Les navigateurs modernes prennent en charge nativement les WebSocket, et la partie back est gérée en PHP pour simplifier l'interfaçage avec la logique métier. 

Pour lancer le serveur de Socket, la commande `php bin/console popotamo:socket` est disponible. 
Elle écoute le port via la variable `socketPort` configuré dans config-server.json 

- Le sens de communication est principalement de serveur à client, et les messages sont structurés en objet json
- Un socket qui se connecte doit s'authentifier pour pouvoir lancer des commandes et recevoir des réponses
- Un socket non authentifier est déconnecté après 5 secondes
- Un socket qui rate son authentification est déconnecté
- Le socket d'un joueur en partie se verra attribué un salon, qui rassemble les joueurs connectés de la même partie 
- Les commandes nécessitent un niveau de droit. Lors de l'authentification, le socket se verra attribué un niveau de droit reflété par sa classe (PopoSocket pour un joueur, AdminSocket pour la supervision)
- Une commande est appelée dans l'attribut `command` de l'objet json envoyé. 
- Une commande peut nécessiter des données supplémentaires (nom d'utilisateur/mot de passe dans le cas de l'authentification)
- Le succès ou non de la commande est envoyé dans l'attribut `success` (booléen) de la réponse, avec le payload
- Les évènements envoyés de façon automones depuis le serveur ont tous un `eventType` attaché dans l'objet json, qui permet de connaitre le comportement à avoir

Une vue superviseur est disponible dans la partie administration de ePopotamo `/admin/socket`

# Règles du jeu

## Description / spécifications

(synthèse des discussions sur le discord d'Eternal Twin)

Le jeu est relativement simple : <http://www.popotamo.com/help>

NB : une copie de cette page a été saugardée sur le gitlab popotamo-extra : <https://gitlab.com/eternal-twin/popotamo/popotamo-extra/-/tree/master/game-rules>

## Fonctionnalités minimales

Basiquement, c'est un scrabble. Mais tous les joueurs jouent en même temps ! Ce n'est pas un jeu en tour par tour.

Déroulement d'une partie :
  * La partie démarre quand 5 joueurs l'ont rejointe (ou 4 : v. plus bas "Précisions sur les règles")
  * Le joueur pioche des lettres avec le bouton "Piocher". Un joueur ne peut piocher plus d 6 lettres. améliorations possibles (A DOCUMENTER).
  * Le joueur pose ses lettres, puis valide son mot avec le bouton "Popotamo". règles de pose :
    - Les nouvelles lettres posées doivent passer par au moins 1 lettre déjà sur le plateau.
	- Le joueur peut poser autant de lettres qu'il veut (pas de tour par tour).
	- Le joueur peut aussi compléter une mot déjà sur le plateau (ex : "parti"+"es")
  * Quand un joueur doit piocher, un compte à rebours se déclenche. Sa durée dépend de la compétence "rapidité" du joueur :
    - 1 point de rapidité = 1 pioche toutes les 20 minutes
    - 10 points de rapidité = 1 pioche toutes les 10 minutes
	 NB : en mode "Rapide", les délais sont plus courts d'en mode "Normal"
  * Le 1er mot doit passer par la case centrale du plateau
  * Si un mot touche le bord du plateau, ce dernier est entièrement nettoyé et le joueur perd 10 points.
  * Lorsqu'un joueur atteint le nombre de points demandé il gagne la partie, les autres joueurs continuent à jouer jusqu'à ce que 4 joueurs aient atteint ce nombre.

**A prévoir en termes de code :**
  * Une grille en tableau HTML
  * Une zone d'inventaire affichant les lettres du joueurs + 1 bouton "Piocher"
  * Clic sur une lettre pour la sélectionner, clic sur la case où la poser. On peut annuler la pose en cliquant à nouveau sur la lettre sur la grille pour la remettre dans l'inventaire.
  * Le serveur vérifie si les lettres posées forment un mot présent dans le dictionnaire du jeu. Implications en code :
	- Chaque nouvelle lettre posée doit être mémorisée temporairement, avec ses coordonnées
	- Au moment de la vérification, le serveur remet les lettres dans l'ordre en les triant par coordonnées
	- Vérifier que ne forme pas de mot inconnu avec les autres cases adjacentes
	  => Si pas de mot formé, afficher un message au joueur pour lui demander de changer son mot.
    => Si un mot est formé, ajouter les points au joueur. Points gagnés = Nombre de lettres déjà posées + Nombre de nouvelles lettres au carré (v. le point 4 de l'aide officielle pour le détail)

  * Dans Popotamo, un algorithme règle la pioche des lettres et fait qu'on a peu de chance de tirer 8 consonnes ou 8 voyelles ou kwqj dans une pioche, contrairement à Street writer où ce genre de chose n'est pas rare !


## Fonctionnalités avancées
  * Rythmes de jeu :
	- Mode normal = ???
	- Mode rapide = ???
  * Partie à thèmes = seuls les mots du thème sont acceptés
  * Classement des joueurs par points
  * Compétences à acquérir

  * En plus des parties "normales" ou "à thèmes" nous avons des parties "énigmes " ou "tournoi double " ou "tournoi simple". Pour ces parties là, nous jouons en équipe en choisissant nos partenaires et sur une base de mots pré-rentrés par Bouillegri et qui rougiront. Cf topic  Forum "Tournois et Énigmes" :
	http://www.popotamo.com/forum/8


## Précisions sur les règles

**Commencer une partie :** une partie se joue normalement à 5, mais s'il y a un trop long temps d'attente, on passe à 4 (au bout de 10 mn).
Le joueur  qui s'inscrit sur une partie ne connait pas ses adversaires  jusqu’au lancement de la partie.
Tant qu'une partie n'et pas démarrée, le joueur qui s'est inscrit  peut à tout moment se désengager.


## Besoins graphiques
Après la mise en service de la V1 fonctionnelle, il faudra ajouter quelques effets visuels pour rendre le jeu plus vivant :
   * Déplacement des lettres par glisser-déposer (plus attractif  qu'un simple "je clique sur la lettre, elle s'efface de l'inventaire et s'affiche sur la grille"). Attention cependant, pour le mobile la méthode en 2 clics de Popotamo est sans doute plus adaptée...
   * Effet visuel sur la grille quand un mot est formé (clignotement...)
   * Effet visuel lors du gain de points


## Ressources disponibles

=> Site de joueur expliquant des mécaniques du jeu : http://klariaweb.free.fr/popotamo/

=> Popotamo sur Webarchive : https://web.archive.org/web/20071020030742/http://pub.motion-twin.com/popotamo/fr/

=> Nous avons déjà la base de mots du jeu (le "popodico") : dans le dépôt "popotamo-extra" sur le Gitlab Eternal Twin. Il est basé sur l'ODS 5 (Officiel Du Scrabble version 5). L'ODS est à la version 8 en 2020, mais un souci de droits d'auteur n'a pas permis à Motion Twin de l'actualiser.
Un formulaire de recherche manuel est disponible sur ce site : http://patatoide.com/popodico/
Contacts : sur Discord : @bouillegri#3812, @lettris#6826, @sissouf#3660

=> Autre base de mots envisageable : http://www.lexique.org/

=> Les joueurs avaient aussi fait programme "dico" qui leur permettait d'entrer tous les mots que nous désirions pour y entrer des mots des parties à thèmes  qui rougissaient et doublaient la valeur des points obtenus sur les parties.

=> Ressources forum Popotamo (à sauvegarder avant fermeture du site !!)
   * Conseils sur le placement des mots, les stratégies... : http://www.popotamo.com/forum/thread/1326703
   * Et les autres post-it de https://fr.scrabblecheat.org/anagramme
