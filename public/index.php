<?php

use Popotamo\Kernel;


ini_set('display_errors', true);

require_once '../vendor/autoload.php';

function dump(...$args) {
    echo "<pre>";
    call_user_func_array('var_dump', $args);
    echo "</pre>";
}
function dd(...$args) {
    echo "<pre>";
    echo debug_backtrace()[0]['file'] . ":" . debug_backtrace()[0]['line'];
    dump($args);
    echo "</pre>";
    die;
}

/*
 * Timezone configuration
 */
// Explicitly set the server time zone to prevent unexpected server configuration.
// Work in UTC to avoid confusions with the different time zones.
date_default_timezone_set('UTC');
// Put the dates in the french format (e.g. with strftime())
// by applying the tme zone set by date_default_timezone_set()
setlocale(LC_TIME, '');

define('PAGE_DIR', realpath(Kernel::getProjectDir().'/src/Pages/'));

ob_start();

$kernel = new Kernel();
$kernel->loadRouteCollection([
    "#^/api/skills/use/?#"                               => 'apiUseSkill',
    "#^/api/games/(?P<gameId>[0-9]+)/popotamo/?#"        => 'apiValidateWord',
    "#^/api/games/(?P<gameId>[0-9]+)/score-estimate/?#"  => 'apiScoreEstimate',
    "#^/api/games/(?P<gameId>[0-9]+)/chat/?#"            => 'apiChat',
    "#^/api/games/(?P<gameId>[0-9]+)/sse/?#"             => 'sseHandler',
    "#^/game/(?P<gameId>[0-9]+)/?#"                      => 'showGame',
    "#^/minishop/(?P<gameId>[0-9]+)/?#"                  => 'minishop',
    "#^/login/?#"                                        => 'login',
    "#^/oauth/callback/?#"                               => 'oAuthCallback',
    "#^/logout/?#"                                       => 'logout',
    "#^/stats/?#"                                        => 'stats',
    "#^/healthcheck/?#"                                  => 'healthcheck',
    "#^/admin/?$#"                                       => 'adminDashboard',
    "#^/admin/games/?$#"                                 => 'adminGames',
    "#^/admin/games/(?P<gameId>[0-9]+)/?$#"              => 'adminGame',
    "#^/admin/socket/?$#"                                => 'adminSocket',
    "#^/?$#"                                             => 'index',
]);
$kernel->handle(explode('?', $_SERVER['REQUEST_URI'])[0]);
